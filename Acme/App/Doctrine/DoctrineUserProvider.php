<?php

namespace Saferia\App\Doctrine;

use LaravelDoctrine\ORM\Facades\EntityManager;
use Doctrine\ORM\EntityRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Saferia\App\Services\HashingStrategy;
use Saferia\Domain\Shared\Email;
use Saferia\Domain\Shared\HashedPassword;
use Saferia\Domain\Shared\Password;
use Saferia\Domain\User\User;

/**
 * This class is very lightly implemented, possibly not matching all the Laravel guidelines for an user provider,
 * but this is only used for resetting passwords, do not use this UserProvider for the Auth methods of Laravel.
 *
 * Class DoctrineUserProvider
 * @package Saferia\App\Doctrine
 */
class DoctrineUserProvider implements UserProvider
{
    /** @var  EntityRepository */
    private $repository;
    private $hasher;

    public function __construct(HashingStrategy $hasher)
    {
        $this->repository = EntityManager::getRepository(User::class);
        $this->hasher = $hasher;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return $this->repository->find($identifier);
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string $token
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return $this->repository->findOneBy(['id' => $identifier, 'remember_token' => $token]);
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string                                     $token
     *
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setRememberToken($token);

        EntityManager::persist($user);
        EntityManager::flush();
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        return $this->repository->findOneBy(['email.email' => $credentials['email']]);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array                                      $credentials
     *
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->hasher->check($credentials['password'], $user->getAuthPassword());
    }
}