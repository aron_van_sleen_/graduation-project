<?php

namespace Saferia\App\Presenters;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use League\Period\Period;

class CalendarMonth
{
    protected $year;
    protected $month;

    /** @var  Collection */
    protected $dates;

    /** @var Period */
    protected $period;

    public function __construct($year = null, $month = null)
    {
        $this->year = (null === $year) ? date("Y") : $year;
        $this->month = (null === $month) ? date("n") : $month;

        $this->dates = new Collection();
        $this->populateDates();
    }

    private function populateDates()
    {
        $first_day = Carbon::create($this->year, $this->month)->firstOfMonth()->startOfWeek();
        $last_day = Carbon::create($this->year, $this->month)->lastOfMonth()->endOfWeek();

        $this->period = new Period($first_day, $last_day);

        foreach ($this->period->getDatePeriod('1 DAY') as $datetime) {
            $this->dates->push($datetime);
        }
    }

    /**
     * @return Collection
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @return Period
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @return Collection
     */
    public function getDays()
    {
        return $this->dates->flatten()->keyBy(function ($datetime) {
            return $datetime->format("Y-m-d");
        });
    }

    /**
     * @return Carbon
     */
    public function getCalendarDate()
    {
        return Carbon::create($this->year, $this->month)->firstOfMonth();
    }
}