<?php

namespace Saferia\App\Presenters;


use Illuminate\Pagination\UrlWindowPresenterTrait;

trait CustomUrlWindowPresenterTrait
{
    /** Overriding methods from this trait */
    use UrlWindowPresenterTrait;

    /**
     * Render the actual link slider.
     *
     * @return string
     */
    protected function getLinks()
    {
        $html = '';

        if ($this->currentPage() > 2) {
//            $html .= $this->getPageLinkWrapper($this->pages[1], 1);
            $html .= $this->getDots();
        }

        $html .= $this->getUrlLinks($this->pages);

        if ($this->currentPage() < $this->lastPage() - 1) {
            $html .= $this->getDots();
//            $html .= $this->getPageLinkWrapper($this->pages[$this->lastPage()], $this->lastPage());
        }

        return $html;
    }

    /**
     * @param array $urls
     *
     * @return string
     */
    protected function getUrlLinks(array $urls)
    {
        $currentPage = $this->currentPage();

        $html = '';


        if ($currentPage == $this->lastPage() && count($urls) >= 3) {
            $html .= $this->getPageLinkWrapper($urls[$currentPage - 2], $currentPage - 2);
        }
        // Generate links to the left of the active link
        if ($currentPage > 1) {
            $html .= $this->getPageLinkWrapper($urls[$currentPage - 1], $currentPage - 1);
        }

        // Generate the active link
        $html .= $this->getPageLinkWrapper($urls[$currentPage], $currentPage);

        // Generate links to the right of the active link
        if ($currentPage < $this->lastPage()) {
            $html .= $this->getPageLinkWrapper($urls[$currentPage + 1], $currentPage + 1);
        }
        if ($currentPage == 1 && count($urls) >= 3) {
            $html .= $this->getPageLinkWrapper($urls[$currentPage + 2], $currentPage + 2);
        }

        return $html;
    }
}