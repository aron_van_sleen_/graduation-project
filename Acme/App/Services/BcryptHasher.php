<?php

namespace Saferia\App\Services;

use Illuminate\Hashing\BcryptHasher as Bcrypt;
use Saferia\Domain\Shared\HashedPassword;
use Saferia\Domain\Shared\Password;

class BcryptHasher implements HashingStrategy
{
    private $hasher;

    public function __construct()
    {
        $this->hasher = new Bcrypt();
    }

    /**
     * @param Password $password
     *
     * @return HashedPassword|string
     */
    public function hash(Password $password)
    {
        return $this->hasher->make((string) $password);
    }

    /**
     * @param  $password
     * @param  $hash
     * @return boolean
     */
    public function check($password, $hash)
    {
        return $this->hasher->check($password, $hash);
    }
}