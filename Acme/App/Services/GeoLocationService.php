<?php

namespace Saferia\App\Services;

use Saferia\Domain\Shared\Address;
use Saferia\Domain\Shared\GeoLocation;

class GeoLocationService
{
    private $apiUrl = 'https://maps.googleapis.com/maps/api/geocode/';
    private $apiKey = '';

    /**
     * @param $searchAddress
     *
     * @return GeoLocation
     */
    public function addressToGeoLocation($searchAddress)
    {
        $url = $this->apiUrl . 'json?address=' . urlencode((string)$searchAddress) . '&components=country:NL&key=AIzaSyCeXL5B1CxftaN-2yjMK9Z68KT-8l3l9JI' . $this->apiKey;
        $data = file_get_contents($url);

        if (!$data) {
            return null;
        }

        $data = json_decode($data);
        $latitude = $data->results[0]->geometry->location->lat;
        $longitude = $data->results[0]->geometry->location->lng;

        return new GeoLocation($latitude, $longitude);
    }
}