<?php

namespace Saferia\App\Services;

use Saferia\Domain\Shared\HashedPassword;
use Saferia\Domain\Shared\Password;

interface HashingStrategy
{
    /**
     * @param Password $password
     * @return \Saferia\Domain\Shared\HashedPassword
     */
    public function hash(Password $password);

    /**
     * @param  $password
     * @param  $hash
     *
     * @return bool
     */
    public function check($password, $hash);
}