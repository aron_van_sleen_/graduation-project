<?php

namespace Saferia\App\Services;

use Illuminate\Support\Facades\Log;
use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\PayRequest;
use PayPal\Types\AP\Receiver;
use PayPal\Types\AP\ReceiverList;
use PayPal\Types\Common\RequestEnvelope;
use Saferia\Domain\Careclient\Plan;
use Webpatser\Uuid\Uuid;

class PayPalPaymentGateway
{
    /**
     * @var AdaptivePaymentsService
     */
    private $service;

    /**
     * PayPalPaymentGateway constructor.
     *
     * @param AdaptivePaymentsService $service
     */
    public function __construct(AdaptivePaymentsService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Uuid $userId
     *
     * @param Plan $plan
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sendSubscriptionRequest($userId, Plan $plan)
    {
        // Format it to a proper decimal amount
        $amount = number_format($plan->getPrice()->getAmount() / 100, 2, '.', '');

        $requestEnvelope = new RequestEnvelope('nl_NL');

        $receivers = array();
        $receivers[0] = new Receiver($amount);
        $receivers[0]->email = env('PAYPAL_RECEIVER_EMAIL');
        $receiverList = new ReceiverList($receivers);

        $payRequest = new PayRequest(
            $requestEnvelope,
            "PAY",
            route('dashboard'),
            "EUR", $receiverList,
            route('home')
        );

        $customData = json_encode(['userId' => $userId, 'plan' => (string) $plan]);

        // TODO: Extract IPN url to env
        $payRequest->ipnNotificationUrl = 'http://c08be4c6.ngrok.io/ipn-subscription-purchased?custom=' . $customData;

        try {
            $response = $this->service->Pay($payRequest);
        } catch (Exception $e) {
            Log::error('PayPal PayRequest failed:', ['message' => $e->getMessage()]);
        }

        return $response->payKey;
    }
}