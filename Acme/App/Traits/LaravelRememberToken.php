<?php

namespace Saferia\App\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait LaravelRememberToken
 *
 * Placed in it's own trait, since it's part of Laravel, it's something the User should have, but it's not really part of the user entity.
 */
trait LaravelRememberToken
{
    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $rememberToken;
}