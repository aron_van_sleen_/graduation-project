<?php

namespace Acme\App\ViewModels;


class CareproviderCardViewModel
{
    public $id;
    public $name;
    public $city;
    public $rating;
    public $profileImage = null;
    public $hourlyRate;
    public $slug;
}