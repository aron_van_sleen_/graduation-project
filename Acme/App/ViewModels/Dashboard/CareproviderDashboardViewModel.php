<?php

namespace Saferia\App\ViewModels\Dashboard;


use Saferia\Domain\Hiring\CareproviderRequest;
use Saferia\Domain\Hiring\HiringRequest;
use Saferia\Domain\Hiring\InterviewRequest;
use Saferia\Infrastructure\Careprovider\DashboardInformationOfCareprovider;

class CareproviderDashboardViewModel
{
    public $name;
    public $interviewRequests;
    public $hiringRequests;

    public function __construct(DashboardInformationOfCareprovider $query)
    {
        $data = $query->get();
        $this->name = $data['name.firstName'];

        $this->interviewRequests = $data['requests']->filter(function ($request) {
            return $request instanceof InterviewRequest;
        })->map(function (InterviewRequest $request) {
            return array_merge($this->getGeneralRequestInformation($request), [
                'dates' => $request->getRequestedDates()->sortBy('date')
            ]);
        });

        $this->hiringRequests = $data['requests']->filter(function ($request) {
            return $request instanceOf HiringRequest;
        })->map(function (HiringRequest $request) {
            return array_merge($this->getGeneralRequestInformation($request), [
                'date' => $request->getSuggestedDate(),
                'to'   => $request->getSuggestedDate()->copy()->addMinutes($request->getDuration())
            ]);
        });
    }

    private function getGeneralRequestInformation(CareproviderRequest $request)
    {
        return [
            'id'           => $request->getId(),
            'name'         => (string) $request->getCareclient()->getName(),
            'email'        => (string) $request->getCareclient()->getEmail(),
            'phoneNumber'  => (string) $request->getCareclient()->getPhoneNumber(),
            'profileImage' => $request->getCareclient()->getProfileImage(),
            'comment'      => $request->getComment(),
            'slug'         => $request->getCareclient()->getSlug()
        ];
    }
}