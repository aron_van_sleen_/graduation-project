<?php

namespace Saferia\App\ViewModels\Dashboard;


use Saferia\Domain\Careclient\Careclient;
use Saferia\Domain\User\Characteristic;

class UpdateCareclientProfileViewModel
{
    public $firstName;
    public $lastName;
    public $streetName;
    public $streetNumber;
    public $city;
    public $postalCode;
    public $dateOfBirth;
    public $phoneNumber;
    public $profileImage;
    public $description;
    public $qualities;
    public $characteristics;

    public function __construct(Careclient $careclient)
    {
        $this->firstName = $careclient->getName()->getFirstName();
        $this->lastName = $careclient->getName()->getLastName();

        $this->dateOfBirth = array(
            'day'   => sprintf("%02d", $careclient->getDateOfBirth()->day),
            'month' => sprintf("%02d", $careclient->getDateOfBirth()->month),
            'year'  => $careclient->getDateOfBirth()->year,
        );

        $this->streetName = $careclient->getAddress()->streetName;
        $this->streetNumber = $careclient->getAddress()->streetNumber;
        $this->city = $careclient->getAddress()->city;
        $postalCode = explode(' ', $careclient->getAddress()->postalCode);
        $this->postalCode = array(
            'digits'     => $postalCode[0],
            'characters' => $postalCode[1]
        );
        $this->phoneNumber = (string) $careclient->getPhoneNumber();
        $this->profileImage = $careclient->getProfileImage();

        $this->characteristics = $careclient->getCharacteristics()->map(function (Characteristic $characteristic) {
            return $characteristic->getId();
        })->toArray();

        $this->description = $careclient->getDescription();
        $this->qualities = $careclient->getQualities();
    }
}