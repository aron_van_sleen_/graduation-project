<?php
namespace Saferia\App\ViewModels\Dashboard;

use Saferia\Domain\Careprovider\Careprovider;
use Saferia\Domain\User\Characteristic;

class UpdateCareproviderProfileViewModel
{
    public $firstName;
    public $lastName;
    public $streetName;
    public $streetNumber;
    public $city;
    public $postalCode;
    public $dateOfBirth;
    public $phoneNumber;
    public $profileImage;
    public $description;
    public $qualities;
    public $characteristics;

    public $hourlyRate;
    public $maximumHourlyRate;

    public function __construct(Careprovider $careprovider)
    {
        $this->firstName = $careprovider->getName()->getFirstName();
        $this->lastName = $careprovider->getName()->getLastName();

        $this->dateOfBirth = array(
            'day'   => sprintf("%02d", $careprovider->getDateOfBirth()->day),
            'month' => sprintf("%02d", $careprovider->getDateOfBirth()->month),
            'year'  => $careprovider->getDateOfBirth()->year,
        );

        $this->streetName = $careprovider->getAddress()->streetName;
        $this->streetNumber = $careprovider->getAddress()->streetNumber;
        $this->city = $careprovider->getAddress()->city;
        $postalCode = explode(' ', $careprovider->getAddress()->postalCode);
        $this->postalCode = array(
            'digits'     => $postalCode[0],
            'characters' => $postalCode[1]
        );
        $this->phoneNumber = (string) $careprovider->getPhoneNumber();
        $this->profileImage = $careprovider->getProfileImage();

        $this->description = $careprovider->getDescription();
        $this->qualities = $careprovider->getQualities();
        $this->characteristics = $careprovider->getCharacteristics()->map(function (Characteristic $characteristic){
            return $characteristic->getId();
        })->toArray();

        $this->hourlyRate = $careprovider->getHourlyRate()->getAmount();
        $this->maximumHourlyRate = $careprovider->getLevel()->getMaximumHourlyRate()->getAmount();
    }
}