<?php

namespace Acme\App\ViewModels;


use Saferia\Domain\User\User;

class RateUserViewModel
{
    public $id;
    public $name;
    public $profileImage;
    public $rating;

    /**
     * RateUserViewModel constructor.
     *
     * @param User $user
     *
     * @internal param User $User
     *
     * @internal param $name
     * @internal param $profileImage
     */
    public function __construct(User $user)
    {
        $this->id = $user->getId();
        $this->name = (string) $user->getName();
        $this->profileImage = $user->getProfileImage();
        $this->rating = $user->getRating()->getValue();
    }
}