<?php

namespace Acme\App\ViewModels;


use Saferia\Domain\User\User;

class UserProfileViewModel
{
    public $name;
    public $city;
    public $description;
    public $qualities;
    public $profileImage;

    public function __construct(User $user)
    {
        $this->name = (string) $user->getName();
        $this->city = $user->getAddress()->city;
        $this->description = $user->getDescription();
        $this->profileImage = $user->getProfileImage();

        if ($user->getQualities() != null) {
            $this->qualities = explode("\n", $user->getQualities());
        }
    }
}