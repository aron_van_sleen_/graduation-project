<?php

namespace Acme\Domain\Authentication\Commands;

class ResetPassword
{
    public $userId;
    public $password;

    /**
     * ResetPasswordCommand constructor.
     *
     * @param $userId
     * @param $password
     */
    public function __construct($userId, $password)
    {
        $this->userId = $userId;
        $this->password = $password;
    }
}