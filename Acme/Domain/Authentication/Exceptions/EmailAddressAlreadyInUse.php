<?php
namespace Acme\Domain\Authentication\Exceptions;

use Exception;

class EmailAddressAlreadyInUse extends Exception {}