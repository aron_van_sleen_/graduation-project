<?php

namespace Acme\Domain\Authentication\Handlers;

use Acme\App\Services\HashingStrategy;
use Acme\Domain\Authentication\Commands\ResetPassword;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Password;
use Acme\Domain\User\UserRepository;

class ResetPasswordHandler
{
    public $repository;
    public $hasher;

    public function __construct(UserRepository $repository, HashingStrategy $hasher)
    {
        $this->repository = $repository;
        $this->hasher = $hasher;
    }

    public function handle(ResetPassword $command)
    {
        $user = $this->repository->findById($command->userId);

        $user->resetPassword(
            HashedPassword::fromHashingStrategy(new Password($command->password), $this->hasher)
        );

        $this->repository->store($user);

        // Possibly throw an event
    }
}