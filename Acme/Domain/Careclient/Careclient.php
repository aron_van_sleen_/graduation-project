<?php

namespace Saferia\Domain\Careclient;

use Doctrine\Common\Collections\ArrayCollection;
use Acme\Domain\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Careclient
 * @ORM\Entity
 */
final class Careclient extends User
{
    const CARECLIENT_TYPE = 'careclient';

    protected $discr = self::CARECLIENT_TYPE;

    /**
     * #ORM\OneToMany(targetEntity="Acme\Domain\Careclient\RelationshipWithCareprovider", mappedBy="careclient", cascade={"persist"})
     * @var ArrayCollection
     */
    private $careproviderRelationships;

    /**
     * @ORM\OneToMany(targetEntity="Acme\Domain\Hiring\InterviewRequest", mappedBy="careclient", cascade={"persist"})
     * @var ArrayCollection
     */
    private $interviewRequests;

    /**
     * @ORM\OneToOne(targetEntity="Acme\Domain\Careclient\Subscription", mappedBy="careclient", cascade={"persist"})
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id", nullable=true)
     * @var Subscription
     */
    private $subscription;

    /**
     * Careclient constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->interviewRequests = new ArrayCollection();
        $this->careproviderRelationships = new ArrayCollection();
    }

    /**
     * return bool
     */
    public function hasActiveSubscription()
    {
        if ($this->subscription === null) {
            return false;
        }

        return $this->subscription->isActive();
    }

    /**
     * @param Plan $plan
     */
    public function subscribeToPlan(Plan $plan)
    {
        $this->subscription = new Subscription($this, $plan);
    }
}