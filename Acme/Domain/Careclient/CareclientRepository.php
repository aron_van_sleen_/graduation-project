<?php
namespace Acme\Domain\Careclient;

use Acme\Domain\User\UserRepository;

interface CareclientRepository extends UserRepository
{
    /**
     * @return Careclient
     */
    public function findById($id);

    /**
     * @param Careclient|\Acme\Domain\Careprovider\Careprovider|\Acme\Domain\User\User $careclient
     *
     * @return mixed
     */
    public function store($careclient);
}