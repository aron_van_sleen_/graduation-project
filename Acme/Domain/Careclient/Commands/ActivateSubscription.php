<?php

namespace Acme\Domain\Careclient\Commands;


class ActivateSubscription
{
    public $userId;

    public $plan;

    /**
     * ActiveCareclientSubscription constructor.
     *
     * @param $userId
     * @param $plan
     */
    public function __construct($userId, $plan)
    {
        $this->userId = $userId;
        $this->plan = $plan;
    }
}