<?php

namespace Acme\Domain\Careclient\Handlers;

use Acme\Domain\Careclient\CareclientRepository;
use Acme\Domain\Careclient\Commands\ActivateSubscription;
use Acme\Domain\Careclient\Events\SubscriptionHasBeenActivated;
use Acme\Domain\Careclient\PlanRepository;

class ActivateSubscriptionHandler
{
    /**
     * @var CareclientRepository
     */
    private $repository;

    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * ActivateSubscriptionHandler constructor.
     *
     * @param $repository
     */
    public function __construct(CareclientRepository $repository, PlanRepository $planRepository)
    {
        $this->repository = $repository;
        $this->planRepository = $planRepository;
    }

    public function handle(ActivateSubscription $command)
    {
        $careclient = $this->repository->findById($command->userId);
        $plan = $this->planRepository->findByPlanName($command->plan);

        $careclient->subscribeToPlan($plan);

        $this->repository->store($careclient);

        event(new SubscriptionHasBeenActivated());
    }
}