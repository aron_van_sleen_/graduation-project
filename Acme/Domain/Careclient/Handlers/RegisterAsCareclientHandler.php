<?php
namespace Acme\Domain\Careclient\Handlers;

use Acme\App\Services\HashingStrategy;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careclient\CareclientRepository;
use Acme\Domain\Careclient\Commands\RegisterAsCareclient;
use Acme\Domain\Careclient\Events\CareclientHasRegistered;
use Acme\Domain\Authentication\Exceptions\EmailAddressAlreadyInUse;
use Acme\Domain\Shared\Address;
use Acme\Domain\Shared\Email;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Name;
use Acme\Domain\Shared\Password;
use Acme\Domain\Shared\PhoneNumber;
use Acme\Domain\User\User;

class RegisterAsCareclientHandler
{
    private $repository;
    private $hasher;

    public function __construct(CareclientRepository $repository, HashingStrategy $hasher)
    {
        $this->hasher = $hasher;
        $this->repository = $repository;
    }

    public function handle(RegisterAsCareclient $command)
    {
        // Build up the value objects
        $name = new Name($command->firstName, $command->lastName);
        $email = new Email($command->email);
        $password = new Password($command->password);
        $phoneNumber = new PhoneNumber($command->phoneNumber);
        $address = new Address(
            $command->streetName, $command->streetNumber, $command->postalCode, $command->city
        );

        if ($this->repository->findByEmail($email) instanceof User) {
            throw new EmailAddressAlreadyInUse('This e-mail address is already in use.');
        }

        $careclient = Careclient::register($email, HashedPassword::fromHashingStrategy($password, $this->hasher));
        $careclient->setName($name);
        $careclient->setAddress($address);
        $careclient->setDateOfBirth($command->dateOfBirth);
        $careclient->setPhoneNumber($phoneNumber);

        // Persist the newly registered careprovider
        $this->repository->store($careclient);

        event(new CareclientHasRegistered());
    }
}