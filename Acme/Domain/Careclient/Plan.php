<?php

namespace Acme\Domain\Careclient;

use Money\Money;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Plan
 * @ORM\Entity
 */
class Plan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique = true)
     * @var string
     */
    private $plan;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $price;

    public function  __construct(string $planName, Money $price)
    {
        $this->plan = $planName;
        $this->price = $price->getAmount();
    }

    /**
     * @param Money $newPrice
     */
    public function changePrice(Money $newPrice)
    {
        $this->price = $newPrice->getAmount();
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return Money::EUR($this->price);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->plan;
    }
}