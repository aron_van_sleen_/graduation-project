<?php

namespace Acme\Domain\Careclient;


interface PlanRepository
{
    public function findByPlanName(string $plan);
}