<?php

namespace Acme\Domain\Careclient;

use Doctrine\ORM\Mapping as ORM;
use Acme\Domain\Careprovider\Careprovider;

/**
 * @ORM\Entity
 */
final class RelationshipWithCareprovider
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Acme\Domain\Careclient\Careclient")
     * @ORM\JoinColumn(name="careclient_id", referencedColumnName="id")
     * @var Careclient
     */
    private $careclient;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Acme\Domain\Careprovider\Careprovider")
     * @ORM\JoinColumn(name="careprovider_id", referencedColumnName="id")
     * @var Careprovider
     */
    private $careprovider;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $partOfCareteam = false;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $favorited = false;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $approvedByCareprovider = false;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $approvedCareprovider = false;

    /**
     * RelationshipWithCareprovider constructor.
     *
     * @param Careclient   $careclient
     * @param Careprovider $careprovider
     */
    public function __construct(Careclient $careclient, Careprovider $careprovider)
    {
        $this->careclient = $careclient;
        $this->careprovider = $careprovider;
    }

    /**
     * Toggles the favorited status of a careprovider
     */
    public function favorite()
    {
        $this->favorited = ! $this->favorited;
    }

    /**
     * @param bool $evaluation
     */
    public function evaluateCareprovider(bool $evaluation)
    {
        $this->approvedCareprovider = $evaluation;
    }

    /**
     * @param bool $evaluation
     */
    public function evaluateCareclient(bool $evaluation)
    {
        $this->approvedByCareprovider = $evaluation;
    }

    /**
     * @return bool
     */
    public function isPartOfCareteam()
    {
        return $this->partOfCareteam;
    }

    /** 
     * @return bool
     */
    public function hasMutualApproval()
    {
        return $this->approvedByCareprovider && $this->approvedCareprovider;
    }

    /**
     * @return Careclient
     */
    public function getCareclient()
    {
        return $this->careclient;
    }

    /**
     * @return Careprovider
     */
    public function getCareprovider()
    {
        return $this->careprovider;
    }
}