<?php
namespace Acme\Domain\Careclient;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Subscription
 * @ORM\Entity
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Acme\Domain\Careclient\Careclient", inversedBy="subscription")
     * @var Careclient
     */
    private $careclient;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     * @var Carbon
     */
    private $activeSince;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     * @var Carbon
     */
    private $expiresOn;

    /**
     * @ORM\ManyToOne(targetEntity="Acme\Domain\Careclient\Plan", cascade={"persist"})
     * @ORM\JoinColumn(name="plan", referencedColumnName="id")
     * @var Plan
     */
    private $plan;

    /**
     * Creates a new active subscription
     *
     * @param Careclient $careclient
     * @param Plan       $plan
     */
    public function __construct(Careclient $careclient, Plan $plan)
    {
        $this->careclient = $careclient;
        $this->plan = $plan;
        $this->activeSince = Carbon::now();
        $this->expiresOn = Carbon::now()->addMonths(12);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        if ($this->expiresOn === null) {
            return false;
        }

        return Carbon::now() < $this->expiresOn;
    }

    /**
     * @return Carbon
     */
    public function activeSince()
    {
        return $this->activeSince();
    }

    /**
     * @return Carbon
     */
    public function expiresOn()
    {
        return $this->expiresOn();
    }
}