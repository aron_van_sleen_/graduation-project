<?php

namespace Acme\Domain\Careprovider;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Acme\Domain\Shared\TimeRange;

/**
 * @ORM\Entity
 * @ORM\Table(name="availability")
 */
final class Availability
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Acme\Domain\Careprovider\Careprovider", inversedBy="availability")
     * @ORM\JoinColumn(name="careprovider_id", referencedColumnName="id")
     * @var Careprovider
     */
    private $careprovider;

    /**
     * @ORM\Column(type="date")
     * @var Carbon
     */
    private $date;

    /**
     * @ORM\Embedded(class = "Acme\Domain\Shared\TimeRange", columnPrefix = false)
     * @var  TimeRange
     */
    private $timeRange;

    /**
     * Availability constructor.
     *
     * @param Carbon       $date
     * @param TimeRange    $timeRange
     * @param Careprovider $careprovider
     */
    public function __construct(Carbon $date, TimeRange $timeRange, Careprovider $careprovider)
    {
        $date->setTime(0, 0);
        if ($date->lt(Carbon::today()->setTime(0, 0))) {
            throw new \InvalidArgumentException('Cannot declare availability that is in the past');
        }

        $this->date = $date;
        $this->timeRange = $timeRange;
        $this->careprovider = $careprovider;
    }

    /**
     * @return Carbon
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return TimeRange
     */
    public function getTimeRange()
    {
        return $this->timeRange;
    }
}