<?php

namespace Acme\Domain\Careprovider;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;
use Money\Money;
use Acme\Domain\Careprovider\Exceptions\HourlyRateCannotExceedTheMaximumRateOfTheLevel;
use Acme\Domain\Hiring\InterviewRequest;
use Acme\Domain\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Careprovider extends User
{
    const CAREPROVIDER_TYPE = 'careprovider';
    const MAX_AVAILABILITY_PER_DATE = 3;

    protected $discr = self::CAREPROVIDER_TYPE;

    /**
     * @ORM\OneToMany(targetEntity="Acme\Domain\Careprovider\Availability", mappedBy="careprovider", fetch="EXTRA_LAZY", cascade={"persist"}, orphanRemoval=true)
     * @var ArrayCollection|Availability[]
     */
    private $availability;

    /**
     * @ORM\Column(type="integer")
     * @var Money
     */
    private $hourlyRate;

    /**
     * @ORM\ManyToOne(targetEntity="Acme\Domain\Careprovider\Level", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", )
     * @var Level
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="Acme\Domain\Hiring\InterviewRequest", mappedBy="careprovider", orphanRemoval=true, cascade={"persist"})
     */
    private $interviewRequests;

    protected function __construct()
    {
        parent::__construct();
        $this->availability = new ArrayCollection();
        $this->interviewRequests = new ArrayCollection();
    }

    /**
     * @param Collection $availability
     */
    public function stateAvailability(Collection $availability)
    {
        if ($availability->count() > self::MAX_AVAILABILITY_PER_DATE) {
            throw new \InvalidArgumentException('U can only state three availability per date');
        }

        $availability->each(function ($item) {
            $this->availability->add($item);
        });
    }

    /**
     * @param Availability $availability
     */
    public function unstateAvailability(Availability $availability)
    {
        $this->availability->removeElement($availability);
    }

    /**
     * @param Money $hourlyRate
     *
     * @throws HourlyRateCannotExceedTheMaximumRateOfTheLevel
     */
    public function setHourlyRate(Money $hourlyRate)
    {
        if ($hourlyRate->greaterThan($this->level->getMaximumHourlyRate())) {
            throw new HourlyRateCannotExceedTheMaximumRateOfTheLevel("The given rate is higher than the maximum hourly rate of this careprovider");
        }

        $this->hourlyRate = $hourlyRate->getAmount();
    }

    /**
     * @param Level $level
     */
    public function setLevel(Level $level)
    {
        $this->level = $level;
    }

    /**
     * @return Money
     */
    public function getHourlyRate()
    {
        return Money::EUR($this->hourlyRate);
    }

    /**
     * @return Level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return ArrayCollection|Availability[]
     */
    public function getAvailability()
    {
        return $this->availability;
    }
}   