<?php

namespace Acme\Domain\Careprovider;

use Acme\Domain\Hiring\CareproviderRequest;
use Acme\Domain\User\UserRepository;


interface CareproviderRepository extends UserRepository
{
    /**
     * @param $id
     *
     * @return Careprovider
     */
    public function findById($id);

    /**
     * @param $careproviderId
     * @param $interviewRequestId
     *
     * @return CareproviderRequest
     */
    public function findInterviewRequestById($careproviderId, $interviewRequestId);

    /**
     * @param $dates
     * @param $careproviderId
     *
     * @return mixed
     */
    public function findCareproviderAvailabilityByDates($careproviderId, array $dates);

    /**
     * @param Careprovider $careprovider
     *
     * @return void
     */
    public function store($careprovider);
}