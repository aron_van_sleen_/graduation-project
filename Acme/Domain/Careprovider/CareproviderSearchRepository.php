<?php

namespace Acme\Domain\Careprovider;

use Carbon\Carbon;
use Money\Money;
use Acme\Domain\Shared\GeoLocation;
use Acme\Domain\Shared\TimeRange;

interface CareproviderSearchRepository
{
    public function all();
    public function paginate($page, $perPage);
    public function whereWithinRadiusOfLocation(GeoLocation $location, $radius);
    public function whereAvailable(Carbon $date, TimeRange $timeRange);
    public function whereNotHavingCharacteristics(array $characteristics);
    public function whereBelowPrice(Money $maxHourlyRate);
}