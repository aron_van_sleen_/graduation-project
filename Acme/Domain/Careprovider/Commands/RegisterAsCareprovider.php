<?php

namespace Acme\Domain\Careprovider\Commands;

use Carbon\Carbon;

class RegisterAsCareprovider {
    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $dateOfBirth;
    public $streetName;
    public $streetNumber;
    public $postalCode;
    public $city;
    public $phoneNumber;

    public function __construct($data)
    {
        $this->firstName = $data['first_name'];
        $this->lastName = $data['last_name'];

        $this->email = $data['email'];
        $this->password = $data['password'];

        $this->dateOfBirth = Carbon::createFromDate(
            $data['birth_year'], $data['birth_month'], $data['birth_day']
        );

        $this->streetName = $data['street_name'];
        $this->streetNumber = $data['street_number'];
        $this->postalCode = $data['postal_code'];
        $this->city = $data['city'];

        $this->phoneNumber = $data['phone_number'];
    }
}