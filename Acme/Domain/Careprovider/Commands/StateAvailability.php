<?php

namespace Acme\Domain\Careprovider\Commands;

use Carbon\Carbon;

class StateAvailability
{
    public $careproviderId;

    /** @var  Carbon */
    public $dates;

    /** @var array */
    public $timeRanges;

    /**
     * StateAvailabilityCommand constructor.
     *
     * @param        $careproviderId
     * @param        $dates
     * @param array  $timeRanges
     */
    public function __construct($careproviderId, $dates, array $timeRanges)
    {
        $this->careproviderId = $careproviderId;
        $this->dates = $dates;
        $this->timeRanges = $timeRanges;
    }


}