<?php

namespace Acme\Domain\Careprovider\Commands;

final class UnstateAvailability
{
    public $careproviderId;
    public $dates;

    /**
     * UnstateAvailability constructor.
     *
     * @param $careproviderId
     * @param $dates
     */
    public function __construct($careproviderId, $dates)
    {
        $this->careproviderId = $careproviderId;
        $this->dates = $dates;
    }
}