<?php

namespace Acme\Domain\Careprovider\Commands;


class UpdateCareproviderProperties
{
    public $careproviderId;
    public $hourlyRate;
    public $qualities;

    /**
     * EditCareproviderPropertiesCommand constructor.
     *
     * @param $careproviderId
     * @param $hourlyRate
     */
    public function __construct($careproviderId, $hourlyRate)
    {
        $this->careproviderId = $careproviderId;
        $this->hourlyRate = $hourlyRate;
    }
}