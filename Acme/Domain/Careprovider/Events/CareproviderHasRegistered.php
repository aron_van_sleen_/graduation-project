<?php
namespace Acme\Domain\Careprovider\Events;


class CareproviderHasRegistered
{
    public $id;
    public $name;
    public $address;
    public $slug;
    public $hourlyRate;

    /**
     * CareproviderHasRegistered constructor.
     *
     * @param $id
     * @param $name
     * @param $address
     * @param $hourlyRate
     */
    public function __construct($id, $name, $address, $slug, $hourlyRate)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->slug = $slug;
        $this->hourlyRate = $hourlyRate;
    }


} 