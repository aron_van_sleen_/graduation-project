<?php

namespace Acme\Domain\Careprovider\Events;


use Carbon\Carbon;
use Illuminate\Support\Collection;

class CareproviderHasStatedAvailability
{
    public $careproviderId;

    /** @var  Collection */
    public $dates;

    /** @var  Collection */
    public $availability;

    /** @var  Collection */
    public $timeRanges;

    /**
     * CareproviderHasStatedAvailability constructor.
     *
     * @param $careproviderId
     * @param $dates
     * @param $timeRanges
     */
    public function __construct($careproviderId, $dates, $timeRanges)
    {
        $this->careproviderId = $careproviderId;
        $this->dates = $dates;
        $this->timeRanges = $timeRanges;
    }
}