<?php

namespace Acme\Domain\Careprovider\Events;


class CareproviderHasUnstatedAvailability
{
    public $careproviderId;
    public $dates;

    /**
     * CareproviderHasUnstatedAvailability constructor.
     *
     * @param $careproviderId
     * @param $dates
     */
    public function __construct($careproviderId, $dates)
    {
        $this->careproviderId = $careproviderId;
        $this->dates = $dates;
    }
}