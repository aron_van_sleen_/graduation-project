<?php

namespace Acme\Domain\Careprovider\Events;


class CareproviderHasUpdatedProfile
{
    public $id;
    public $name;
    public $address;
    public $slug;
    public $hourlyRate;
    public $characteristics;
    public $profileImage;

    /**
     * CareproviderHasUpdatedProfile constructor.
     *
     * @param       $id
     * @param       $name
     * @param       $address
     * @param       $slug
     * @param       $hourlyRate
     * @param null  $profileImage
     * @param array $characteristics
     */
    public function __construct($id, $name, $address, $slug, $hourlyRate, $characteristics = [], $profileImage = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->slug = $slug;
        $this->hourlyRate = $hourlyRate;
        $this->characteristics = $characteristics;
        $this->profileImage = $profileImage;
    }
}