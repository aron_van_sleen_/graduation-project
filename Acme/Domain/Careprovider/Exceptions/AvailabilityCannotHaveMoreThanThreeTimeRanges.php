<?php

namespace Saferia\Domain\Careprovider\Exceptions;


class AvailabilityCannotHaveMoreThanThreeTimeRanges extends \Exception {}