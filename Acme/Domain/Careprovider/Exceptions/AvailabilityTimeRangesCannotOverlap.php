<?php

namespace Saferia\Domain\Careprovider\Exceptions;


class AvailabilityTimeRangesCannotOverlap extends \Exception {}