<?php

namespace Saferia\Domain\Careprovider\Exceptions;


class HourlyRateCannotExceedTheMaximumRateOfTheLevel extends \Exception {}