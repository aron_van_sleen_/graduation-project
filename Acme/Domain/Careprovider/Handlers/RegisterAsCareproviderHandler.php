<?php

namespace Acme\Domain\Careprovider\Handlers;

use Acme\Domain\Careprovider\LevelRepository;
use Acme\Domain\Authentication\Exceptions\EmailAddressAlreadyInUse;
use Acme\App\Services\HashingStrategy;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Careprovider\Commands\RegisterAsCareprovider;
use Acme\Domain\Careprovider\Events\CareproviderHasRegistered;
use Acme\Domain\Shared\Address;
use Acme\Domain\Shared\Email;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Name;
use Acme\Domain\Shared\Password;
use Acme\Domain\Shared\PhoneNumber;
use Acme\Domain\User\User;

final class RegisterAsCareproviderHandler
{
    private $repository;
    private $hasher;
    private $levelRepository;

    public function __construct(CareproviderRepository $repository, LevelRepository $levelRepository,  HashingStrategy $hasher)
    {
        $this->repository = $repository;
        $this->hasher = $hasher;
        $this->levelRepository = $levelRepository;
    }

    public function handle(RegisterAsCareprovider $command)
    {
        // Build up the value objects
        $name = new Name($command->firstName, $command->lastName);
        $email = new Email($command->email);
        $password = new Password($command->password);
        $phoneNumber = new PhoneNumber($command->phoneNumber);
        $address = new Address(
            $command->streetName, $command->streetNumber, $command->postalCode, $command->city
        );

        // Check for email uniqueness
        if ($this->repository->findByEmail($email) instanceof User) {
            throw new EmailAddressAlreadyInUse('This e-mail address is already in use.');
        }

        $careprovider = Careprovider::register($email, HashedPassword::fromHashingStrategy($password, $this->hasher));
        $careprovider->setName($name);
        $careprovider->setAddress($address);
        $careprovider->setDateOfBirth($command->dateOfBirth);
        $careprovider->setPhoneNumber($phoneNumber);

        // Set the default level and initial hourlyRate
        $careprovider->setLevel($this->levelRepository->findDefaultLevel());
        $careprovider->setHourlyRate($careprovider->getLevel()->getMaximumHourlyRate());

        // Persist the newly registered careprovider
        $this->repository->store($careprovider);

        // Raise event of the newly registered Careprovider
        event(new CareproviderHasRegistered(
            $careprovider->getId(),
            $careprovider->getName(),
            $careprovider->getAddress(),
            $careprovider->getSlug(),
            $careprovider->getHourlyRate()->getAmount()
        ));
    }
}
