<?php

namespace Acme\Domain\Careprovider\Handlers;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Acme\Domain\Careprovider\Availability;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Careprovider\Commands\StateAvailability;
use Acme\Domain\Careprovider\Events\CareproviderHasStatedAvailability;
use Acme\Domain\Shared\TimeRange;

class StateAvailabilityHandler
{
    protected $repository;

    public function __construct(CareproviderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(StateAvailability $command)
    {
        $careprovider = $this->repository->findById($command->careproviderId);
        $dates = $command->dates;

        // Make sure it is always an array
        if ( ! is_array($dates)) {
            $dates = [$dates];
        }

        $dates = array_map(function ($date) {
            return (new Carbon($date))->setTime(0, 0);
        }, $dates);

        // Remove availability with the same dates so u 'restate' it and keeping the maximum of three per day.
        $availabilityToRemove = $this->repository->findCareproviderAvailabilityByDates($command->careproviderId, $dates);
        foreach ($availabilityToRemove as $toRemove) {
            $careprovider->unstateAvailability($toRemove);
        }

        // Change array of strings to collection of value objects
        $timeRanges = collect($command->timeRanges)->map(function ($timeRange) {
            return new TimeRange($timeRange["from"], $timeRange["to"]);
        });

        foreach ($dates as $date) {
            $availability = $this->createAvailabilityCollection($date, $timeRanges, $careprovider);

            $careprovider->stateAvailability($availability);
        }

        $this->repository->store($careprovider);

        event(new CareproviderHasStatedAvailability($command->careproviderId, $dates, $timeRanges));
    }

    /**
     * @param Carbon $date
     * @param        $timeRanges
     * @param        $careprovider
     *
     * @return Collection
     */
    private function createAvailabilityCollection(Carbon $date, $timeRanges, $careprovider)
    {
        $availability = new Collection();

        foreach ($timeRanges as $timeRange) {
            $availability->push(new Availability($date, $timeRange, $careprovider));
        };

        return $availability;
    }
}