<?php

namespace Acme\Domain\Careprovider\Handlers;

use Carbon\Carbon;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Careprovider\Commands\UnstateAvailability;
use Acme\Domain\Careprovider\Events\CareproviderHasUnstatedAvailability;

final class UnstateAvailabilityHandler
{
    private $repository;

    /**
     * UnstateAvailabilityHandler constructor.
     *
     * @param $repository
     */
    public function __construct(CareproviderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(UnstateAvailability $command)
    {
        // Make sure its an array
        $dates = $command->dates;
        if ( ! is_array($dates)) {
            $dates = [$dates];
        }

        $dates = array_map(function ($date) {
            return (new Carbon($date))->setTime(0, 0);
        }, $dates);

        $careprovider = $this->repository->findById($command->careproviderId);
        $availability = $this->repository->findCareproviderAvailabilityByDates($command->careproviderId, $dates);

        foreach ($availability as $item) {
            $careprovider->unstateAvailability($item);
        }

        $this->repository->store($careprovider);

        event(new CareproviderHasUnstatedAvailability($command->careproviderId, $dates));
    }
}