<?php

namespace Acme\Domain\Careprovider\Handlers;

use Money\Money;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Careprovider\Commands\UpdateCareproviderProperties;
use Acme\Domain\Careprovider\Events\CareproviderHasUpdatedProfile;

class UpdateCareproviderPropertiesHandler
{
    private $repository;

    /**
     * EditCareproviderPropertiesHandler constructor.
     *
     * @param $repository
     */
    public function __construct(CareproviderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(UpdateCareproviderProperties $command)
    {
        $careprovider = $this->repository->findById($command->careproviderId);

        $careprovider->setHourlyRate(Money::EUR((int) $command->hourlyRate));

        $this->repository->store($careprovider);

        event(new CareproviderHasUpdatedProfile(
            $careprovider->getId(),
            $careprovider->getName(),
            $careprovider->getAddress(),
            $careprovider->getSlug(),
            $careprovider->getHourlyRate()->getAmount(),
            $careprovider->getCharacteristics(),
            $careprovider->getProfileImage()
        ));
    }
}