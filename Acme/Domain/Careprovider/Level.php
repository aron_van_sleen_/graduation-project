<?php

namespace Acme\Domain\Careprovider;

use Money\Money;
use Doctrine\ORM\Mapping as ORM;
use Acme\Domain\Shared\Rateable;

/**
 * @ORM\Entity
 */
class Level
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique = true)
     */
    private $levelName;

    /**
     * @ORM\Column(type="integer")
     */
    private $maximumHourlyRate;
  
    public function __construct($levelName, Money $maximumHourlyRate)
    {
        $this->levelName = $levelName;
        $this->maximumHourlyRate = $maximumHourlyRate->getAmount();
    }

    /**
     * @return string
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * @return Money
     */
    public function getMaximumHourlyRate()
    {
        return Money::EUR($this->maximumHourlyRate);
    }
}