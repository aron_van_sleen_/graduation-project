<?php

namespace Acme\Domain\Careprovider;

use Money\Money;

interface LevelRepository
{
    /**
     * @param $id
     *
     * @return Level
     */
    public function findById($id);

    /**
     * @return Level
     */
    public function findDefaultLevel();

    /**
     * @return Money
     */
    public function findHighestMaximumHourlyRate();
}