<?php

namespace Acme\Domain\Hiring;


use Gedmo\Timestampable\Traits\Timestampable;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"interview" = "Acme\Domain\Hiring\InterviewRequest", "hiring" = "Acme\Domain\Hiring\HiringRequest"})
 */
abstract class CareproviderRequest
{
    use Timestampable;

    const STATE_PENDING = 'pending';
    const STATE_ACCEPTED = 'accepted';
    const STATE_DECLINED = 'declined';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var  int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Acme\Domain\Careclient\Careclient", fetch="EAGER", inversedBy="interviewRequests")
     * @ORM\JoinColumn(name="careclient_id", referencedColumnName="id")
     * @var  Careclient
     */
    protected $careclient;

    /**
     * @ORM\ManyToOne(targetEntity="Acme\Domain\Careprovider\Careprovider", fetch="EAGER", inversedBy="interviewRequests")
     * @ORM\JoinColumn(name="careprovider_id", referencedColumnName="id")
     * @var  Careprovider
     */
    protected $careprovider;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $state = self::STATE_PENDING;

    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $comment;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Careclient
     */
    public function getCareclient()
    {
        return $this->careclient;
    }

    /**
     * @return Careprovider
     */
    public function getCareprovider()
    {
        return $this->careprovider;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }
}