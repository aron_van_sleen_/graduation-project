<?php

namespace Acme\Domain\Hiring;


interface CareproviderRequestRepository
{
    public function findById($id);
    public function findPendingInterviewRequest($careclientId, $careproviderId);
    public function store(CareproviderRequest $request);
    public function remove(CareproviderRequest $request);
}