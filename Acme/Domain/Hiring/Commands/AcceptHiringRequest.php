<?php

namespace Acme\Domain\Hiring\Commands;

final class AcceptHiringRequest
{
    public $careproviderId;
    public $hiringRequestId;

    /**
     * AcceptHiringRequest constructor.
     *
     * @param $careproviderId
     * @param $hiringRequestId
     */
    public function __construct($careproviderId, $hiringRequestId)
    {
        $this->careproviderId = $careproviderId;
        $this->hiringRequestId = $hiringRequestId;
    }
}