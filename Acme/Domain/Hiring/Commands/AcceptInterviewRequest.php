<?php

namespace Acme\Domain\Hiring\Commands;

class AcceptInterviewRequest
{
    public $careproviderId;
    public $interviewRequestId;
    public $acceptedDate;

    /**
     * AcceptInterviewRequest constructor.
     *
     * @param $careproviderId
     * @param $interviewRequestId
     * @param $acceptedDate
     */
    public function __construct($careproviderId, $interviewRequestId, $acceptedDate)
    {
        $this->careproviderId = $careproviderId;
        $this->interviewRequestId = $interviewRequestId;
        $this->acceptedDate = $acceptedDate;
    }


}