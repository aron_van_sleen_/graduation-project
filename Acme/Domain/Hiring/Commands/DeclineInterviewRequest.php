<?php

namespace Acme\Domain\Hiring\Commands;


class DeclineInterviewRequest
{
    public $careproviderId;
    public $interviewRequestId;

    /**
     * DeclineInterviewRequest constructor.
     *
     * @param $careproviderId
     * @param $interviewRequestId
     */
    public function __construct($careproviderId, $interviewRequestId)
    {
        $this->careproviderId = $careproviderId;
        $this->interviewRequestId = $interviewRequestId;
    }


}