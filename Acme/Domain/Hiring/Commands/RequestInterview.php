<?php

namespace Acme\Domain\Hiring\Commands;

class RequestInterview
{
    public $careclientId;
    public $careproviderId;
    public $requestedDates;

    /**
     * RequestInterview constructor.
     *
     * @param $careclientId
     * @param $careproviderId
     * @param $requestedDates
     */
    public function __construct($careclientId, $careproviderId, $requestedDates)
    {
        $this->careclientId = $careclientId;
        $this->careproviderId = $careproviderId;
        $this->requestedDates = $requestedDates;
    }


}