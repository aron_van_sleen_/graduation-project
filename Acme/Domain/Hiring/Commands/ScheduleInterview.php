<?php

namespace Acme\Domain\Hiring\Commands;


class ScheduleInterview
{
    public $careproviderId;
    public $careclientId;
    public $scheduledDate;

    /**
     * ScheduleInterview constructor.
     *
     * @param $careproviderId
     * @param $careclientId
     * @param $scheduledDate
     */
    public function __construct($careproviderId, $careclientId, $scheduledDate)
    {
        $this->careproviderId = $careproviderId;
        $this->careclientId = $careclientId;
        $this->scheduledDate = $scheduledDate;
    }


}