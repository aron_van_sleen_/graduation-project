<?php

namespace Acme\Domain\Hiring\Events;


class HiringRequestWasAccepted
{
    public $careclient;
    public $careprovider;
    public $date;
    public $duration;

    /**
     * HiringRequestWasAccepted constructor.
     *
     * @param $careclient
     * @param $careprovider
     * @param $date
     * @param $duration
     */
    public function __construct($careclient, $careprovider, $date, $duration)
    {
        $this->careclient = $careclient;
        $this->careprovider = $careprovider;
        $this->date = $date;
        $this->duration = $duration;
    }


}