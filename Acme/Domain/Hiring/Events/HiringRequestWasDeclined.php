<?php

namespace Acme\Domain\Hiring\Events;


class HiringRequestWasDeclined
{
    public $careclient;
    public $careprovider;

    /**
     * HiringRequestWasDeclined constructor.
     *
     * @param $careclient
     * @param $careprovider
     */
    public function __construct($careclient, $careprovider)
    {
        $this->careclient = $careclient;
        $this->careprovider = $careprovider;
    }


}