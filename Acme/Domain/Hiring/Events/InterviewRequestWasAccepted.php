<?php

namespace Acme\Domain\Hiring\Events;

use Carbon\Carbon;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;

final class InterviewRequestWasAccepted
{
    public $careprovider;
    public $careclient;
    public $acceptedDate;

    /**
     * InterviewRequestWasAccepted constructor.
     *
     * @param $careprovider
     * @param $careclient
     * @param $acceptedDate
     */
    public function __construct(Careprovider $careprovider, Careclient $careclient, Carbon $acceptedDate)
    {
        $this->careprovider = $careprovider;
        $this->careclient = $careclient;
        $this->acceptedDate = $acceptedDate;
    }
}