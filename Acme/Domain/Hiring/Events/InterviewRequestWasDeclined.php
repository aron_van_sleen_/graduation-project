<?php

namespace Acme\Domain\Hiring\Events;

use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;

class InterviewRequestWasDeclined
{
    public $careclient;
    public $careprovider;

    /**
     * InterviewRequestWasDeclined constructor.
     *
     * @param $careclient
     * @param $careprovider
     */
    public function __construct(Careclient $careclient, Careprovider $careprovider)
    {
        $this->careclient = $careclient;
        $this->careprovider = $careprovider;
    }
}