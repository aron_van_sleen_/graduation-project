<?php

namespace Acme\Domain\Hiring\Events;


use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;

class InterviewWasRequested
{
    public function __construct(Careclient $careclient, Careprovider $careprovider)
    {
        $this->careclient = $careclient;
        $this->careprovider = $careprovider;
    }
}