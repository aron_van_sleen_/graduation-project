<?php

namespace Acme\Domain\Hiring\Exceptions;

class InterviewRequestAlreadyPending extends \Exception {};