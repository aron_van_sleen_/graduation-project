<?php

namespace Acme\Domain\Hiring\Exceptions;

class InterviewRequestDoesNotExists extends \Exception {}