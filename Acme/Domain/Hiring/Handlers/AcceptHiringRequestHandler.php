<?php

namespace Acme\Domain\Hiring\Handlers;

use Illuminate\Support\Facades\Gate;
use Acme\Domain\Hiring\CareproviderRequestRepository;
use Acme\Domain\Hiring\Commands\AcceptHiringRequest;
use Acme\Domain\Hiring\Events\HiringRequestWasAccepted;

class AcceptHiringRequestHandler
{
    private $repository;

    /**
     * AcceptHiringRequestHandler constructor.
     *
     * @param $repository
     */
    public function __construct(CareproviderRequestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(AcceptHiringRequest $command)
    {
        $request = $this->repository->findById($command->hiringRequestId);

        if (Gate::denies('accept', $request)) {
            abort(403);
        }

        $request->accept();

        $this->repository->store($request);

        event(new HiringRequestWasAccepted($request->getCareclient(), $request->getCareprovider(), $request->getSuggestedDate(), $request->getDuration()));
    }
}