<?php

namespace Acme\Domain\Hiring\Handlers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Acme\Domain\Hiring\CareproviderRequestRepository;
use Acme\Domain\Hiring\Commands\AcceptInterviewRequest;
use Acme\Domain\Hiring\Events\InterviewRequestWasAccepted;

final class AcceptInterviewRequestHandler
{
    private $repository;

    public function __construct(CareproviderRequestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(AcceptInterviewRequest $command)
    {
        $request = $this->repository->findById($command->interviewRequestId);
        $acceptedDate = new Carbon($command->acceptedDate);

        if (Gate::denies('accept', $request)) {
            abort(403);
        }

        $request->accept($acceptedDate);

        $this->repository->store($request);

        event(new InterviewRequestWasAccepted($request->getCareprovider(), $request->getCareclient(), $acceptedDate));
    }
}