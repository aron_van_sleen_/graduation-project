<?php

namespace Acme\Domain\Hiring\Handlers;

use Illuminate\Support\Facades\Gate;
use Acme\Domain\Hiring\CareproviderRequestRepository;
use Acme\Domain\Hiring\Commands\DeclineHiringRequest;
use Acme\Domain\Hiring\Events\HiringRequestWasDeclined;

class DeclineHiringRequestHandler
{
    private $repository;

    /**
     * DeclineHiringRequestHandler constructor.
     *
     * @param $repository
     */
    public function __construct(CareproviderRequestRepository $repository)
    {
        $this->repository = $repository;
    }


    public function handle(DeclineHiringRequest $command)
    {
        $request = $this->repository->findById($command->hiringRequestId);

        if (Gate::denies('decline', $request)) {
            abort(403);
        }

        $request->decline();

        $this->repository->store($request);

        event(new HiringRequestWasDeclined($request->getCareclient(), $request->getCareprovider()));
    }
}