<?php

namespace Acme\Domain\Hiring\Handlers;

use Illuminate\Support\Facades\Gate;
use Acme\Domain\Hiring\CareproviderRequestRepository;
use Acme\Domain\Hiring\Commands\DeclineInterviewRequest;
use Acme\Domain\Hiring\Events\InterviewRequestWasDeclined;

final class DeclineInterviewRequestHandler
{
    private $repository;

    public function __construct(CareproviderRequestRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function handle(DeclineInterviewRequest $command)
    {
        $request = $this->repository->findById($command->interviewRequestId);

        if (Gate::denies('decline', $request)) {
            abort(403);
        }

        $request->decline();

        $this->repository->store($request);

        event(new InterviewRequestWasDeclined($request->getCareclient(), $request->getCareprovider()));
    }
}