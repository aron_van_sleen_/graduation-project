<?php

namespace Acme\Domain\Hiring\Handlers;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Acme\Domain\Careclient\CareclientRepository;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Hiring\CareproviderRequest;
use Acme\Domain\Hiring\CareproviderRequestRepository;
use Acme\Domain\Hiring\Commands\RequestInterview;
use Acme\Domain\Hiring\Events\InterviewWasRequested;
use Acme\Domain\Hiring\Exceptions\InterviewRequestAlreadyPending;
use Acme\Domain\Hiring\InterviewRequest;

class RequestInterviewHandler
{
    private $repository;
    private $careclientRepository;
    private $careproviderRepository;

    /**
     * RequestInterviewHandler constructor.
     *
     * @param CareclientRepository   $careclientRepository
     * @param CareproviderRepository $careproviderRepository
     */
    public function __construct(CareproviderRequestRepository $repository, CareclientRepository $careclientRepository, CareproviderRepository $careproviderRepository)
    {
        $this->repository = $repository;
        $this->careclientRepository = $careclientRepository;
        $this->careproviderRepository = $careproviderRepository;
    }

    public function handle(RequestInterview $command)
    {
        // check if there are any pending requests.
        if (count($this->repository->findPendingInterviewRequest($command->careclientId, $command->careproviderId)) > 0) {
            throw new InterviewRequestAlreadyPending('This careclient already has a pending interview request with the careprovider.');
        }

        $careclient = $this->careclientRepository->findById($command->careclientId);
        $careprovider = $this->careproviderRepository->findById($command->careproviderId);

        $dates = new Collection();
        foreach($command->requestedDates as $date) {
            $dates->push(new Carbon($date));
        }

        $request = new InterviewRequest($careclient, $careprovider, $dates);

        $this->repository->store($request);

        event(new InterviewWasRequested($careclient, $careprovider));
    }
}