<?php

namespace Acme\Domain\Hiring\Handlers;

use Acme\Domain\Careclient\CareclientRepository;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Hiring\Commands\ScheduleInterview;
use Acme\Domain\Hiring\Interview;
use Acme\Domain\Hiring\InterviewRepository;

class ScheduleInterviewHandler
{
    public $careproviderRepository;
    public $careclientRepository;
    public $repository;

    public function __construct(InterviewRepository $repository, CareproviderRepository $careproviderRepository, CareclientRepository $careclientRepository)
    {
        $this->careproviderRepository = $careproviderRepository;
        $this->careclientRepository = $careclientRepository;

        $this->repository = $repository;
    }

    public function handle(ScheduleInterview $command)
    {
        $careclient = $this->careclientRepository->findById($command->careclientId);
        $careprovider = $this->careproviderRepository->findById($command->careproviderId);

        $interview = new Interview($command->scheduledDate);

        $this->repository->store($interview);

        // Possible event
    }
}