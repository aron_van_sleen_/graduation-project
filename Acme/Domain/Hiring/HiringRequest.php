<?php

namespace Acme\Domain\Hiring;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Acme\Domain\Careclient\RelationshipWithCareprovider;
use Acme\Domain\Shared\TimeRange;

/** @ORM\Entity */
final class HiringRequest extends CareproviderRequest
{
    /**
     * @ORM\Column(type="datetime")
     * @var Carbon
     */
    private $suggestedDate;

    /**
     * @ORM\Column(type="integer")seq
     * @var integer
     */
    private $duration;

    public function __construct(RelationshipWithCareprovider $relationship, Carbon $date, $duration, $comment = null)
    {
        if ( ! $relationship->hasMutualApproval()) {
            throw new \InvalidArgumentException('There must be mutual approval before a careprovider can be hired');
        }

        $this->careclient = $relationship->getCareclient();
        $this->careprovider = $relationship->getCareprovider();
        $this->suggestedDate = $date;
        $this->duration = $duration;
        $this->comment = $comment;
    }

    public function accept()
    {
        $this->state = self::STATE_ACCEPTED;
    }

    public function decline()
    {
        $this->state = self::STATE_DECLINED;
    }

    /**
     * @return Carbon
     */
    public function getSuggestedDate()
    {
        return $this->suggestedDate;
    }

    /**
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }


}
