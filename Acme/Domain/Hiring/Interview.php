<?php

namespace Acme\Domain\Hiring;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Shared\Rateable;

/**
 * @ORM\Entity
 * @package Acme\Domain\Hiring
 */
final class Interview
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @var Carbon
     */
    private $scheduledOn;

    public function __construct(Carbon $scheduledOn)
    {
        $this->scheduledOn = $scheduledOn;
    }
}