<?php

namespace Acme\Domain\Hiring;

interface InterviewRepository
{
    public function findById($id);
    public function store(Interview $interview);
}