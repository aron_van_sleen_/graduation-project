<?php
namespace Acme\Domain\Hiring;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Support\Collection;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;

/** @ORM\Entity */
final class InterviewRequest extends CareproviderRequest
{
    const REQUIRED_AMOUNT_OF_DATES = 3;

    /**
     * @ORM\Column(type="array")
     * @var  Collection|Carbon[]
     */
    private $requestedDates;

    public function __construct(Careclient $byCareclient, Careprovider $forCareprovider, Collection $requestedDates, $comment = null)
    {
        $this->careclient = $byCareclient;
        $this->careprovider = $forCareprovider;

        // Make sure the dates are in the future
        $requestedDates->each(function (Carbon $date) {
            if ($date->lt(Carbon::now())) {
                throw new \InvalidArgumentException('A requested date cannot be in the past.');
            }
        });

        if ($requestedDates->count() !== self::REQUIRED_AMOUNT_OF_DATES) {
            throw new \InvalidArgumentException('You must specify exactly three dates.');
        }

        $this->requestedDates = $requestedDates;
        $this->comment = $comment;
    }

    /**
     * @param Carbon $date
     *
     * @return Interview
     */
    public function accept(Carbon $date)
    {
        if ( ! $this->requestedDates->contains($date)) {
            throw new \InvalidArgumentException('This date is not found within the requested dates.');
        }

        $this->state = self::STATE_ACCEPTED;
    }

    public function decline()
    {
        $this->state = self::STATE_DECLINED;
    }

    /**
     * @return \Carbon\Carbon[]|Collection
     */
    public function getRequestedDates()
    {
        return $this->requestedDates;
    }
}