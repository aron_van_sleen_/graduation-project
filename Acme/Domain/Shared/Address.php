<?php

namespace Acme\Domain\Shared;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable */
final class Address
{
    /**
     * @ORM\Column(type="string")
     */
    public $streetName;

    /**
     * @ORM\Column(type="string")
     */
    public $streetNumber;

    /**
     * @ORM\Column(type="string")
     */
    public $postalCode;

    /**
     * @ORM\Column(type="string")
     */
    public $city;

    /**
     * Address constructor.
     * @param $streetName
     * @param $streetNumber
     * @param $city
     */
    public function __construct($streetName, $streetNumber, $postalCode, $city)
    {
        $this->streetName = $streetName;
        $this->streetNumber = $streetNumber;
        $this->postalCode = $postalCode;
        $this->city = $city;
    }

    public function __toString()
    {
        return "$this->streetName $this->streetNumber, $this->postalCode $this->city";
    }
}