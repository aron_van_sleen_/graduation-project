<?php

namespace Acme\Domain\Shared;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable */
final class Email
{
    /**
     * @ORM\Column(type = "string", unique = true)
     */
    private $email;

    /**
     * Email constructor.
     * @param $email
     */
    public function __construct($email)
    {
        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException("$email is not a valid e-mail address.");
        }

        $this->email = $email;
    }

    /**
     * Checks if both the email values are the same
     *
     * @param Email $other
     * @return bool
     */
    public function equals(Email $other)
    {
        if ($this->email === (string) $other) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->email;
    }
}