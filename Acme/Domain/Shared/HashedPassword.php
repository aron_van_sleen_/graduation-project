<?php

namespace Acme\Domain\Shared;

use Doctrine\ORM\Mapping as ORM;
use Acme\App\Services\HashingStrategy;

/**
 * Class HashedPassword
 * @ORM\Embeddable
 */
final class HashedPassword
{
    /**
     * @ORM\Column(name="password", type="string")
     */
    private $hash;

    /**
     * HashedPassword constructor.
     * Can only be created with fromHashingStrategy method.
     * @param $hash
     */
    private function __construct($hash)
    {
        $this->hash = $hash;
    }
    
    /**
     * @param HashedPassword $other
     * @return boolean
     */
    public function equals(HashedPassword $other)
    {
        if ($this->hash === (string) $other) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     *
     * @return static
     */
    public static function fromHashingStrategy(Password $password, HashingStrategy $strategy)
    {
        return new static($strategy->hash($password));
    }
}