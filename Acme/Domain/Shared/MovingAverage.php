<?php
namespace Acme\Domain\Shared;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
final class MovingAverage
{
    /**
     * @ORM\Column(name="rating", type="decimal", precision=3, scale=2, options={"default":0})
     * @var float
     */
    private $average;

    /**
     * @ORM\Column(name="rating_count", type="integer", options={"default":0})
     * @var integer
     */
    private $count;

    public function __construct()
    {
        $this->average = 0;
        $this->count = 0;
    }

    /**
     * @param $amount
     *
     * @return static
     */
    public function addToAverage($amount)
    {
        $newAverage = (($this->average * $this->count) + $amount) / ($this->count + 1);

        $this->count += 1;
        $this->average = $newAverage;
    }

    /**
     * @param $amount
     */
    public function subtractFromAverage($amount)
    {
        $currentAverage = $this->average * $this->count;

        $currentAverage -= $amount;
        $this->count -= 1;

        $this->average = $currentAverage / $this->count;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->average;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}