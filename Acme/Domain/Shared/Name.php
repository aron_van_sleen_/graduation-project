<?php

namespace Acme\Domain\Shared;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable */
final class Name
{
    /** @ORM\Column(type="string") */
    private $firstName;

    /** @ORM\Column(type="string") */
    private $lastName;

    /**
     * Name constructor.
     *
     * @param $firstName
     * @param $lastName
     */
    public function __construct($firstName, $lastName)
    {
        if (trim($firstName) === "" || trim($lastName) === "") {
            throw new \InvalidArgumentException("A first name or last name can't be empty.");
        }

        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "$this->firstName $this->lastName";
    }
}