<?php

namespace Acme\Domain\Shared;


final class Password
{
    const FORMAT = '/^(?=(.*[A-Z]))(?=(.*[0-9]))(?=(.*([^\s\w]|\_))).{8,}$/';

    /** @var  string */
    private $password;

    /**
     * Password constructor.
     *
     * @param $password
     */
    public function __construct($password)
    {
        if (strlen($password) < 8 || ! preg_match(self::FORMAT, $password)) {
            throw new \InvalidArgumentException("A password must be at least 8 characters in length and contain a capital letter, a number and a special character.");
        }

        $this->password = $password;
    }

    /**
     * @param Password $other
     *
     * @return bool
     */
    public function equals(Password $other)
    {
        if ($this->password === (string) $other) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->password;
    }
}