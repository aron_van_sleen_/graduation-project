<?php

namespace Acme\Domain\Shared;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
final class PhoneNumber
{
    const FORMAT = '/^([0-9]{4}|\+[0-9]{1,3}|[0-9])((((\s?[()-]\s?)|\s))?[0-9]){9,10}$/';

    /**
     * @ORM\Column(type="string")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string")
     */
    private $numericPhoneNumber;

    public function __construct($phoneNumber)
    {
        if ( ! preg_match(self::FORMAT, $phoneNumber)) {
            throw new \Exception('The phone number is not of the right format.');
        }

        $this->phoneNumber = $phoneNumber;
        $this->numericPhoneNumber = preg_replace('/([^\d\+])/', '', $phoneNumber);
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getNumeric()
    {
        return $this->numericPhoneNumber;
    }

    /**
     * @return mixed
     */
    function __toString()
    {
        return $this->phoneNumber;
    }


}