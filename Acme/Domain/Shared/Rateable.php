<?php

namespace Acme\Domain\Shared;

trait Rateable
{
    /**
     * @ORM\Embedded(class = "Acme\Domain\Shared\MovingAverage", columnPrefix = false)
     * @var MovingAverage
     */
    protected $rating;

    /**
     * @param Rating $rating
     */
    public function rate(Rating $rating)
    {
        $this->rating->addToAverage($rating->getValue());
    }

    /**
     * @return MovingAverage
     */
    public function getRating()
    {
        return $this->rating;
    }
}