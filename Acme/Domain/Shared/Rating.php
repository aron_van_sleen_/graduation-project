<?php
namespace Acme\Domain\Shared;

final class Rating
{
    const MIN_RATING = 1;
    const MAX_RATING = 5;

    /**
     * @var float
     */
    private $amount;

    public function __construct($amount)
    {
        if ( ! is_numeric($amount)) {
            throw new \InvalidArgumentException('Rating can only contain numeric values');
        }
        if ($amount < self::MIN_RATING || $amount > self::MAX_RATING) {
            throw new \InvalidArgumentException('Rating can only have values between 0 and 5');
        }

        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->amount;
    }
}