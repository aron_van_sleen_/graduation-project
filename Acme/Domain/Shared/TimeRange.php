<?php

namespace Acme\Domain\Shared;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
final class TimeRange
{
    /**
     * @ORM\Column(name="`from`", type="smallint")
     * @var int
     */
    private $from;

    /**
     * @ORM\Column(name="`to`", type="smallint")
     * @var int
     */
    private $to;

    /**
     * TimeRange constructor.
     *
     * @param $from
     * @param $to
     */
    public function __construct($from, $to)
    {
        if ( ! is_numeric($from) && ! is_numeric($to)) {
            throw new \InvalidArgumentException('From and until time needs to be expressed in numeric characters. (Full hours)');
        }
        if ($from == 24 || $to == 0) {
            throw new \InvalidArgumentException('A time range cannot start at 24 or end at 0');
        }
        if ($to < 0 || $from > 24) {
            throw new \InvalidArgumentException('A time range cannot exceed the standard 24 hours');
        }
        if ($from >= $to) {
            throw new \InvalidArgumentException('From time cannot be higher or equal than the until time');
        }

        $this->from = $from;
        $this->to = $to;
    }

    /**
     * Checks if the other time range intersects with the current
     *
     * @param TimeRange $other
     *
     * @return bool
     */
    public function intersects(TimeRange $other)
    {
        // Check if $this->from is between start and end of $other
        // And check if $other->from() is between start and end of $this
        if (($this->from >= $other->from() && $this->from <= $other->to()) ||
            ($other->from() >= $this->from && $other->from() <= $this->to)
        ) {
            return true;
        }

        return false;
    }

    /**
     * Combines two TimeRanges into a single larger TimeRange
     *
     * @param TimeRange $other
     *
     * @return TimeRange
     */
    public function combine(TimeRange $other)
    {
        $from = $this->from();
        $to = $this->to();

        // Use the from time if it's smaller of the other
        if ($other->from() < $this->from) {
            $from = $other->from();
        }
        // Same for the to time, but if it’s larger
        if ($other->to() > $this->to) {
            $to = $other->to();
        }

        return new TimeRange($from, $to);
    }

    /**
     * Returns the duration of the TimeRange
     *
     * @return int|string
     */
    public function duration()
    {
        return $this->to - $this->from;
    }

    /**
     * Checks if the given TimeRanges are equal
     *
     * @param TimeRange $other
     *
     * @return bool
     */
    public function equals(TimeRange $other)
    {
        if ($this->from === $other->from() && $this->to === $other->to()) {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function from()
    {
        return $this->from;
    }

    /**
     * @return int
     */
    public function to()
    {
        return $this->to;
    }
}