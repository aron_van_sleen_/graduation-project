<?php

namespace Acme\Domain\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
final class Characteristic
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $characteristic;

    /**
     * @ORM\Column(type="string")
     */
    private $label;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * Characteristic constructor.
     *
     * @param string $characteristic
     * @param        $userType
     */
    public function __construct(string $characteristic, $label, $userType)
    {
        $this->characteristic = $characteristic;
        $this->label = $label;
        $this->type = $userType;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->characteristic;
    }
}