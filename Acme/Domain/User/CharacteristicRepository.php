<?php

namespace Acme\Domain\User;


use Illuminate\Support\Collection;

interface CharacteristicRepository
{
    /**
     * @param $id
     *
     * @return Characteristic|Characteristic[]
     */
    public function findById($id);

    /**
     * @param $id
     *
     * @return Collection|Characteristic[]
     */
    public function findByType($id);
}