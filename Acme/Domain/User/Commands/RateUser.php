<?php

namespace Acme\Domain\User\Commands;


class RateUser
{
    public $userId;
    public $rating;

    /**
     * RateUser constructor.
     *
     * @param $userId
     * @param $rating
     */
    public function __construct($userId, $rating)
    {
        $this->userId = $userId;
        $this->rating = $rating;
    }
}