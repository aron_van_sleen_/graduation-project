<?php

namespace Acme\Domain\User\Commands;


final class RemoveProfileImage
{
    public $userId;

    /**
     * RemoveProfileImage constructor.
     *
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


}