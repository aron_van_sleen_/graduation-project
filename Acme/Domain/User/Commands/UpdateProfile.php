<?php
namespace Acme\Domain\User\Commands;


use Carbon\Carbon;
use Acme\Domain\Shared\Address;
use Acme\Domain\Shared\Name;
use Acme\Domain\Shared\PhoneNumber;

class UpdateProfile
{
    public $id;

    public $type;

    /** @var Name */
    public $name;
    /** @var Address */
    public $address;
    /** @var Carbon */
    public $dateOfbirth;
    /** @var PhoneNumber */
    public $phoneNumber;

    public $description;

    public $qualities;

    /** @var  array */
    public $characteristics = [];

    public $profileImage;

    public $password;

    public function __construct($id, $type, $data, $fileName = null)
    {
        $this->id = $id;
        $this->type = $type;
        $this->dateOfbirth = Carbon::createFromFormat('Y-m-d', $data['date_of_birth']);
        $this->name = new Name($data['first_name'], $data['last_name']);
        $this->address = new Address($data['street_name'], $data['street_number'], $data['postal_code'], $data['city']);
        $this->phoneNumber = new PhoneNumber($data['phone_number']);
        $this->description = $data['description'];
        $this->qualities = $data['qualities'];
        $this->characteristics = isset($data['characteristics']) ? $data['characteristics'] : [];
        $this->profileImage = $fileName;

        $this->password = empty($data["password"]) ? null : $data["password"];
    }
}