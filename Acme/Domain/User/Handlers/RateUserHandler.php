<?php

namespace Acme\Domain\User\Handlers;


use Acme\Domain\Shared\Rating;
use Acme\Domain\User\Commands\RateUser;
use Acme\Domain\User\UserRepository;

class RateUserHandler
{
    private $repository;

    /**
     * RateUserHandler constructor.
     *
     * @param $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(RateUser $command)
    {
        $user = $this->repository->findById($command->userId);
        $rating =  new Rating($command->rating);

        $user->rate($rating);

        $this->repository->store($user);
    }
}