<?php

namespace Acme\Domain\User\Handlers;


use Acme\Domain\User\Commands\RemoveProfileImage;
use Acme\Domain\User\UserRepository;

class RemoveProfileImageHandler
{
    private $repository;

    /**
     * RemoveProfileImageHandler constructor.
     *
     * @param $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(RemoveProfileImage $command)
    {
        $user = $this->repository->findById($command->userId);

        $user->setProfileImage(null);

        $this->repository->store($user);
    }
}