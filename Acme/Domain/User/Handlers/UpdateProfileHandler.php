<?php

namespace Acme\Domain\User\Handlers;

use Acme\App\Services\HashingStrategy;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Password;
use Acme\Domain\User\CharacteristicRepository;
use Acme\Domain\User\Commands\UpdateProfile;
use Acme\Domain\User\UserRepository;

final class UpdateProfileHandler
{
    /** @var UserRepository */
    private $repository;
    /** @var CharacteristicRepository */
    private $characteristicRepository;
    /** @var HashingStrategy */
    private $hasher;

    public function __construct(UserRepository $repository, CharacteristicRepository $characteristicRepository, HashingStrategy $hasher)
    {
        $this->repository = $repository;
        $this->characteristicRepository = $characteristicRepository;
        $this->hasher = $hasher;
    }

    public function handle(UpdateProfile $command)
    {
        $user = $this->repository->findById($command->id);

        // Be sure the characteristics are in the available set of this user type.
        $characteristics = $this->characteristicRepository->findByType($command->type);
        foreach ($characteristics as $characteristic) {
            if (in_array($characteristic->getId(), $command->characteristics)) {
                $user->addCharacteristic($characteristic);
                continue;
            }
            // Otherwise remove it
            $user->removeCharacteristic($characteristic);
        }

        $user->setName($command->name);
        $user->setDateOfBirth($command->dateOfbirth);
        $user->setAddress($command->address);
        $user->setPhoneNumber($command->phoneNumber);
        $user->setDescription($command->description);
        $user->setQualities($command->qualities);

        if ($command->profileImage !== null) {
            $user->setProfileImage($command->profileImage);
        }

        // Reset password if it's present.
        if ($command->password !== null) {
            $password = new Password($command->password);
            $user->resetPassword(HashedPassword::fromHashingStrategy($password, $this->hasher));
        }

        $this->repository->store($user);;
    }
}