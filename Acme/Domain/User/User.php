<?php

namespace Acme\Domain\User;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Support\Collection;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use Acme\App\Traits\LaravelRememberToken;
use Acme\Domain\Authentication\AcmeAuthenticable;
use Acme\Domain\Shared\MovingAverage;
use Acme\Domain\Shared\Rateable;
use Acme\Domain\Shared\Address;
use Acme\Domain\Shared\Email;
use Acme\Domain\Shared\HashedPassword;
use Gedmo\Mapping\Annotation as Gedmo;
use Acme\Domain\Shared\Name;
use Acme\Domain\Shared\PhoneNumber;


/**
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name = "discr", type = "string")
 */
class User implements CanResetPassword, Authenticatable
{
    use LaravelRememberToken, Timestamps, AcmeAuthenticable, Rateable;

    /**
     * Applies optimistic locking to the user table
     * @ORM\Column(type="integer") @ORM\Version
     */
    private $version;

    // Used to get the specific type of the user
    protected $discr;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type = "integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $activeAccount = false;

    /**
     * @ORM\Embedded(class = "Acme\Domain\Shared\Email", columnPrefix = false)
     * @var Email
     */
    protected $email;

    /**
     * @ORM\Embedded(class = "Acme\Domain\Shared\HashedPassword", columnPrefix = false)
     * @var HashedPassword
     */
    protected $password;

    /**
     * @ORM\Embedded(class = "Acme\Domain\Shared\Name", columnPrefix = false)
     * @var Name
     */
    protected $name;

    /**
     * @ORM\Column(type="date")
     * @var Carbon
     */
    protected $dateOfBirth;

    /**
     * @ORM\Embedded(class = "Acme\Domain\Shared\Address", columnPrefix = false)
     * @var Address
     */
    protected $address;

    /**
     * @ORM\Embedded(class = "Acme\Domain\Shared\PhoneNumber", columnPrefix = false)
     * @var PhoneNumber
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    protected $qualities;

    /**
     * @ORM\ManyToMany(targetEntity="Characteristic", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="users_characteristics",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="characteristic_id", referencedColumnName="id")}
     * )
     * @var ArrayCollection
     */
    protected $characteristics;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $profileImage;

    /**
     * @Gedmo\Slug(fields={"name.firstName", "name.lastName"}, separator=".")
     * @ORM\Column(length=128, unique=true)
     */
    protected $slug;

    /**
     * User constructor.
     */
    protected function __construct()
    {
        $this->characteristics = new ArrayCollection();
        $this->rating = new MovingAverage();
    }

    public function resetPassword(HashedPassword $newPassword)
    {
        $this->password = $newPassword;
    }

    /**
     * @param Name $name
     */
    public function setName(Name $name)
    {
        $this->name = $name;
    }

    /**
     * @param Carbon $dateOfBirth
     */
    public function setDateOfBirth(Carbon $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @param PhoneNumber $phoneNumber
     */
    public function setPhoneNumber(PhoneNumber $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @param string $qualities
     */
    public function setQualities(string $qualities)
    {
        $this->qualities = $qualities;
    }

    /**
     * @param string $profileImage
     */
    public function setProfileImage($profileImage)
    {
        $this->profileImage = $profileImage;
    }

    /**
     * @param Characteristic $characteristic
     */
    public function addCharacteristic(Characteristic $characteristic)
    {
        if ($this->characteristics->contains($characteristic)) {
            return;
        }

        $this->characteristics->add($characteristic);
    }

    /**
     * @param Characteristic $characteristic
     */
    public function removeCharacteristic(Characteristic $characteristic)
    {
        if ( ! $this->characteristics->contains($characteristic)) {
            return;
        }

        $this->characteristics->removeElement($characteristic);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return Carbon
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return PhoneNumber
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getQualities()
    {
        return $this->qualities;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * @return Collection
     */
    public function getCharacteristics()
    {
        return collect($this->characteristics->toArray());
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->discr;
    }

    public function activateAccount()
    {
        $this->activeAccount = true;
    }

    /**
     * @param Email          $email
     * @param HashedPassword $password
     *
     * @return static
     */
    public static function register(Email $email, HashedPassword $password)
    {
        $user = new static;
        $user->email = $email;
        $user->password = $password;

        return $user;
    }
}