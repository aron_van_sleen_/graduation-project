<?php

namespace Acme\Domain\User;

use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Shared\Email;
use Webpatser\Uuid\Uuid;

interface UserRepository
{
    /**
     * @param $id
     *
     * @return User
     */
    public function findById($id);

    /**
     * @param $slug
     *
     * @return User
     */
    public function findBySlug($slug);

    /**
     * @param Email $email
     *
     * @return User
     */
    public function findByEmail(Email $email);

    /**
     * @param Careprovider|Careclient|User $user
     *
     * @return void
     */
    public function store($user);
}