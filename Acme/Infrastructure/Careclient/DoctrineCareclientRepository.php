<?php

namespace Acme\Infrastructure\Careclient;

use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careclient\CareclientRepository;
use Acme\Domain\Shared\Email;
use Webpatser\Uuid\Uuid;

class DoctrineCareclientRepository extends EntityRepository implements CareclientRepository
{
    public function store($careclient)
    {
        EntityManager::persist($careclient);
        EntityManager::flush();
    }

    public function findById($id)
    {
        return $this->find($id);
    }

    public function findByEmail(Email $email)
    {
       return $this->findOneBy(['email.email' => (string) $email]);
    }

    public function findBySlug($slug)
    {
        return $this->findOneBy(['slug' => $slug]);
    }

    /**
     * @param Uuid $id
     *
     * @return Careclient
     */
    public function findByUuid(Uuid $id)
    {
        return $this->findOneBy(['uuid' => (string) $id]);
    }
}