<?php

namespace Acme\Infrastructure\Careclient;


use Doctrine\ORM\EntityRepository;
use Acme\Domain\Careclient\PlanRepository;

class DoctrinePlanRepository extends EntityRepository implements PlanRepository
{
    public function findByPlanName(string $plan)
    {
        return $this->findOneBy(['plan' => $plan]);
    }
}