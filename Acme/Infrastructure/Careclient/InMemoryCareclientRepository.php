<?php
namespace Acme\Infrastructure\Careclient;

use Illuminate\Support\Collection;
use Acme\Domain\Careclient\CareclientRepository;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Shared\Email;
use Webpatser\Uuid\Uuid;

class InMemoryCareclientRepository implements CareclientRepository
{
    private $items;

    public function __construct()
    {
        $this->items = new Collection();
    }

    public function findById($id)
    {
        return $this->items->first(function($i, Careclient $careclient) use ($id) {
            return $careclient->getId() == $id;
        });
    }

    public function findByEmail(Email $email)
    {
        return $this->items->first(function($i, Careclient $careclient) use ($email) {
            return $careclient->getEmail()->equals($email);
        }, null);
    }

    public function store($careprovider)
    {
        $this->items->push($careprovider);
    }

    public function findBySlug($slug)
    {
        // TODO: Implement findBySlug() method.
    }

    /**
     * @param Uuid $id
     *
     * @return mixed
     */
    public function findByUuid(Uuid $id)
    {
        // TODO: Implement findByUuid() method.
    }
}