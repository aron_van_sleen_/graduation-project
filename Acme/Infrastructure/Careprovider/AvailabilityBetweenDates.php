<?php

namespace Acme\Infrastructure\Careprovider;

use Carbon\Carbon;
use Doctrine\ORM\Query;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Acme\Domain\Careprovider\Availability;
use Acme\Domain\Careprovider\Careprovider;

final class AvailabilityBetweenDates
{
    private $careproviderId;
    private $from;
    private $to;

    public function __construct($careproviderId, Carbon $from, Carbon $to)
    {
        $this->careproviderId = $careproviderId;
        $this->from = $from->format('Y-m-d');
        $this->to = $to->format('Y-m-d');
    }

    public function get()
    {
        $qb = EntityManager::createQueryBuilder()
            ->select('a')
            ->from(Availability::class, 'a')
            ->join(Careprovider::class, 'c', 'WITH', 'c.id = :careproviderId')
            ->andWhere('a.date >= :from')
            ->andWhere('a.date <= :to');

        $query = $qb->getQuery()->setParameters([
            'careproviderId' => $this->careproviderId,
            'from' => $this->from,
            'to' => $this->to
        ]);

        return $query->getResult(Query::HYDRATE_OBJECT);
    }
}