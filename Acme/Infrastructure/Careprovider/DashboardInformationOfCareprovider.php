<?php

namespace Acme\Infrastructure\Careprovider;

use Doctrine\ORM\Query;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Hiring\CareproviderRequest;
use Acme\Domain\Hiring\Commands\RequestInterview;
use Acme\Domain\Hiring\InterviewRequest;

final class DashboardInformationOfCareprovider
{
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    public function get()
    {
        $data = EntityManager::createQueryBuilder()
            ->select('c.name.firstName, c.name.lastName')
            ->from(Careprovider::class, 'c')
            ->where('c.id = :userId')
            ->getQuery()->setParameter('userId', $this->userId)->getSingleResult(Query::HYDRATE_ARRAY);

        $requests = EntityManager::createQueryBuilder()
            ->select('r, c')
            ->from(CareproviderRequest::class, 'r')
            ->leftJoin('r.careclient', 'c')
            ->where('r.careprovider = :careprovider')
            ->andWhere('r.state = :state')
            ->getQuery()->setParameters([
                'careprovider' => $this->userId,
                'state'        => CareproviderRequest::STATE_PENDING
            ])->getResult(Query::HYDRATE_OBJECT);

        $data["requests"] = collect($requests);

        return $data;
    }
}