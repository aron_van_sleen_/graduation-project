<?php

namespace Acme\Infrastructure\Careprovider;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Acme\Domain\Careprovider\Availability;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Hiring\InterviewRequest;
use Acme\Domain\Shared\Email;
use Acme\Domain\User\Characteristic;
use Webpatser\Uuid\Uuid;

class DoctrineCareproviderRepository extends EntityRepository implements CareproviderRepository
{
    /**
     * @param $id
     *
     * @return Careprovider
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $careproviderId
     * @param $id
     *
     * @return null|InterviewRequest
     */
    public function findInterviewRequestById($careproviderId, $id)
    {
        $qb = EntityManager::createQueryBuilder()
            ->select('r', 'c')->from(InterviewRequest::class, 'r')
            ->join('r.careclient', 'c')
            ->where('r.id = :id')->andWhere('r.careprovider = :careprovider');

        $query = $qb->getQuery()->setParameters([
            ':id'           => $id,
            ':careprovider' => $careproviderId
        ]);

        return $query->getSingleResult(Query::HYDRATE_OBJECT);
    }

    /**
     * @param $dates
     * @param $careproviderId
     *
     * @return mixed
     */
    public function findCareproviderAvailabilityByDates($careproviderId, array $dates)
    {
        $qb = EntityManager::createQueryBuilder()
            ->select('a')->from(Availability::class, 'a')
            ->where('a.careprovider = (:careproviderId)')
            ->andWhere('a.date IN (:dates)');

        $query = $qb->getQuery()->setParameters([
            'careproviderId' => $careproviderId,
            'dates'          => $dates
        ]);

        return $query->getResult(Query::HYDRATE_OBJECT);
    }

    /**
     * @param Email $email
     *
     * @return Careprovider
     */
    public function findByEmail(Email $email)
    {
        return $this->findOneBy(['email.email' => (string) $email]);
    }

    /**
     * @param $slug
     *
     * @return null|object
     */
    public function findBySlug($slug)
    {
        return $this->findOneBy(['slug' => $slug]);
    }

    public function store($careprovider)
    {
        EntityManager::persist($careprovider);
        EntityManager::flush();
    }

    /**
     * @param Uuid $id
     *
     * @return Careprovider
     */
    public function findByUuid(Uuid $id)
    {
        return $this->findOneBy(['uuid' => (string) $id]);
    }
}