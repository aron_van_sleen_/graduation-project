<?php

namespace Acme\Infrastructure\Careprovider;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Money\Money;
use Acme\Domain\Careprovider\Level;
use Acme\Domain\Careprovider\LevelRepository;

class DoctrineLevelRepository extends EntityRepository implements LevelRepository
{
    /**
     * @param $id
     *
     * @return Level
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @return Level
     */
    public function findDefaultLevel()
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT l FROM Saferia\Domain\Careprovider\Level l 
                           WHERE l.maximumHourlyRate = (
                              SELECT MIN(l2.maximumHourlyRate) FROM Saferia\Domain\Careprovider\Level l2)');

        return $query->getSingleResult(Query::HYDRATE_OBJECT);
    }

    /**
     * @return Money
     */
    public function findHighestMaximumHourlyRate()
    {
        $query = $this->getEntityManager()->createQuery('SELECT MAX(l.maximumHourlyRate) FROM Saferia\Domain\Careprovider\Level l');

        return Money::EUR((int) $query->getResult(Query::HYDRATE_SINGLE_SCALAR));
    }
}