<?php

namespace Acme\Infrastructure\Careprovider;

use Carbon\Carbon;
use Elasticsearch\Client;
use Money\Money;
use Acme\Domain\Careprovider\CareproviderSearchRepository;
use Acme\Domain\Shared\GeoLocation;
use Acme\App\ViewModels\CareproviderCardViewModel;
use Acme\Domain\Shared\TimeRange;

class ElasticSearchCareproviderSearchRepository implements CareproviderSearchRepository
{
    /** @var Client */
    protected $elasticSearch;

    /** @var array */
    protected $query;

    /** @var  integer */
    protected $currentPage;

    public function __construct()
    {
        $this->elasticSearch = app()->make('ElasticSearch');
        // Set the initial query
        $this->query = [
            'index' => 'acme',
            'type'  => 'careprovider',
            'body'  => [
                'query' => []
            ]
        ];
    }

    /**``
     * @return mixed;
     */
    public function all()
    {
        $result = $this->elasticSearch->search($this->query);
        $response['total'] = $result['hits']['total'];
        $response['data'] = $this->mapToViewModels($result);

        return $response;
    }

    /**
     * Adds pagination to the results query and executes the search.
     *
     * @param $page
     * @param $perPage
     *
     * @return mixed
     */
    public function paginate($page, $perPage)
    {
        $this->currentPage = $page;

        // At pagination parameters to the root of the body
        $this->query['body'] = array_merge(
            $this->query['body'],
            ['size' => $perPage, 'from' => ($perPage * ($page - 1))]
        );

        return $this->all();
    }

    /**
     * Selects the careproviders who are available on this date that can fit the time range.
     *
     * @param           $date
     * @param TimeRange $searchRange
     *
     * @return $this
     */
    public function whereAvailable(Carbon $date, TimeRange $searchRange)
    {
        $this->query['body']['query']['bool']['filter']['bool']['must'][]['has_child'] = [
            'type'  => 'availability',
            'query' => ['bool' => ['must' => [
                ['term' => ['date' => $date->format('Y-m-d')]],
                ['nested' => [
                    'path'   => 'time_ranges',
                    'filter' => ['bool' => ['must' => [
                        // Need to be separate arrays, combining them doesn't work :(
                        ['range' => [
                            'time_ranges.from' => ['lte' => $searchRange->from()],
                        ]],
                        ['range' => [
                            'time_ranges.to' => ['gte' => $searchRange->to()]
                        ]]
                    ]]]
                ]]
            ]]]

        ];

        return $this;
    }

    /**
     * Finds careproviders within a radius of a location in kilometers.
     *
     * @param GeoLocation $location
     * @param             $radius
     *
     * @return $this
     */
    public function whereWithinRadiusOfLocation(GeoLocation $location, $radius)
    {
        // Add the the 'must' array, equivalent of AND
        $this->query['body']['query']['bool']['filter']['bool']['must'][]['geo_distance'] = array(
            'distance' => "{$radius}km",
            'location' => array(
                'lat' => $location->getLatitude(),
                'lon' => $location->getLongitude()
            )
        );

        return $this;
    }

    /**
     * Filters the query so only careproviders below a specific price are found
     *
     * @param Money $maxHourlyRate
     *
     * @return $this
     */
    public function whereBelowPrice(Money $maxHourlyRate)
    {
        $this->query['body']['query']['bool']['filter']['bool']['must'][]['range']['hourlyRate'] = [
            'lte' => $maxHourlyRate->getAmount()
        ];

        return $this;
    }

    /**
     * Filters the query to find where careproviders have at least the given characteristics
     *
     * @param array $characteristics
     *
     * @return $this
     */
    public function whereNotHavingCharacteristics(array $characteristics)
    {
        foreach ($characteristics as $characteristic) {
            $this->query['body']['query']['bool']['filter']['bool']['must_not'][]['term'] = [
                'characteristics' => $characteristic
            ];
        }

        return $this;
    }

    /**
     * Maps the elasticsearch results to the appropriate view model
     *
     * @param $results
     *
     * @return CareproviderCardViewModel[]
     */
    protected function mapToViewModels($results)
    {
        $mapped = [];
        foreach ($results["hits"]["hits"] as $result) {
            $result = $result["_source"];

            $viewModel = new CareproviderCardViewModel();
            $viewModel->name = $result["name"];
            $viewModel->hourlyRate = $result["hourlyRate"];
            $viewModel->city = $result["city"];
            $viewModel->slug = $result["slug"];

            if (key_exists('profileImage', $result)) {
                $viewModel->profileImage = $result['profileImage'];
            }

            $mapped[] = $viewModel;
        }

        return $mapped;
    }
}