<?php

namespace Acme\Infrastructure\Careprovider;

use Illuminate\Support\Collection;
use Acme\Domain\Careprovider\Availability;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Hiring\InterviewRequest;
use Acme\Domain\Shared\Email;
use Acme\Infrastructure\ViewModels\CareproviderCardViewModel;
use Webpatser\Uuid\Uuid;

class InMemoryCareproviderRepository implements CareproviderRepository
{
    private $items;

    public function __construct()
    {
        $this->items = new Collection();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->items->first(function($i, Careprovider $careprovider) use ($id) {
           return $careprovider->getId() == $id;
        });
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findByEmail(Email $email)
    {
        
        return $this->items->first(function($i, Careprovider $careprovider) use ($email) {
            return $careprovider->getEmail()->equals($email);
        }, null);
    }


    public function store($careprovider)
    {
        $this->items->push($careprovider);
    }

    public function findBySlug($slug)
    {
        // TODO: Implement findBySlug() method.
    }

    /**
     * @param $careproviderId
     * @param $interviewRequest
     *
     * @return InterviewRequest
     */
    public function findInterviewRequestById($careproviderId, $interviewRequest)
    {
        // TODO: Implement findInterviewRequestById() method.
    }

    /**
     * @param $dates
     * @param $careproviderId
     *
     * @return mixed
     */
    public function findCareproviderAvailabilityByDates($careproviderId, array $dates)
    {
        // TODO: Implement findCareproviderAvailabilityByDates() method.
    }

    /**
     * @param Uuid $id
     *
     * @return mixed
     */
    public function findByUuid(Uuid $id)
    {
        // TODO: Implement findByUuid() method.
    }
}