<?php

namespace Acme\Infrastructure\Hiring;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Acme\Domain\Hiring\CareproviderRequest;
use Acme\Domain\Hiring\CareproviderRequestRepository;
use Acme\Domain\Hiring\InterviewRequest;

class DoctrineCareproviderRequestRepository extends EntityRepository implements CareproviderRequestRepository
{
    public function findById($id)
    {
        return $this->find($id);
    }

    public function findPendingInterviewRequest($careclientId, $careproviderId)
    {
        $qb = EntityManager::createQueryBuilder()
            ->select('r')->from(InterviewRequest::class, 'r')
            ->where('r.careclient = :careclientId')
            ->andWhere('r.careprovider = :careproviderId')
            ->andWhere('r.state = :state');

        $query = $qb->getQuery()->setParameters([
            'careclientId'    => $careclientId,
            'careproviderId' => $careproviderId,
            'state'          => CareproviderRequest::STATE_PENDING
        ]);

        return $query->getResult(Query::HYDRATE_OBJECT);
    }

    public function store(CareproviderRequest $request)
    {
        EntityManager::persist($request);
        EntityManager::flush();
    }

    public function remove(CareproviderRequest $request)
    {
        EntityManager::remove($request);
        EntityManager::flush();
    }
}