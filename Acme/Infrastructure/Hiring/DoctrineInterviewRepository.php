<?php
namespace Acme\Infrastructure\Hiring;

use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Acme\Domain\Hiring\Interview;
use Acme\Domain\Hiring\InterviewRepository;

class DoctrineInterviewRepository extends EntityRepository implements InterviewRepository
{
    public function findById($id)
    {
        return $this->find($id);
    }

    public function store(Interview $interview)
    {
        EntityManager::persist($interview);
        EntityManager::flush();
    }
}