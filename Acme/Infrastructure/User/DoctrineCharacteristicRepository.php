<?php

namespace Acme\Infrastructure\User;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Acme\Domain\User\Characteristic;
use Acme\Domain\User\CharacteristicRepository;

class DoctrineCharacteristicRepository extends EntityRepository implements CharacteristicRepository
{

    /**
     * @param $id
     *
     * @return Characteristic|Characteristic[]
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $type
     *
     * @return \Acme\Domain\User\Characteristic[]
     */
    public function findByType($type)
    {
        $query = $this->getEntityManager()->createQuery('SELECT c FROM Acme\Domain\User\Characteristic c WHERE c.type = :type')
            ->setParameters(array('type' => $type));

        return $query->getResult(Query::HYDRATE_OBJECT);
    }
}