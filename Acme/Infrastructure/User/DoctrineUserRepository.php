<?php

namespace Acme\Infrastructure\User;


use Doctrine\ORM\EntityRepository;
use Illuminate\Support\Collection;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Acme\Domain\User\Characteristic;
use Acme\Domain\User\User;
use Acme\Domain\User\UserRepository;
use Acme\Domain\Shared\Email;
use Webpatser\Uuid\Uuid;

class DoctrineUserRepository extends EntityRepository implements UserRepository
{
    /**
     * @param $id
     *
     * @return User
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $email
     *
     * @return User
     */
    public function findByEmail(Email $email)
    {
        return $this->findOneBy(['email.email' => (string) $email]);
    }

    /**
     * @param \Acme\Domain\Careclient\Careclient|\Acme\Domain\Careprovider\Careprovider|User $user
     */
    public function store($user)
    {
        EntityManager::persist($user);
        EntityManager::flush();
    }

    /**
     * @param $slug
     *
     * @return User
     */
    public function findBySlug($slug)
    {
        return $this->findOneBy(['slug' => $slug]);
    }

    /**
     * @param Uuid $id
     *
     * @return mixed
     */
    public function findByUuid(Uuid $id)
    {
        return $this->findOneBy(['uuid' => (string) $id]);
    }
}