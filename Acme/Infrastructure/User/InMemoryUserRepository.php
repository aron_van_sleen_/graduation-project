<?php
namespace Acme\Infrastructure\User;

use Illuminate\Support\Collection;
use Acme\Domain\Shared\Email;
use Acme\Domain\User\User;
use Acme\Domain\User\UserRepository;
use Webpatser\Uuid\Uuid;

class InMemoryUserRepository implements UserRepository
{
    private $items;

    public function __construct()
    {
        $this->items = new Collection();
    }

    public function findById($id)
    {

    }

    /**
     * Finds the registered user that has the specified e-mail
     *
     * @param Email $email
     *
     * @return User
     */
    public function findByEmail(Email $email)
    {
        // TODO: Implement findByEmail() method.
    }

    /**
     * @param $slug
     *
     * @return User
     */
    public function findBySlug($slug)
    {
        // TODO: Implement findBySlug() method.
    }

    public function store($user)
    {
        // TODO: Implement store() method.
    }

    /**
     * @param Uuid $id
     *
     * @return mixed
     */
    public function findByUuid(Uuid $id)
    {
        // TODO: Implement findByUuid() method.
    }
}