# Graduation project #
During this project, I mostly focused on trying out new concepts and experimenting with application architecture, rather than implementing as many features as I can. During this graduation term, I became very excited about the subject of Domain-Driven Design, which I discovered through Eric Evan's book: Domain-Driven Design, Tackling Complexity in the Heart of Software. Because of this subject, I wanted to use my graduation project to experiment with this subject. On a more technical level, I got excited by Hexagonal architecture and used such an architecture to separate the domain layer from the rest of the application.

## Application structure ##
The majority of the folder structure is based on a default [Laravel project](https://laravel.com/docs/5.1/structure). The biggest deviation is the Acme folder, this is the folder containing all the logic that is relevant to the domain knowledge of the application. Acme is just a placeholder name, it could either stay like that or be replaced with the name of the business you're making the application for. The Acme folder is split into three subfolders: App, Domain and Infrastructure.

### App folder ###
This folder contains classes meant to support the application, but aren't really part of the business logic. It contains things like custom Doctrine Types, ViewModels to send back to the templates and service classes for tasks like hashing.

### Domain folder ###
This is essentially the heart of the application, this folder contains all of the business logic. To separate the business logic from the rest of the application, I made use of a command bus. For the bus itself, I made us of the [Tactician Package](http://tactician.thephpleague.com/). The business logic is contained in specific Handler classes, tier to an use case. When you want to start an use case, you need to place the appropriate command class in the bus, which in turn will make sure the Handler class will receive and handle it. For example the next code fragment shows how to initiate an use case:

```
#!php

$this->commandBus->handle(
    new RequestInterview(
        Auth::user()->getId(),
        $careproviderId,
        $request->get('dates')
    )
);
```

The RequestInterview class is a command class, which is essentially a DTO (Data transfer object). In this case, the command bus will make sure it has an instance of the RequestInterviewHandler class and pass it the command class so it has the data to handle the use case correctly. Value objects can be found in the Shared subfolder.

### Infrastructure folder ###
To persist data in this application, I've decided to use Doctrine as an ORM system. This ties in with my goal to delve further into Domain-Driven Design. The largest factor in that decision is the fact that it made use of the Data Mapper pattern, rather than the active record. Because of this, you can write Entities to resemble the business as close as possible, while worrying about how to persist the data later. Doctrine logic is abstracted away behind repositories, which can be found in the Infrastructure folder. Next to repositories, there are also query objects in this folders. These objects are meant to fetch data that are out of the scope of the entity-specific repositories.