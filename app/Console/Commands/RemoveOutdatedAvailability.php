<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Console\Command;

class RemoveOutdatedAvailability extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove-outdated-availability';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = EntityManager::createQuery('DELETE Acme\Domain\Careprovider\Availability a WHERE a.date < :date');
        $query->setParameter('date', Carbon::now()->setTime(0, 0));
        $this->comment($query->getResult() . " items removed.");
    }
}
