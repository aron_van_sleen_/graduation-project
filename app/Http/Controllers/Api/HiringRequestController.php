<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use League\Tactician\CommandBus;
use Acme\Domain\Hiring\Commands\AcceptHiringRequest;
use Acme\Domain\Hiring\Commands\DeclineHiringRequest;
use Acme\Domain\Hiring\Commands\DeclineInterviewRequest;

final class HiringRequestController extends Controller
{
    private $commandBus;

    /**
     * HiringRequestController constructor.
     *
     * @param $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }


    public function accept(Request $request, $hiringRequestId)
    {
        $this->commandBus->handle(new AcceptHiringRequest(
            Auth::user()->getId(),
            $hiringRequestId
        ));

        return response()->json(['message' => 'Je hebt het inhuur verzoek geaccepteerd!']);
    }

    public function decline(Request $request, $hiringRequestId)
    {
        $this->commandBus->handle(new DeclineHiringRequest(
            Auth::user()->getId(),
            $hiringRequestId
        ));

        return response()->json(['message' => 'Je hebt het inhuur verzoek geweigerd.']);
    }
}
