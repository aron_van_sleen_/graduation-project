<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Tactician\CommandBus;
use Acme\Domain\Hiring\Commands\AcceptInterviewRequest;
use Acme\Domain\Hiring\Commands\DeclineInterviewRequest;

final class InterviewRequestController extends Controller
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @param Request $request
     * @param         $interviewRequestId
     *
     * @return Response
     */
    public function accept(Request $request, $interviewRequestId)
    {
        $this->commandBus->handle(new AcceptInterviewRequest(
            Auth::user()->getId(),
            $interviewRequestId,
            $request->get('date')
        ));

        return response()->json(['message' => 'Je hebt het verzoek tot een interview geaccepteerd.']);
    }

    /**
     * @param Request $request
     * @param         $interviewRequestId
     *
     * @return Response
     */
    public function decline(Request $request, $interviewRequestId)
    {
        $this->commandBus->handle(new DeclineInterviewRequest(
            Auth::user()->getId(),
            $interviewRequestId
        ));

        return response()->json(['message' => 'Je hebt het verzoek tot een interview afgewezen.']);
    }
}
