<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\Auth\ResetPassword;
use League\Tactician\CommandBus;
use Acme\Domain\Authentication\Commands\ResetPassword as ResetPasswordCommand;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    use ResetsPasswords;

    private $commandBus;

    protected $redirectTo = '/dashboard';

    /**
     * Create a new password controller instance.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->middleware('guest');
        $this->commandBus = $commandBus;
    }

    public function getEmail()
    {
        return view('auth.passwords.email')->with([
            'metaTitle' => 'Wachtwoord vergeten'
        ]);
    }

    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }

        $email = $request->input('email');

        return view('auth.passwords.reset')->with(array_merge(compact('token', 'email'), [
            'metaTitle' => 'Wachtwoord resetten'
        ]));
    }
    
    /**
     * Get the response for after the reset link has been successfully sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        flashy()->success(trans($response));

        return redirect()->back();
    }

    protected function getSendResetLinkEmailFailureResponse($response)
    {
        flashy()->error(trans($response));

        return redirect()->back()->withErrors(['email' => trans($response)]);
    }

    /**
     * @param ResetPassword $request
     *
     * @return \Symfo ny\Component\HttpFoundation\Response
     */
    public function reset(ResetPassword $request)
    {
        $credentials = $this->getResetCredentials($request);
        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password)  {
            // Put the reset password command in the command bus and authenticate the user if the command is executed correctly
            $this->commandBus->handle(new ResetPasswordCommand($user->getId(), $password));
            // Using ID since my user class isn't using the authenticable interface
            Auth::login($user);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                flashy()->success(trans($response));
                return $this->getResetSuccessResponse($response);
            default:
                flashy()->error(trans($response));
                return $this->getResetFailureResponse($request, $response);
        }
    }
}
