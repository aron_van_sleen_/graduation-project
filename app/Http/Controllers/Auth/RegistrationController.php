<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterAsCareclient;
use App\Http\Requests\Auth\RegisterAsCareprovider;
use App\Http\Requests\Auth\RegisterAsMember;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use League\Tactician\CommandBus;
use Money\Money;
use Acme\App\Services\PayPalPaymentGateway;
use Acme\Domain\Careclient\Commands\RegisterAsCareclient as RegisterAsCareclientCommand;
use Acme\Domain\Careprovider\Commands\RegisterAsCareprovider as RegisterAsCareproviderCommand;
use Webpatser\Uuid\Uuid;

class RegistrationController extends Controller
{
    protected $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function showRoleSelectionPAge()
    {
        return view('auth.select-role')->with([
            'metaTitle' => 'Kies een rol'
        ]);
    }


    /**
     * Displays the registration form
     *
     * @return Response
     */
    public function showRegisterAsCareclientForm()
    {
        return view('auth.register-as-careclient')->with([
            'metaTitle' => 'Aanmelden als ouder of verzorger'
        ]);
    }

    /**
     * Displays the registration form
     *
     * @return Response
     */
    public function showRegisterAsCareproviderForm()
    {
        return view('auth.register-as-careprovider')->with([
            'metaTitle' => 'Aanmelden als oppasser'
        ]);
    }

    /**
     * @param RegisterAsMember $request
     *
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function registerAsCareprovider(RegisterAsCareprovider $request)
    {
        $this->commandBus->handle(new RegisterAsCareproviderCommand($request->all()));

        // Registration has been succesful, log the new user in automatically
        Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]);

        flashy()->success('U heeft succesvol een account als oppasser geregistreerd!');

        return redirect(route('dashboard'));
    }

    /**
     * @param RegisterAsCareclient $request
     *
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function registerAsCareclient(RegisterAsCareclient $request)
    {
        $this->commandBus->handle(new RegisterAsCareclientCommand($request->all()));

        // Log the user in
        Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]);

        flashy()->success('U heeft succesvol een account als ouder of verzorger geregistreerd!');

        return redirect(route('subscription.plans'));
    }
}