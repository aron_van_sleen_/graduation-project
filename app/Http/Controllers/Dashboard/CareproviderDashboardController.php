<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\StateAvailabilityRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Period\Period;
use Response;
use Acme\App\Presenters\CalendarMonth;
use Acme\Domain\Careprovider\Availability;
use Acme\Domain\Careprovider\Commands\StateAvailability;
use Acme\Domain\Careprovider\Commands\UnstateAvailability;
use Acme\Domain\Shared\TimeRange;
use Acme\Infrastructure\Careprovider\AvailabilityBetweenDates;

class CareproviderDashboardController extends DashboardController
{
    /**
     * @param null $year
     * @param null $month
     *
     * @return $this
     */
    public function showAvailabilityForm($year = null, $month = null)
    {
        // Default to this year and month if the time is in the past or not correct.
        if ($year !== null && $month !== null &&
            ($year < date("Y") || $month < 1 || $month > 12)
        ) {
            $year = date("Y");
            $month = date("m");
        }

        $calendar = new CalendarMonth($year, $month);
        $dates = $calendar->getDates()->keyBy(function (\DateTimeInterface $date) {
            return $date->format("Y-m-d");
        })->chunk(7)->keyBy(function (Collection $week) {
            return $week->first()->format('W');
        });

        return view('pages.dashboard.availability')->with([
            'metaTitle'   => 'Beschikbaarheid aangeven',
            'days'        => $dates,
            'currentDate' => $calendar->getCalendarDate()
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailabilityBetweenDates(Request $request)
    {
        $from = Carbon::createFromFormat("Y-m-d", $request->get('from'));
        $to = Carbon::createFromFormat("Y-m-d", $request->get('to'));

        // Get the availability of the authenticated user and format it for the view
        $availability = collect((new AvailabilityBetweenDates(Auth::user()->getId(), $from, $to))->get());

        $availability = $availability->map(function ($item) use ($availability) {
            $date = $item->getDate();
            // Group all availability on the same day
            return $availability->filter(function ($other) use ($date) {
                return $other->getDate()->eq($date);
            });
        })->keyBy(function ($item) {
            // Key it by date format (this removes any duplicate groups)
            return $item->first()->getDate()->format('Y-m-d');
        })->map(function ($item) {
            // Map each date to an array of timeRanges and call values to reset the indices
            return $item->map(function ($availability) {
                $timeRange = $availability->getTimeRange();
                return ["from" => $timeRange->from(), "to" => $timeRange->to()];
            })->values()->toArray();
        });

        return response()->json($availability);
    }

    /**
     * @param StateAvailabilityRequest $request
     *
     * @return string
     */
    public function stateAvailability(StateAvailabilityRequest $request)
    {
        if ($request->has('timeRanges')) {
            $command = new StateAvailability(
                Auth::user()->getId(),
                $request->get('dates'),
                $request->get('timeRanges')
            );
        } else {
            $command = new UnstateAvailability(
                Auth::user()->getId(),
                $request->get('dates')
            );
        }

        $this->commandBus->handle($command);

        return response()->json([]);
    }
}