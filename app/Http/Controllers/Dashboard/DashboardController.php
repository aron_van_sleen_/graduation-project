<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\UpdateCareclientProfile;
use App\Http\Requests\Dashboard\UpdateCareproviderProfile;
use App\Http\Requests\Dashboard\UpdateProfile as UpdateProfileRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use League\Tactician\CommandBus;
use Acme\App\ViewModels\Dashboard\CareproviderDashboardViewModel;
use Acme\App\ViewModels\Dashboard\UpdateCareclientProfileViewModel;
use Acme\App\ViewModels\Dashboard\UpdateCareproviderProfileViewModel;
use Acme\Domain\Careclient\CareclientRepository;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Careprovider\Commands\UpdateCareproviderProperties;
use Acme\Domain\User\Characteristic;
use Acme\Domain\User\CharacteristicRepository;
use Acme\Domain\User\Commands\RemoveProfileImage;
use Acme\Domain\User\Commands\UpdateProfile;
use Acme\Infrastructure\Careprovider\DashboardInformationOfCareprovider;

class DashboardController extends Controller
{
    use DispatchesJobs;

    /**
     * @var CommandBus
     */
    protected $commandBus;

    /**
     * @var CareclientRepository|CareproviderRepository
     */
    protected $repository;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;

        // Depending on the logged in user, get the right repository
        if (Auth::user() instanceof Careprovider) {
            $this->repository = app()->make(CareproviderRepository::class);
        } else {
            $this->repository = app()->make(CareclientRepository::class);
        }
    }

    /**
     * @return $this
     */
    public function showDashboardPage()
    {
        $user = Auth::user();
        $viewModel = null;

        if ($user instanceof Careprovider) {
            $viewModel = new CareproviderDashboardViewModel(
                new DashboardInformationOfCareprovider($user->getId())
            );
        } else {
            // TODO: Implement careclient dashboard information
        }

        return view("pages.dashboard.{$user->getType()}-dashboard")->with([
            'metaTitle' => 'Dashboard',
            'model' => $viewModel
        ]);
    }

    /**
     * @param CharacteristicRepository $characteristicRepository
     *
     * @return $this
     * @throws \Exception
     */
    public function showEditProfileForm(CharacteristicRepository $characteristicRepository)
    {
        $user = $this->repository->findById(Auth::user()->getId());
        $characteristics = collect($characteristicRepository->findByType(Auth::user()->getType()));
        $viewModel = null;

        if ($user instanceof Careprovider) {
            $template = 'edit-careprovider-profile';
            $viewModel = new UpdateCareproviderProfileViewModel($user);
        } else {
            $template = 'edit-careclient-profile';
            $viewModel = new UpdateCareclientProfileViewModel($user);
        }

        $characteristics = $characteristics->keyBy(function (Characteristic $characteristic) {
            return $characteristic->getId();
        })->map(function ($characteristic) {
            return (string) $characteristic;
        });

        return view("pages.dashboard.{$template}")->with([
            'metaTitle'       => 'Profiel wijzigen',
            'profile'         => $viewModel,
            'characteristics' => $characteristics,
            'type'            => Auth::user()->getType()
        ]);
    }

    /**
     * @param                      $userId
     * @param UpdateProfileRequest $request
     */
    private function updateProfile($userId, UpdateProfileRequest $request)
    {
        $fileName = null;

        if ($request->hasFile('profile_image')) {
            $fileName = 'avatars/' . uniqid('profile_') . '.jpg';

            // TODO: Temporary solution, server config thing?
            ini_set('memory_limit', '256M');

            $file = $request->file('profile_image');
            $oldProfileFileName = Auth::user()->getProfileImage();

            $img = Image::make($file);
            $img->fit(160, 160);

            Storage::disk('public')->put($fileName, (string) $img->encode('jpg', 100));

            // Remove old file if the upload was successfull
            if (Storage::disk('public')->exists($oldProfileFileName)) {
                Storage::disk('public')->delete($oldProfileFileName);
            }
        }

        $this->commandBus->handle(new UpdateProfile(
            $userId,
            Auth::user()->getType(),
            $request->all(),
            $fileName
        ));
    }


    /**
     * @param UpdateCareclientProfile $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateCareclientProfile(UpdateCareclientProfile $request)
    {
        $careclientId = Auth::user()->getId();

        $this->updateProfile($careclientId, $request);

        // TODO: Careclient specific profile things here

        flashy()->success('Je profiel is aangepast.');

        return back();
    }

    /**
     * @param UpdateCareproviderProfile $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateCareproviderProfile(UpdateCareproviderProfile $request)
    {
        $careproviderId = Auth::user()->getId();

        $this->updateProfile($careproviderId, $request);

        $this->commandBus->handle(new UpdateCareproviderProperties(
            $careproviderId,
            $request->get('hourlyRate')
        ));

        flashy()->success('Je profiel is aangepast.');

        return back();
    }

    public function removeProfileImage(Request $request)
    {
        if (Storage::disk('public')->exists(Auth::user()->getProfileImage())) {
            Storage::disk('public')->delete(Auth::user()->getProfileImage());
        }

        $this->commandBus->handle(new RemoveProfileImage(
           Auth::user()->getId()
        ));

        flashy()->success('Je hebt je profiel afbeelding verwijderd.');

        return back();
    }
}
