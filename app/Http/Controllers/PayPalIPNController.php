<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\Tactician\CommandBus;
use PayPal\IPN\PPIPNMessage;
use Acme\Domain\Careclient\Commands\ActivateSubscription;

final class PayPalIPNController extends Controller
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * PayPalIPNController constructor.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @param Request $request
     */
    public function handleSubscriptionPurchasedIPNMessage(Request $request)
    {
        // Using null to just use all Request data.
        $ipnMessage = new PPIPNMessage(null, config('paypal.adaptive-payments'));
        $custom = json_decode($request->get('custom'));

        if ($ipnMessage->validate()) {

            $this->commandBus->handle(new ActivateSubscription($custom->userId, $custom->plan));

            Log::info('PayPal betaling ontvangen', [
                'id'   => $custom->userId,
                'plan' => $custom->plan
            ]);
        }
    }
}