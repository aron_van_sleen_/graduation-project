<?php

namespace App\Http\Controllers;

use App\Http\Requests\Hiring\RequestInterview as RequestInterviewRequest;
use Acme\App\ViewModels\UserProfileViewModel;
use Acme\Domain\Hiring\Commands\RequestInterview;
use Illuminate\Support\Facades\Auth;
use League\Tactician\CommandBus;
use Acme\Domain\Hiring\Exceptions\InterviewRequestAlreadyPending;
use Acme\Domain\User\UserRepository;

final class ProfileController extends Controller
{
    private $repository;
    private $commandBus;

    public function __construct(UserRepository $repository, CommandBus $commandBus)
    {
        $this->repository = $repository;
        $this->commandBus = $commandBus;
    }

    /**
     * @param $slug
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function showUserProfilePage($slug)
    {
        $user = $this->repository->findBySlug($slug);
        if ($user === null) {
            abort(404);
        }

        // Only allow user's of the other role to view the profile, unless it's your own profile
        if (Auth::user()->getType() === $user->getType() && Auth::user()->getId() !== $user->getId()) {
            flashy()->error('Je hebt geen toegang om dit profiel te bekijken.');
            return back();
        }

        $viewModel = new UserProfileViewModel($user);

        return view("pages.profile.{$user->getType()}-profile")->with([
            'metaTitle' => "Het profiel van $viewModel->name",
            'model'     => $viewModel,
            'slug'      => $slug
        ]);
    }

    /**
     * @param $slug
     *
     * @return $this
     */
    public function showRequestInterviewPage($slug)
    {
        $careprovider = $this->repository->findBySlug($slug);
        if ($careprovider === null) {
            abort(404);
        }

        $viewModel = new UserProfileViewModel($careprovider);

        return view('pages.profile.request-interview')->with([
            'metaTitle' => 'Kennismaken met ' . $viewModel->name,
            'model'     => $viewModel,
            'slug'      => $slug
        ]);
    }

    /**
     * @param RequestInterviewRequest $request
     * @param                         $slug
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function requestInterview(RequestInterviewRequest $request, $slug)
    {
        $careproviderId = $this->repository->findBySlug($slug)->getId();

        try {
            $this->commandBus->handle(
                new RequestInterview(
                    Auth::user()->getId(),
                    $careproviderId,
                    $request->get('dates')
                )
            );
            flashy()->success('Je verzoek is verstuurd!');
        } catch (InterviewRequestAlreadyPending $e) {
            flashy()->error('Er staat nog een verzoek open bij deze oppasser.');
        }

        return back();
    }
}