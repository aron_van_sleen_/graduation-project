<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Money\Money;
use Acme\App\Services\GeoLocationService;
use Acme\Domain\Careprovider\CareproviderSearchRepository;
use Acme\Domain\Careprovider\LevelRepository;
use Acme\Domain\Shared\TimeRange;
use Acme\Domain\User\CharacteristicRepository;
use Acme\Infrastructure\Careprovider\ElasticSearchCareproviderSearchRepository;

class SearchController extends Controller
{
    const CAREPROVIDERS_PER_PAGE = 9;

    /**
     * @var ElasticSearchCareproviderSearchRepository
     */
    protected $repository;

    /**
     * @var GeoLocationService
     */
    protected $geoService;

    public function __construct(CareproviderSearchRepository $searchRepository)
    {
        $this->repository = $searchRepository;
        $this->geoService = new GeoLocationService();
    }

    /**
     * @param SearchRequest            $request
     * @param LevelRepository          $levelRepository
     * @param CharacteristicRepository $characteristicRepository
     *
     * @return mixed
     */
    public function showResultsPage(SearchRequest $request, LevelRepository $levelRepository, CharacteristicRepository $characteristicRepository)
    {
        $results = [];
        $page = $request->has('page') ? $request->get('page') : 1;

        $characteristics = collect($characteristicRepository->findByType('careprovider'))->keyBy(function ($characteristic) {
            return $characteristic->getId();
        })->map(function ($characteristic) {
            return $characteristic->getLabel();
        });

        if ($request->all()) {
            $results = $this->search($request, $page);

            // Build up pagination for the search results
            $results = new LengthAwarePaginator($results["data"], $results["total"], self::CAREPROVIDERS_PER_PAGE, $page);
        }

        $request->flash();

        return view('pages.search')->with([
            'metaTitle'            => 'Zoeken naar oppassers',
            'results'              => $results,
            'paginationParameters' => Input::except('page'),
            'characteristics'      => $characteristics,
            'maximumHourlyRate'    => $levelRepository->findHighestMaximumHourlyRate()->getAmount()
        ]);
    }


    /**
     * @param SearchRequest $request
     *
     * @return $this|string
     */
    public function filterSearch(SearchRequest $request)
    {
        $page = $request->has('page') ? $request->get('page') : 1;
        $resultHtml = '';

        if ($request->all()) {
            $results = $this->search($request, $page);
            $results = new LengthAwarePaginator($results["data"], $results["total"], self::CAREPROVIDERS_PER_PAGE, $page);

            $resultHtml = view('partials.search.search-results')->with([
                'results'              => $results,
                'paginationParameters' => Input::except('page')
            ]);
        }

        return $resultHtml;
    }


    /**
     * @param SearchRequest $request
     * @param               $page
     * @param int           $perPage
     *
     * @return mixed
     */
    private function search(SearchRequest $request, $page = 1, $perPage = self::CAREPROVIDERS_PER_PAGE)
    {
        $date = Carbon::createFromTimestamp(strtotime($request->get('date')));
        $location = $this->geoService->addressToGeoLocation($request->get('location'));

        $this->repository->whereAvailable($date, new TimeRange($request->get('from'), $request->get('to')));
        $this->repository->whereWithinRadiusOfLocation($location, $request->get('radius'));

        if ($request->has('hourlyRate')) {
            $this->repository->whereBelowPrice(Money::EUR((int) $request->get('hourlyRate')));
        }

        if ($request->has('characteristics')) {
            $this->repository->whereNotHavingCharacteristics($request->get('characteristics'));
        }

        return $this->repository->paginate($page, $perPage);
    }
}