<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Money\Money;
use Acme\App\Services\PayPalPaymentGateway;
use Acme\Domain\Careclient\Plan;

class SubscriptionController extends Controller
{
    public function showSubscriptionPlansPage()
    {
        $plans = config('acme.subscription_plans');

        return view('subscription.select-plan')->with([
            'plans'     => $plans,
            'metaTitle' => 'Kies een abonnement'
        ]);
    }

    public function showSubscriptionCheckoutPage(Request $request)
    {
        $plans = collect(config('acme.subscription_plans'))->keyBy(function ($plan, $type) {
            return $type;
        })->map(function ($plan, $type) {
            return trans("subscriptions.{$type}.title") . ': &euro;' . number_format($plan['price'] / 100, 2, ',', '') . '  per jaar';
        });

        $request->flash();

        return view('subscription.checkout')->with([
            'plans'     => $plans,
            'metaTitle' => 'Door naar PayPal'
        ]);
    }

    /**
     * @param PayPalPaymentGateway $paymentGateway
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkoutToPayPal(Request $request, PayPalPaymentGateway $paymentGateway)
    {
        // Get all plan names and implode them to a string for validation
        $plans = collect(config('acme.subscription_plans'))->map(function ($plan, $title) {
            return $title;
        })->implode(',');

        // Validate the chosen plan
        $this->validate(
            $request,
            ['plan' => "in:{$plans}"],
            ['plan.in' => 'Het gekozen pakket bestaat niet.']
        );

        $plan = \EntityManager::getRepository(Plan::class)->findOneBy(['plan' => $request->get('plan')]);

        $payKey = $paymentGateway->sendSubscriptionRequest(Auth::user()->getId(), $plan);

        return redirect('https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=' . $payKey);
    }
}