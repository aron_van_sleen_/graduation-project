<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Auth;
use Acme\Domain\Careprovider\Careprovider;

class CareproviderOnly
{
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();
        if ($user === null) {
            return redirect()->route('auth.login');
        }

        // TODO: Refactor this so it uses an UserProvider returning domain users
        if ( ! $user instanceof Careprovider) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {

                flashy()->error('Alleen oppassers hebben toegang tot deze pagina.');

                return redirect()->route('home');
            }
        }

        return $next($request);
    }
}