<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Acme\Domain\Careclient\Careclient;

class NeedsActiveSubscription
{
    public function handle($request, Closure $next, $guard = null)
    {
        /** @var Careclient */
        $user = Auth::user();

        // If the user is a careclient withouth an active subscription, redirect to subscription plan page
        if ($user instanceof Careclient && ! $user->hasActiveSubscription()) {
            return redirect()->route('subscription.plans');
        }

        return $next($request);
    }
}