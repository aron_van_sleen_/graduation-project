<?php

namespace App\Http\Middleware;


use Auth;
use Closure;
use Acme\Domain\Careclient\Careclient;

class NeedsInactiveSubscription
{
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();

        if ($user instanceof Careclient && $user->hasActiveSubscription()) {
            flashy()->error('Je hebt al een actief abonnement');

            return redirect()->route('dashboard');
        }

        return $next($request);
    }
}