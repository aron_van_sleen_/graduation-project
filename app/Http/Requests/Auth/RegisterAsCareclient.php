<?php

namespace App\Http\Requests\Auth;

class RegisterAsCareclient extends RegisterAsMember
{
    public function rules()
    {
        $minAge = config('acme.min_careprovider_age');

        return array_merge([
            'date_of_birth' => "date| {$minAge} years ago"
        ], parent::rules());
    }
}