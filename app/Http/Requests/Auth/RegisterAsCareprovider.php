<?php

namespace App\Http\Requests\Auth;

class RegisterAsCareprovider extends RegisterAsMember
{
    public function rules()
    {
        $minAge = config('acme.min_careprovider_age');

        return array_merge([
            'date_of_birth' => "date|before: {$minAge} years ago"
        ], parent::rules());
    }
}