<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Acme\Domain\Shared\Password;
use Acme\Domain\Shared\PhoneNumber;

class RegisterAsMember extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Check if the user is already logged in, can't make the request then
        if (Auth::check()) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',

            'date_of_birth' => 'date|before:16 years ago',

            'street_name' => 'required',
            'street_number' => 'required',

            'postal_code' => 'required|regex:/^([0-9]{4})\s?([a-zA-Z]{2})$/',

            'city' => 'required',
            'phone_number' => ['required', 'regex:' . PhoneNumber::FORMAT],
            'email' => 'required|email|unique:Acme\Domain\User\User,email.email',
            
            'password' => ['required', 'min:8', 'regex:' . Password::FORMAT],
            'password_repeat' => 'required|same:password'
        ];
    }

    public function validate()
    {
        // Merge the date fields into one to use laravels validation on them
        $date = implode('-',[$this->input('birth_year'), $this->input('birth_month'), $this->input('birth_day')]);
        $postal_code = $this->input('postal_digits') . ' ' . $this->input('postal_characters');
        $phoneNumber = trim($this->input('phone_number'));

        // Merge new input together with old input
        $this->merge([
            'date_of_birth' => $date,
            'postal_code' => $postal_code,
            'phone_number' => $phoneNumber
        ]);

        parent::validate();
    }

    public function messages()
    {
        return [
            'date_of_birth.after' => trans('validation.too_old'),
            'date_of_birth.before' => trans('validation.too_young'),

            'postal_code.regex' => trans('validation.postal_code.invalid_format'),
            'phone_number.regex' => trans('validation.phone_number.invalid_format'),
            'password.regex' => trans('validation.password.invalid_format')
        ];
    }
}
