<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Acme\Domain\Shared\Password;

final class ResetPassword extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guest()) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required', 'min:8', 'confirmed', 'regex:' . Password::FORMAT]
        ];
    }
}