<?php
namespace App\Http\Requests\Dashboard;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class StateAvailabilityRequest extends Request
{
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }

        return false;
    }
    
    public function rules()
    {
        return [
            'dates.*' => 'required|date',
            'timeRanges.*' => 'valid_time_range',
        ];
    }
}