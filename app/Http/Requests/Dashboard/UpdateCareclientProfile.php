<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Support\Facades\Auth;

class UpdateCareclientProfile extends UpdateProfile
{
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        return false;
    }

    public function rules()
    {
        $rules = parent::rules();

        return array_merge($rules, [

        ]);
    }
}