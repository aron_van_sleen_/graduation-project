<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Support\Facades\Auth;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Careprovider\CareproviderRepository;

class UpdateCareproviderProfile extends UpdateProfile
{
    public function authorize()
    {
        if (Auth::check() && Auth::user()->getType() === Careprovider::CAREPROVIDER_TYPE) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $careprovider = Auth::user();
        $maxHourlyRate = $careprovider->getLevel()->getMaximumHourlyRate();

        return array_merge($rules, [
            'hourlyRate' => "numeric|between:0,{$maxHourlyRate->getAmount()}"
        ]);
    }

    public function messages()
    {
        return [
            'hourlyRate.between' => trans('validation.hourly_rate.over_max_amount')
        ];
    }
}
