<?php

namespace App\Http\Requests\Dashboard;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Acme\Domain\Shared\Password;
use Acme\Domain\Shared\PhoneNumber;

class UpdateProfile extends FormRequest
{
    public function rules()
    {
        return [
            'password'        => ['required_with:password_repeat', 'min:8', 'regex:' . Password::FORMAT],
            'password_repeat' => 'required_with:password|same:password',

            'first_name' => 'required',
            'last_name'  => 'required',

            'date_of_birth' => 'date|before:16 years ago',

            'street_name'   => 'required',
            'street_number' => 'required',

            'postal_code' => 'required|regex:/^([0-9]{4})\s?([a-zA-Z]{2})$/',

            'city'         => 'required',
            'phone_number' => ['required', 'regex:' . PhoneNumber::FORMAT],

            'profile_image' => 'mimes:jpg,jpeg,png'
        ];
    }

    public function validate()
    {
        // Merge the date fields into one to use laravels validation on them
        $date = implode('-', [$this->input('birth_year'), $this->input('birth_month'), $this->input('birth_day')]);
        $postal_code = $this->input('postal_digits') . ' ' . $this->input('postal_characters');
        $phoneNumber = trim($this->input('phone_number'));

        // Merge new input together with old input
        $this->merge([
            'date_of_birth' => $date,
            'postal_code'   => $postal_code,
            'phone_number'  => $phoneNumber
        ]);
        parent::validate();
    }

    public function messages()
    {
        return [
            'date_of_birth.before' => trans('validation.too_young'),
        ];
    }
}