<?php

namespace App\Http\Requests\Hiring;

use App\Http\Requests\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Acme\Domain\Careclient\Careclient;

class RequestInterview extends Request
{
    public function authorize()
    {
        if (Auth::check() && Auth::user()->getType() === Careclient::CARECLIENT_TYPE) {
            return true;
        }

        return false;
    }

    public function rules()
    {
        $date = Carbon::now();
        return [
            'dates'   => 'required|array|size:3',
            'dates.*' => 'date|after:' . $date
        ];
    }

    public function messages()
    {
        return [
            'dates.*.date'  => trans('validation.not_a_date'),
            'dates.*.after' => trans('validation.date_in_past')
        ];
    }
}