<?php

namespace App\Http\Requests;

use Acme\Domain\Careprovider\LevelRepository;

class SearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function validate()
    {
        // Only validate if there is atleast value
        if (count($this->input()) === 0) {
            return;
        }

        // Merge from and to into an array to easily validate it as a valid time range.
        $this->merge([
            'range' => [
                'from' => $this->input('from'),
                'to'   => $this->input('to')
            ]
        ]);

        parent::validate();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'              => 'required|date|after:yesterday',
            'from'              => 'required|numeric',
            'to'                => 'required|numeric',
            'location'          => 'required|string',
            'radius'            => 'required|numeric|in:5,10,20,50',
            'range'             => 'valid_time_range',
            'hourlyRate'        => 'numeric',
            'characteristics.*' => 'string'
        ];
    }

    public function messages()
    {
        return [
            'date.after' => trans('validation.date_future'),
        ];
    }
}