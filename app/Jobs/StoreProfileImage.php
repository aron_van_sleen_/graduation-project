<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class StoreProfileImage extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $file;
    private $fileName;
    private $oldFileName;

    /**
     * StoreProfileImage constructor.
     *
     * @param $file
     * @param $fileName
     * @param $oldFileName
     */
    public function __construct($file, $fileName, $oldFileName)
    {
        $this->file = $file;
        $this->fileName = $fileName;
        $this->oldFileName = $oldFileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    }
}
