<?php

namespace App\Listeners\Careprovider;


use Carbon\Carbon;
use Elasticsearch\Client;
use Illuminate\Support\Collection;
use Saferia\Domain\Careprovider\Events\CareproviderHasStatedAvailability;
use Saferia\Domain\Shared\TimeRange;

class IndexAvailabilityToElasticSearch
{
    /** @var  Client */
    private $elasticSearch;

    public function __construct()
    {
        $this->elasticSearch = app()->make('ElasticSearch');
    }

    public function handle(CareproviderHasStatedAvailability $event)
    {
        // If there are intersecting timeRanges, merge them together for easier searching.
        $timeRanges = $event->timeRanges->map(function (TimeRange $timeRange, $i) use ($event) {
            foreach ($event->timeRanges as $j => $other) {
                if ($i === $j) {
                    continue;
                }
                if ($timeRange->intersects($other)) {
                    return $timeRange->combine($other);
                }
            }
            return $timeRange;
        })->unique();

        foreach ($event->dates as $date) {
            $id = $event->careproviderId . '-' . $date->format('Y-m-d');

            // Add it to the index
            $params = [
                'index'  => 'saferia',
                'type'   => 'availability',
                'id'     => $id,
                'parent' => $event->careproviderId,
                'body'   => [
                    'date'        => $date->format('Y-m-d'),
                    'time_ranges' => []
                ]
            ];

            foreach ($timeRanges as $timeRange) {
                $params['body']['time_ranges'][] = [
                    'from' => $timeRange->from(),
                    'to'   => $timeRange->to()
                ];
            }
            $this->elasticSearch->index($params);
        };
    }
}