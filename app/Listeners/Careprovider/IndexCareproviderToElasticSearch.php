<?php

namespace App\Listeners\Careprovider;

use Saferia\App\Services\GeoLocationService;
use Saferia\Domain\Careprovider\Events\CareproviderHasRegistered;
use Saferia\Domain\Careprovider\Events\CareproviderHasUpdatedProfile;

class IndexCareproviderToElasticSearch
{
    protected $elasticSearch;
    protected $geoService;

    public function __construct()
    {
        $this->elasticSearch = app()->make('ElasticSearch');
        $this->geoService = new GeoLocationService();
    }

    /**
     * @param CareproviderHasRegistered|CareproviderHasUpdatedProfile $event
     */
    public function handle($event)
    {
        $location = $this->geoService->addressToGeoLocation((string) $event->address);

        $params = [
            'index' => 'saferia',
            'type'  => 'careprovider',
            'id'    => $event->id,
            'body'  => [
                'name'       => (string) $event->name,
                'city'       => $event->address->city,
                'hourlyRate' => $event->hourlyRate,
                'slug'       => $event->slug,

                'location' => [
                    'lat' => $location->getLatitude(),
                    'lon' => $location->getLongitude()
                ],

                'characteristics' => [],
            ]
        ];

        if (isset($event->characteristics)) {
            foreach ($event->characteristics as $characteristic) {
                $params['body']['characteristics'][] = $characteristic->getId();
            }
        }

        if (isset($event->profileImage) && $event->profileImage !== null) {
            $params['body']['profileImage'] = $event->profileImage;
        }

        $this->elasticSearch->index($params);
    }
}