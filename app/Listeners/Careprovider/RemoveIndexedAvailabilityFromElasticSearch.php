<?php

namespace App\Listeners\Careprovider;


use Saferia\Domain\Careprovider\Events\CareproviderHasUnstatedAvailability;

class RemoveIndexedAvailabilityFromElasticSearch
{
    /** @var  Client */
    private $elasticSearch;

    public function __construct()
    {
        $this->elasticSearch = app()->make('ElasticSearch');
    }

    public function handle(CareproviderHasUnstatedAvailability $event)
    {
        foreach ($event->dates as $date) {
            $id = $event->careproviderId . '-' . $date->format('Y-m-d');

            $params = [
                'index' => 'saferia',
                'type' => 'availability',
                'id' => $id,
                'parent' => $event->careproviderId,
            ];

            $this->elasticSearch->delete($params);
        }
    }
}