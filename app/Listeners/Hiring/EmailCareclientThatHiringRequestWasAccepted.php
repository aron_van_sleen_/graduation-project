<?php

namespace App\Listeners\Hiring;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Saferia\Domain\Hiring\Events\HiringRequestWasAccepted;

class EmailCareclientThatHiringRequestWasAccepted implements ShouldQueue
{
    public function handle(HiringRequestWasAccepted $event)
    {
        $careprovider = $event->careprovider;
        $careclient = $event->careclient;
        $date = $event->date;

        Mail::send('emails.hiring.request-hiring-accepted', [
            'careproviderName' => (string) $careprovider->getName(),
            'careclientName'   => (string) $careclient->getName(),
            'date'             => $date,
            'to'               => $date->copy()->addMinutes($event->duration)
        ], function ($mail) use ($careclient) {
            $mail->from("noreply@saferia.dev")->subject('Inhuur verzoek geaccepteerd.');
            $mail->to((string) $careclient->getEmail());
        });
    }
}