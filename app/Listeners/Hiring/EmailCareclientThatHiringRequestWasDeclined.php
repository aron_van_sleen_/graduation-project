<?php

namespace App\Listeners\Hiring;


use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Saferia\Domain\Hiring\Events\HiringRequestWasDeclined;

class EmailCareclientThatHiringRequestWasDeclined implements ShouldQueue
{
    public function handle(HiringRequestWasDeclined $event)
    {
        $careprovider = $event->careprovider;
        $careclient = $event->careclient;

        Mail::send('emails.hiring.request-hiring-declined', [
            'careproviderName' => (string) $careprovider->getName(),
            'careclientName'   => (string) $careclient->getName(),
        ], function ($mail) use ($careclient) {
            $mail->from("noreply@saferia.dev")->subject('Inhuur verzoek afgewezen');
            $mail->to((string) $careclient->getEmail());
        });
    }
}