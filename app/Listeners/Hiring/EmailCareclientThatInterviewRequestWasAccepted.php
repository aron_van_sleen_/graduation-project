<?php
namespace App\Listeners\Hiring;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Saferia\Domain\Hiring\Events\InterviewRequestWasAccepted;

class EmailCareclientThatInterviewRequestWasAccepted implements ShouldQueue
{
    public function handle(InterviewRequestWasAccepted $event)
    {
        $careprovider = $event->careprovider;
        $careclient = $event->careclient;
        $date = $event->acceptedDate;

        Mail::send('emails.hiring.request-interview-accepted', [
            'careproviderName' => (string) $careprovider->getName(),
            'careclientName'   => (string) $careclient->getName(),
            'date'             => $date
        ], function ($mail) use ($careclient) {
            $mail->from("noreply@saferia.dev")->subject('Interview verzoek geaccepteerd.');
            $mail->to((string) $careclient->getEmail());
        });
    }
}