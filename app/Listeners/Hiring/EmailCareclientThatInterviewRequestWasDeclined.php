<?php
namespace App\Listeners\Hiring;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Saferia\Domain\Hiring\Events\InterviewRequestWasDeclined;

class EmailCareclientThatInterviewRequestWasDeclined implements ShouldQueue
{
    public function handle(InterviewRequestWasDeclined $event)
    {
        $careprovider = $event->careprovider;
        $careclient = $event->careclient;

        Mail::send('emails.hiring.request-interview-declined', [
            'careproviderName' => (string) $careprovider->getName(),
            'careclientName'   => (string) $careclient->getName(),
        ], function ($mail) use ($careclient) {
            $mail->from("noreply@saferia.dev")->subject('Interview verzoek afgewezen');
            $mail->to((string) $careclient->getEmail());
        });
    }
}