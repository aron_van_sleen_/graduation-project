<?php
namespace App\Listeners\Hiring;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Saferia\Domain\Hiring\Events\InterviewWasRequested;

class EmailCareproviderAboutInterviewRequest implements ShouldQueue
{
    public function handle(InterviewWasRequested $event)
    {
        $careprovider = $event->careprovider;
        $careclient = $event->careclient;

        Mail::send('emails.hiring.request-interview', [
            'careproviderName' => (string) $careprovider->getName(),
            'careclientName'   => (string) $careclient->getName()
        ], function ($mail) use ($careprovider) {
            $mail->from("noreply@saferia.dev")->subject('Verzoek tot een interview');
            $mail->to((string) $careprovider->getEmail());
        });
    }
}