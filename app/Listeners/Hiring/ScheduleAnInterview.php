<?php

namespace App\Listeners\Hiring;


use League\Tactician\CommandBus;
use Saferia\Domain\Hiring\Commands\ScheduleInterview;
use Saferia\Domain\Hiring\Events\InterviewRequestWasAccepted;

class ScheduleAnInterview
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function handle(InterviewRequestWasAccepted $event)
    {
        $this->commandBus->handle(new ScheduleInterview(
            $event->careprovider->getId(),
            $event->careclient->getId(),
            $event->acceptedDate
        ));
    }
}