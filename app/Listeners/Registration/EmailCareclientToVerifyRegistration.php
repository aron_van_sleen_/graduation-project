<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Saferia\Domain\Careclient\Events\CareclientHasRegistered;

class EmailCareclientToVerifyRegistration implements ShouldQueue
{
    /**
     * @param CareclientHasRegistered $event
     */
    public function handle(CareclientHasRegistered $event)
    {

    }
}