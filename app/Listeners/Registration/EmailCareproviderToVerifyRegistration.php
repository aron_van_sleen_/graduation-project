<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Saferia\Domain\Careprovider\Events\CareproviderHasRegistered;

class EmailCareproviderToVerifyRegistration implements ShouldQueue
{
    public function handle(CareproviderHasRegistered $event)
    {
        
    }
}