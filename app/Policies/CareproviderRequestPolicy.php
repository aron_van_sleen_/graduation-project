<?php

namespace App\Policies;

use Saferia\Domain\Hiring\CareproviderRequest;
use Saferia\Domain\User\User;

class CareproviderRequestPolicy
{
    /**
     * @param User                $user
     * @param CareproviderRequest $request
     *
     * @return bool
     */
    public function accept(User $user, CareproviderRequest $request)
    {
        return $user->getId() === $request->getCareprovider()->getId();
    }

    /**
     * @param User                $user
     * @param CareproviderRequest $request
     *
     * @return bool
     */
    public function decline(User $user, CareproviderRequest $request)
    {
        return $user->getId() === $request->getCareprovider()->getId();
    }
}
