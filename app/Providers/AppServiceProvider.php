<?php

namespace App\Providers;

use Doctrine\ORM\Id\UuidGenerator;
use PayPal\Service\AdaptivePaymentsService;
use Saferia\App\Presenters\CustomPaginationPresenter;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Saferia\App\Services\BcryptHasher;
use Saferia\App\Services\HashingStrategy;
use Saferia\Domain\Careprovider\CareproviderSearchRepository;
use Saferia\Infrastructure\Careprovider\ElasticSearchCareproviderSearchRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // TODO: Move to session / Locale Service middleware
        setlocale(LC_ALL, 'nl_NL');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Paginator::presenter(function ($paginator) {
            return new CustomPaginationPresenter($paginator);
        });

        // Set default hasher to BcryptHasher
        $this->app->bind(HashingStrategy::class, function ($app) {
            return new BcryptHasher();
        });

        // Bind the PayPal Adaptive payments servic
        $this->app->bind(AdaptivePaymentsService::class, function () {
            return new AdaptivePaymentsService(config('paypal.adaptive-payments'));
        });
        // Bind ES searchRepository to the interface
        $this->app->bind(CareproviderSearchRepository::class, function () {
            return new ElasticSearchCareproviderSearchRepository();
        });
    }
}
