<?php

namespace App\Providers;

use App\Policies\CareproviderRequestPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Saferia\App\Doctrine\DoctrineUserProvider;
use Saferia\App\Services\BcryptHasher;
use Saferia\Domain\Careclient\Careclient;
use Saferia\Domain\Careprovider\Careprovider;
use Saferia\Domain\Hiring\HiringRequest;
use Saferia\Domain\Hiring\InterviewRequest;
use Saferia\Domain\User\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        HiringRequest::class    => CareproviderRequestPolicy::class,
        InterviewRequest::class => CareproviderRequestPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('view-careprovider-profile', function (User $user) {
            return $user->getType() === Careclient::CARECLIENT_TYPE;
        });

        $gate->define('view-careclient-profile', function (User $user) {
           return $user->getType() ===  Careprovider::CAREPROVIDER_TYPE;
        });

        Auth::provider('doctrine', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            return new DoctrineUserProvider(new BcryptHasher());
        });
    }
}
