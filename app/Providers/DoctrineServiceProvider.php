<?php

namespace App\Providers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use LaravelDoctrine\ORM\Configuration\Connections\ConnectionManager;
use LaravelDoctrine\ORM\DoctrineManager;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Saferia\App\Doctrine\CarbonDateTimeType;
use Saferia\App\Doctrine\CarbonDateType;
use Saferia\App\Doctrine\CarbonType;
use Saferia\App\Doctrine\CollectionType;
use Saferia\Domain\Careclient\Careclient;
use Saferia\Domain\Careclient\CareclientRepository;
use Saferia\Domain\Careclient\Plan;
use Saferia\Domain\Careclient\PlanRepository;
use Saferia\Domain\Careprovider\Careprovider;
use Saferia\Domain\Careprovider\CareproviderRepository;
use Saferia\Domain\Careprovider\Level;
use Saferia\Domain\Careprovider\LevelRepository;
use Saferia\Domain\Hiring\CareproviderRequest;
use Saferia\Domain\Hiring\CareproviderRequestRepository;
use Saferia\Domain\Hiring\Interview;
use Saferia\Domain\Hiring\InterviewRepository;
use Saferia\Domain\User\Characteristic;
use Saferia\Domain\User\CharacteristicRepository;
use Saferia\Domain\User\User;
use Saferia\Domain\User\UserRepository;
use Saferia\Infrastructure\Careclient\DoctrineCareclientRepository;
use Saferia\Infrastructure\Careclient\DoctrinePlanRepository;
use Saferia\Infrastructure\Careprovider\DoctrineCareproviderRepository;
use Saferia\Infrastructure\Careprovider\DoctrineLevelRepository;
use Saferia\Infrastructure\Hiring\DoctrineCareproviderRequestRepository;
use Saferia\Infrastructure\Hiring\DoctrineInterviewRepository;
use Saferia\Infrastructure\User\DoctrineCharacteristicRepository;
use Saferia\Infrastructure\User\DoctrineUserRepository;

class DoctrineServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Make Doctrine use Carbon for datetimes
        Type::overrideType('date', CarbonDateType::class);
        Type::overrideType('datetime', CarbonDateTimeType::class);
        Type::overrideType('array', CollectionType::class);

        $this->app->bind(UserRepository::class, function ($app) {
            return new DoctrineUserRepository(
                $app['em'],
                $app['em']->getClassMetaData(User::class)
            );
        });

        $this->app->bind(CareproviderRepository::class, function ($app) {
            return new DoctrineCareproviderRepository(
                $app['em'],
                $app['em']->getClassMetaData(Careprovider::class)
            );
        });

        $this->app->bind(CareclientRepository::class, function ($app) {
            return new DoctrineCareclientRepository(
                $app['em'],
                $app['em']->getClassMetaData(Careclient::class)
            );
        });

        $this->app->bind(LevelRepository::class, function ($app) {
            return new DoctrineLevelRepository(
                $app['em'],
                $app['em']->getClassMetaData(Level::class)
            );
        });

        $this->app->bind(InterviewRepository::class, function ($app) {
            return new DoctrineInterviewRepository(
                $app['em'],
                $app['em']->getClassMetaData(Interview::class)
            );
        });

        $this->app->bind(CharacteristicRepository::class, function ($app) {
            return new DoctrineCharacteristicRepository(
                $app['em'],
                $app['em']->getClassMetaData(Characteristic::class)
            );
        });

        $this->app->bind(CareproviderRequestRepository::class, function ($app) {
            return new DoctrineCareproviderRequestRepository(
                $app['em'],
                $app['em']->getClassMetaData(CareproviderRequest::class)
            );
        });

        $this->app->bind(PlanRepository::class, function ($app) {
            return new DoctrinePlanRepository(
                $app['em'],
                $app['em']->getClassMetaData(Plan::class)
            );
        });
    }
}