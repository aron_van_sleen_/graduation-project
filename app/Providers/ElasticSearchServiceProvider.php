<?php

namespace App\Providers;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\ServiceProvider;

class ElasticSearchServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ElasticSearch', function ($app) {
            $config = config('elasticsearch');

            $logger = ClientBuilder::defaultLogger($config['logPath']);

            $client = ClientBuilder::create()
                ->setHosts($config['hosts'])
                ->setLogger($logger)
                ->build();

            $this->applyMappings($client);

            return $client;
        });
    }

    private function applyMappings(Client $client)
    {
        $indexParams['index'] = 'saferia';
        // Do nothing if index exists
        if ($client->indices()->exists($indexParams)) {
            return;
        }

        $params = config('elasticsearch.mappings');

        $client->indices()->create($params);
    }
}