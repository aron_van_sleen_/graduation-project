<?php

namespace App\Providers;

use App\Listeners\Careprovider\IndexAvailabilityToElasticSearch;
use App\Listeners\Careprovider\IndexCareproviderToElasticSearch;
use App\Listeners\Careprovider\RemoveIndexedAvailabilityFromElasticSearch;
use App\Listeners\Hiring\BookAnAppointment;
use App\Listeners\Hiring\EmailCareclientThatHiringRequestWasAccepted;
use App\Listeners\Hiring\EmailCareclientThatHiringRequestWasDeclined;
use App\Listeners\Hiring\EmailCareclientThatInterviewRequestWasAccepted;
use App\Listeners\Hiring\EmailCareclientThatInterviewRequestWasDeclined;
use App\Listeners\Hiring\EmailCareproviderAboutInterviewRequest;
use App\Listeners\Hiring\ScheduleAnInterview;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Saferia\Domain\Careclient\Events\CareclientHasRegistered;
use Saferia\Domain\Careprovider\Events\CareproviderHasRegistered;
use Saferia\Domain\Careprovider\Events\CareproviderHasStatedAvailability;
use Saferia\Domain\Careprovider\Events\CareproviderHasUnstatedAvailability;
use Saferia\Domain\Careprovider\Events\CareproviderHasUpdatedProfile;
use Saferia\Domain\Hiring\Events\HiringRequestWasAccepted;
use Saferia\Domain\Hiring\Events\HiringRequestWasDeclined;
use Saferia\Domain\Hiring\Events\InterviewRequestWasAccepted;
use Saferia\Domain\Hiring\Events\InterviewRequestWasDeclined;
use Saferia\Domain\Hiring\Events\InterviewWasRequested;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /** Registration events */
        CareproviderHasRegistered::class => [

        ],
        CareclientHasRegistered::class   => [

        ],

        /** Careprovider events */
        CareproviderHasRegistered::class           => [
            IndexCareproviderToElasticSearch::class
        ],
        CareproviderHasUpdatedProfile::class       => [
            IndexCareproviderToElasticSearch::class
        ],
        CareproviderHasStatedAvailability::class   => [
            IndexAvailabilityToElasticSearch::class
        ],
        CareproviderHasUnstatedAvailability::class => [
            RemoveIndexedAvailabilityFromElasticSearch::class
        ],

        /** Hiring events */
        InterviewWasRequested::class               => [
            EmailCareproviderAboutInterviewRequest::class
        ],
        InterviewRequestWasAccepted::class         => [
            EmailCareclientThatInterviewRequestWasAccepted::class,
            ScheduleAnInterview::class
        ],
        InterviewRequestWasDeclined::class         => [
            EmailCareclientThatInterviewRequestWasDeclined::class
        ],
        HiringRequestWasAccepted::class            => [
            EmailCareclientThatHiringRequestWasAccepted::class,
            BookAnAppointment::class,
        ],
        HiringRequestWasDeclined::class            => [
            EmailCareclientThatHiringRequestWasDeclined::class
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     *
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
    }
}
