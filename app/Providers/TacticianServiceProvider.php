<?php

namespace App\Providers;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\ServiceProvider;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\CallableLocator;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;

class TacticianServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('League\Tactician\CommandBus', function ($app) {
            $nameExtractor = new ClassNameExtractor();

            $locator = new CallableLocator(function ($commandName) {
                // Replaces /Commands/UseCase to /Handlers/UseCaseHandler
                $handlerName = str_replace('Command', 'Handler', $commandName) . 'Handler';

                // Let the Laravel service container resolve the handler class so dependencies are injected.
                return app($handlerName);
            });

            // Makes the command bus call the handle method on the Handler classes
            $inflector = new HandleInflector();

            $bus = new CommandBus([new CommandHandlerMiddleware($nameExtractor, $locator, $inflector)]);

            return $bus;
        });
    }
}