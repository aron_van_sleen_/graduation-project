<?php

namespace App\Providers;

use Saferia\Domain\Careprovider\Exceptions\AvailabilityTimeRangesCannotOverlap;
use Saferia\Domain\Shared\TimeRange;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('valid_time_range', function ($attribute, $value, $parameters, $validator) {
            return $value["from"] < $value["to"];
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }
}