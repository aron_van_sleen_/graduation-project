<?php


return [
    'min_careclient_age' => 16,
    'min_careprovider_age' => 16,

    'subscription_plans' => [
        'bronze' => [
            'price' => 750,
        ],
        'silver' => [
            'price' => 1000,
        ],
        'gold' => [
            'price' => 1250,
        ],
        'platinum' => [
            'price' => 1750,
        ]
    ]
];