<?php
return [
    /*
     * Specify the host(s) where elasticsearch is running.
     */
    'hosts'            => [
        'localhost:9200',
    ],
    /*
     * Specify the path where Elasticsearch will write it's logs.
     */
    'logPath'          => storage_path() . '/logs/elasticsearch.log',

    /*
     * Specify how verbose the logging must be
     * Possible values are listed here
     * https://github.com/Seldaek/monolog/blob/master/src/Monolog/Logger.php
     *
     */
    'logLevel'         => 200,

    /*
     * The name of the index elasticsearch will write to.
     */
    'defaultIndexName' => 'acme',


    'mappings' => [
        'index' => 'acme',
        'body'  => [
            'settings' => [
                'number_of_shards'   => 1,
                'number_of_replicas' => 0,
            ],
            'mappings' => [
                'careprovider' => ['properties' => [
                    'location'        => ['type' => 'geo_point'],
                    'characteristics' => [
                        'type'  => 'string',
                        'index' => 'not_analyzed'
                    ]
                ]],
                'availability' => [
                    '_parent'    => ['type' => 'careprovider'],
                    'properties' => [
                        'date'        => ['type' => 'date'],
                        'time_ranges' => [
                            'type'       => 'nested',
                            'properties' => [
                                'from' => ['type' => 'integer'],
                                'to'   => ['type' => 'integer']
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];