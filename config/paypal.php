<?php
return [
    'adaptive-payments' => [
        'mode'            => env('PAYPAL_MODE'),
        'acct1.UserName'  => env('PAYPAL_USERNAME'),
        'acct1.Password'  => env('PAYPAL_PASSWORD'),
        'acct1.Signature' => env('PAYPAL_SIGNATURE'),
        'acct1.AppId'     => env('PAYPAL_APP_ID'),
        'log.logEnabled'  => true,
        'log.FileName'    => storage_path('logs') . '/PayPal.log',
        'log.logLevel'    => 'INFO'
    ]
];