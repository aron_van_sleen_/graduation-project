<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160721115354 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE careproviders ADD level INT DEFAULT NULL');
        $this->addSql('ALTER TABLE careproviders ADD CONSTRAINT FK_9A1634CA9AEACC13 FOREIGN KEY (level) REFERENCES levels (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9A1634CA9AEACC13 ON careproviders (level)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE careproviders DROP FOREIGN KEY FK_9A1634CA9AEACC13');
        $this->addSql('DROP INDEX UNIQ_9A1634CA9AEACC13 ON careproviders');
        $this->addSql('ALTER TABLE careproviders DROP level');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
    }
}
