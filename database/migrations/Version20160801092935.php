<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160801092935 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users_characteristics (user_id INT NOT NULL, characteristic_id INT NOT NULL, INDEX IDX_CB424ECCA76ED395 (user_id), INDEX IDX_CB424ECCDEE9D12B (characteristic_id), PRIMARY KEY(user_id, characteristic_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE characteristics (id INT AUTO_INCREMENT NOT NULL, characteristic VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_characteristics ADD CONSTRAINT FK_CB424ECCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users_characteristics ADD CONSTRAINT FK_CB424ECCDEE9D12B FOREIGN KEY (characteristic_id) REFERENCES characteristics (id)');
        $this->addSql('ALTER TABLE users ADD qualities LONGTEXT DEFAULT NULL, CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE careproviders DROP qualities');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_characteristics DROP FOREIGN KEY FK_CB424ECCDEE9D12B');
        $this->addSql('DROP TABLE users_characteristics');
        $this->addSql('DROP TABLE characteristics');
        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE careproviders ADD qualities LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE users DROP qualities, CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
    }
}
