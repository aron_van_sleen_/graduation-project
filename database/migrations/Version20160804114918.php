<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160804114918 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE interview_requests (id INT AUTO_INCREMENT NOT NULL, careclient_id INT DEFAULT NULL, careprovider_id INT DEFAULT NULL, requested_dates LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_124D15B261D889FC (careclient_id), INDEX IDX_124D15B29ECF89B3 (careprovider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE interview_requests ADD CONSTRAINT FK_124D15B261D889FC FOREIGN KEY (careclient_id) REFERENCES careclients (id)');
        $this->addSql('ALTER TABLE interview_requests ADD CONSTRAINT FK_124D15B29ECF89B3 FOREIGN KEY (careprovider_id) REFERENCES careproviders (id)');
        $this->addSql('DROP TABLE interviews');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE interviews (id INT AUTO_INCREMENT NOT NULL, careclient_id INT DEFAULT NULL, careprovider_id INT DEFAULT NULL, discr VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_3A7526829ECF89B3 (careprovider_id), INDEX IDX_3A75268261D889FC (careclient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE interviews ADD CONSTRAINT FK_3A75268261D889FC FOREIGN KEY (careclient_id) REFERENCES careclients (id)');
        $this->addSql('ALTER TABLE interviews ADD CONSTRAINT FK_3A7526829ECF89B3 FOREIGN KEY (careprovider_id) REFERENCES careproviders (id)');
        $this->addSql('DROP TABLE interview_requests');
        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
    }
}
