<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160810090939 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("CREATE TABLE jobs (
                      id BIGINT AUTO_INCREMENT NOT NULL, 
                      queue VARCHAR(255) NOT NULL, 
                      payload LONGTEXT NOT NULL, 
                      attempts TINYINT UNSIGNED NOT NULL,
                      reserved TINYINT UNSIGNED NOT NULL,
                      reserved_at INT UNSIGNED,
                      available_at INT UNSIGNED NOT NULL,
                      created_at INT UNSIGNED NOT NULL,
                      PRIMARY KEY (id),
                      INDEX(queue, reserved, reserved_at)
                  )");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE jobs');
    }


//Schema::create('jobs', function (Blueprint $table) {
//    $table->bigIncrements('id');
//    $table->string('queue');
//    $table->longText('payload');
//    $table->tinyInteger('attempts')->unsigned();
//    $table->tinyInteger('reserved')->unsigned();
//    $table->unsignedInteger('reserved_at')->nullable();
//    $table->unsignedInteger('available_at');
//    $table->unsignedInteger('created_at');
//    $table->index(['queue', 'reserved', 'reserved_at']);
//});
}
