<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160810140917 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE relationship_with_careproviders (careclient_id INT NOT NULL, careprovider_id INT NOT NULL, part_of_careteam TINYINT(1) NOT NULL, favorited TINYINT(1) NOT NULL, approved_by_careprovider TINYINT(1) NOT NULL, approved_careprovider TINYINT(1) NOT NULL, INDEX IDX_F652EB3561D889FC (careclient_id), INDEX IDX_F652EB359ECF89B3 (careprovider_id), PRIMARY KEY(careclient_id, careprovider_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE relationship_with_careproviders ADD CONSTRAINT FK_F652EB3561D889FC FOREIGN KEY (careclient_id) REFERENCES careclients (id)');
        $this->addSql('ALTER TABLE relationship_with_careproviders ADD CONSTRAINT FK_F652EB359ECF89B3 FOREIGN KEY (careprovider_id) REFERENCES careproviders (id)');
        $this->addSql('DROP TABLE jobs');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE jobs (id BIGINT AUTO_INCREMENT NOT NULL, queue VARCHAR(255) NOT NULL COLLATE utf8_general_ci, payload LONGTEXT NOT NULL COLLATE utf8_general_ci, attempts TINYINT(1) NOT NULL, reserved TINYINT(1) NOT NULL, reserved_at INT UNSIGNED DEFAULT NULL, available_at INT UNSIGNED NOT NULL, created_at INT UNSIGNED NOT NULL, INDEX queue (queue, reserved, reserved_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE relationship_with_careproviders');
        $this->addSql('ALTER TABLE availability CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
    }
}
