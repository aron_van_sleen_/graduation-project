<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160811071421 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hiring_requests (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
        $this->addSql('ALTER TABLE availability ADD `from` SMALLINT NOT NULL, ADD `to` SMALLINT NOT NULL, DROP time_ranges, CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE careproviders_availability DROP FOREIGN KEY FK_C5B1095F61778466');
        $this->addSql('ALTER TABLE careproviders_availability ADD CONSTRAINT FK_C5B1095F61778466 FOREIGN KEY (availability_id) REFERENCES availability (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE hiring_requests');
        $this->addSql('ALTER TABLE availability ADD time_ranges LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\', DROP `from`, DROP `to`, CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE careproviders_availability DROP FOREIGN KEY FK_C5B1095F61778466');
        $this->addSql('ALTER TABLE careproviders_availability ADD CONSTRAINT FK_C5B1095F61778466 FOREIGN KEY (availability_id) REFERENCES availability (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
    }
}
