<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160811100702 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE careproviders_availability');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
        $this->addSql('ALTER TABLE availability ADD careprovider_id INT DEFAULT NULL, CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE availability ADD CONSTRAINT FK_3FB7A2BF9ECF89B3 FOREIGN KEY (careprovider_id) REFERENCES careproviders (id)');
        $this->addSql('CREATE INDEX IDX_3FB7A2BF9ECF89B3 ON availability (careprovider_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE careproviders_availability (careprovider_id INT NOT NULL, availability_id INT NOT NULL, UNIQUE INDEX UNIQ_C5B1095F61778466 (availability_id), INDEX IDX_C5B1095F9ECF89B3 (careprovider_id), PRIMARY KEY(careprovider_id, availability_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE careproviders_availability ADD CONSTRAINT FK_C5B1095F61778466 FOREIGN KEY (availability_id) REFERENCES availability (id)');
        $this->addSql('ALTER TABLE careproviders_availability ADD CONSTRAINT FK_C5B1095F9ECF89B3 FOREIGN KEY (careprovider_id) REFERENCES careproviders (id)');
        $this->addSql('ALTER TABLE availability DROP FOREIGN KEY FK_3FB7A2BF9ECF89B3');
        $this->addSql('DROP INDEX IDX_3FB7A2BF9ECF89B3 ON availability');
        $this->addSql('ALTER TABLE availability DROP careprovider_id, CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE users CHANGE date_of_birth date_of_birth DATETIME NOT NULL');
    }
}
