<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160812135925 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("CREATE TABLE t_jobs (
                      id BIGINT AUTO_INCREMENT NOT NULL, 
                      queue VARCHAR(255) NOT NULL, 
                      payload LONGTEXT NOT NULL, 
                      attempts TINYINT UNSIGNED NOT NULL,
                      reserved TINYINT UNSIGNED NOT NULL,
                      reserved_at INT UNSIGNED,
                      available_at INT UNSIGNED NOT NULL,
                      created_at INT UNSIGNED NOT NULL,
                      PRIMARY KEY (id),
                      INDEX(queue, reserved, reserved_at)
                  )");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE t_jobs');
    }
}
