<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160815092558 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE careprovider_requests (id INT AUTO_INCREMENT NOT NULL, careclient_id INT DEFAULT NULL, careprovider_id INT DEFAULT NULL, discr VARCHAR(255) NOT NULL, requested_dates LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', suggested_date DATE DEFAULT NULL, `from` SMALLINT DEFAULT NULL, `to` SMALLINT DEFAULT NULL, INDEX IDX_E633A36F61D889FC (careclient_id), INDEX IDX_E633A36F9ECF89B3 (careprovider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE careprovider_requests ADD CONSTRAINT FK_E633A36F61D889FC FOREIGN KEY (careclient_id) REFERENCES careclients (id)');
        $this->addSql('ALTER TABLE careprovider_requests ADD CONSTRAINT FK_E633A36F9ECF89B3 FOREIGN KEY (careprovider_id) REFERENCES careproviders (id)');
        $this->addSql('DROP TABLE hiring_requests');
        $this->addSql('DROP TABLE interview_requests');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hiring_requests (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interview_requests (id INT AUTO_INCREMENT NOT NULL, careclient_id INT DEFAULT NULL, careprovider_id INT DEFAULT NULL, requested_dates LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\', INDEX IDX_124D15B261D889FC (careclient_id), INDEX IDX_124D15B29ECF89B3 (careprovider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE interview_requests ADD CONSTRAINT FK_124D15B261D889FC FOREIGN KEY (careclient_id) REFERENCES careclients (id)');
        $this->addSql('ALTER TABLE interview_requests ADD CONSTRAINT FK_124D15B29ECF89B3 FOREIGN KEY (careprovider_id) REFERENCES careproviders (id)');
        $this->addSql('DROP TABLE careprovider_requests');
    }
}
