<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160815112735 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE careprovider_requests ADD duration INT DEFAULT NULL, DROP `from`, DROP `to`, CHANGE suggested_date suggested_date DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE careprovider_requests ADD `from` SMALLINT DEFAULT NULL, ADD `to` SMALLINT DEFAULT NULL, DROP duration, CHANGE suggested_date suggested_date DATE DEFAULT NULL');
    }
}
