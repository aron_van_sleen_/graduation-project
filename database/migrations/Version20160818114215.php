<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160818114215 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE careproviders DROP INDEX UNIQ_9A1634CA5FB14BA7, ADD INDEX IDX_9A1634CA5FB14BA7 (level_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE careproviders DROP INDEX IDX_9A1634CA5FB14BA7, ADD UNIQUE INDEX UNIQ_9A1634CA5FB14BA7 (level_id)');
    }
}
