<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160823083457 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE subscriptions (id INT AUTO_INCREMENT NOT NULL, careclient_id INT DEFAULT NULL, active_since DATETIME DEFAULT NULL, expires_on DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_4778A0161D889FC (careclient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subscriptions ADD CONSTRAINT FK_4778A0161D889FC FOREIGN KEY (careclient_id) REFERENCES careclients (id)');
        $this->addSql('ALTER TABLE careclients ADD subscription_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE careclients ADD CONSTRAINT FK_476E9A1D9A1887DC FOREIGN KEY (subscription_id) REFERENCES subscriptions (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_476E9A1D9A1887DC ON careclients (subscription_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE careclients DROP FOREIGN KEY FK_476E9A1D9A1887DC');
        $this->addSql('DROP TABLE subscriptions');
        $this->addSql('DROP INDEX UNIQ_476E9A1D9A1887DC ON careclients');
        $this->addSql('ALTER TABLE careclients DROP subscription_id');
    }
}
