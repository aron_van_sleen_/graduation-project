<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20160823112045 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE plans (id INT AUTO_INCREMENT NOT NULL, plan VARCHAR(255) NOT NULL, price INT NOT NULL, UNIQUE INDEX UNIQ_356798D1DD5A5B7D (plan), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subscriptions ADD plan INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subscriptions ADD CONSTRAINT FK_4778A01DD5A5B7D FOREIGN KEY (plan) REFERENCES plans (id)');
        $this->addSql('CREATE INDEX IDX_4778A01DD5A5B7D ON subscriptions (plan)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscriptions DROP FOREIGN KEY FK_4778A01DD5A5B7D');
        $this->addSql('DROP TABLE plans');
        $this->addSql('DROP INDEX IDX_4778A01DD5A5B7D ON subscriptions');
        $this->addSql('ALTER TABLE subscriptions DROP plan');
    }
}
