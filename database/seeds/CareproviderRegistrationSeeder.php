<?php


use Elasticsearch\Client;
use Illuminate\Database\Seeder;
use League\Tactician\CommandBus;
use Acme\Domain\Careprovider\Commands\RegisterAsCareprovider;

class CareproviderRegistrationSeeder extends Seeder
{
    /** @var  Client */
    private $client;

    private $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 100; $i++)
        {
            $params = [];

            $params["first_name"] = $faker->firstName;
            $params["last_name"] = $faker->lastName;

            $params["birth_year"] = $faker->year;
            $params["birth_month"] = $faker->month;
            $params["birth_day"] = $faker->dayOfMonth;
            $params["street_name"] = "";
            $params["street_number"] = "";
            $params["postal_code"] = "";
            $params["city"] = "Amsterdam";
            $params["email"] = $faker->email;
            $params["phone_number"] = '0612345678';
            $params["password"] = 'Veilig123!';

            $command = new RegisterAsCareprovider($params);
            $this->bus->handle($command);
        }
    }
}