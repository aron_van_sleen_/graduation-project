<?php

use Elasticsearch\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Validator;

class ElasticSearchCareproviderSeeder extends Seeder
{
    /** @var  Client */
    private $client;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->client = app()->make('ElasticSearch');
        $faker = \Faker\Factory::create();

        $possibleCharacteristics = [
            [],
            ["smokes"],
            ["religious"],
            ["smokes", "religious"]
        ];

        $params = ['body' => []];
        for ($i = 0; $i < 500; $i++)
        {
            $params['body'][] = [
                'index' => [
                    '_index' => 'acme',
                    '_type' => 'careprovider',
                    '_id' => $i
                ]
            ];

            $params['body'][] = [
                'name' => $faker->name,
                'characteristics' => $possibleCharacteristics[rand(0, 3)],
                'city' => $faker->city,
                'location' => [
                    'lat' => '52.' . mt_rand(366000,512999),
                    'lon' => '4.' . mt_rand(800000,900000)
                ],
                'hourly_rate' => mt_rand(300, 1000)
            ];

            if ($i % 1000 == 0)
            {
                $responses = $this->client->bulk($params);

                $params = ['body' => []];

                unset($responses);
            }
        }
        // Send the last batch if it exists
        if (!empty($params['body'])) {
            $responses = $this->client->bulk($params);
        }
    }
}