<?php

use Illuminate\Database\Seeder;
use Money\Money;
use Acme\Domain\Careclient\Plan;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TODO: Refactor to a laravel command, a seeder's job is not to alter existing things?
        $repository = EntityManager::getRepository(Plan::class);

        foreach (config('acme.subscription_plans') as $planName => $properties) {

            $plan = $repository->findOneBy(['plan' => $planName]);

            if ($plan === null) {
                $plan = new Plan($planName, Money::EUR($properties['price']));
            } else {
                $plan->changePrice(Money::EUR($properties['price']));
            }

            EntityManager::persist($plan);
        }

        EntityManager::flush();
    }
}