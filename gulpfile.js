var elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.sass(['app.scss'], 'resources/assets/css/app.css');
    mix.styles(['app.css', 'jquery-ui.min.css'], 'public/css/app.css');

    mix.scripts([
        'libs/jquery.min.js',
        'libs/fastclick.js',
        'libs/foundation.min.js',
        'libs/underscore-min.js',
        'libs/foundation-datepicker.min.js',
        'libs/locales/foundation-datepicker.nl.js'
    ], 'public/js/vendor.js');

    mix.browserify('registration.js');
    mix.browserify('search-results.js');
    mix.browserify('search.js');
    mix.browserify('dashboard/edit-profile.js');
    mix.browserify('dashboard/careprovider-requests.js');
    mix.browserify('dashboard/availability-calendar.js');
});
