(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Availability = function () {

    /** Creates a new avaialbility collection for data management */

    function Availability() {
        var availability = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

        _classCallCheck(this, Availability);

        this.availability = availability;
    }

    /** Fetches the availability of the current user from the server */


    _createClass(Availability, [{
        key: 'fetchAvailabilityBetweenDates',
        value: function fetchAvailabilityBetweenDates(startDate, endDate, callback) {
            var _this = this;

            $.ajax({
                url: '/availability',
                data: {
                    from: startDate, to: endDate
                }
            }).done(function (results) {
                // Map the object to an array
                _this.availability = [];
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = Object.keys(results)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var date = _step.value;

                        _this.availability[date] = results[date];
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                if (callback) {
                    callback(_this.availability);
                }
            });
        }

        /** Set the availability for a date */

    }, {
        key: 'updateAvailability',
        value: function updateAvailability(dates, timeRanges) {
            var _this2 = this;

            // Return this as a promise, so the calendar can react to the response
            if (!(dates instanceof Array)) {
                dates = [dates];
            }

            return $.ajax({
                url: '/availability',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                }, data: {
                    dates: dates,
                    timeRanges: timeRanges
                }
            }).done(function () {
                // Sync the availability array to match the result
                if (timeRanges.length === 0) {
                    var _iteratorNormalCompletion2 = true;
                    var _didIteratorError2 = false;
                    var _iteratorError2 = undefined;

                    try {
                        for (var _iterator2 = dates[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            var date = _step2.value;

                            delete _this2.availability[date];
                        }
                    } catch (err) {
                        _didIteratorError2 = true;
                        _iteratorError2 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }
                        } finally {
                            if (_didIteratorError2) {
                                throw _iteratorError2;
                            }
                        }
                    }
                } else {
                    var _iteratorNormalCompletion3 = true;
                    var _didIteratorError3 = false;
                    var _iteratorError3 = undefined;

                    try {
                        for (var _iterator3 = dates[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            var _date = _step3.value;


                            _this2.availability[_date] = timeRanges;
                        }
                    } catch (err) {
                        _didIteratorError3 = true;
                        _iteratorError3 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                _iterator3.return();
                            }
                        } finally {
                            if (_didIteratorError3) {
                                throw _iteratorError3;
                            }
                        }
                    }
                }
            });
        }

        /** Returns an array with timeRanges for it's date */

    }, {
        key: 'getTimeRangesForDate',
        value: function getTimeRangesForDate(dates) {
            if (dates instanceof Array) {
                return []; // TODO: Timeranges on week select
            } else if (this.availability.hasOwnProperty(dates)) {
                // Need to copy the array, otherwise it's passed as reference and will be altered 
                return JSON.parse(JSON.stringify(this.availability[dates]));
            }

            return [];
        }
    }]);

    return Availability;
}();

exports.default = Availability;

},{}],2:[function(require,module,exports){
'use strict';

var _Availability = require('./Availability');

var _Availability2 = _interopRequireDefault(_Availability);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

(function ($) {
    var maxFieldSetAmount = 3;
    var gridItemSelector = '.availability-calendar__item:not(.is-unavailable)';
    var weekClassName = 'availability-calendar__item--week';
    var selectedClassName = 'is-selected';
    var availableClassName = 'is-available';
    var formShownClassName = 'is-shown';
    var form = $('.availability-calendar__form');
    var formPointer = $('.availability-calendar__form__pointer');

    // Temporary data for the UI to use before persisting it.
    var selectedDate = void 0;
    var selectedTimeRanges = [];
    var errorBag = [];

    var availability = new _Availability2.default();

    // Update the calendar to show the available days
    var updateCalendar = function updateCalendar() {
        // Remove all, so outdated classes are removed
        $(gridItemSelector).removeClass(availableClassName);

        for (var date in availability.availability) {
            // Apply classes to the relevant dates
            $('[data-date=' + date + ']').addClass(availableClassName);
        }
    };

    /** Positions the forms and renders it under the given element */
    var showFormUnderElement = function showFormUnderElement(element) {
        form.addClass(formShownClassName);
        renderFieldSets();

        var position = element.position();
        var width = element.outerWidth();
        var height = element.outerHeight();
        var formWidth = form.outerWidth();
        var windowWidth = $(window).width();
        var formLeftPosition = position.left;

        if (windowWidth <= formWidth) {
            // Make it full screen
            formLeftPosition = 0;
        } else if (position.left + formWidth + width >= windowWidth) {
            // Make sure form doesn't go out of the edges
            formLeftPosition = position.left - formWidth + width;
        }

        form.css({
            top: position.top + height, left: formLeftPosition
        });
        // Position the pointer to be centered with the button
        formPointer.css({
            left: position.left - form.position().left + width / 2 - formPointer.outerWidth() / 2
        });
    };

    var hideForm = function hideForm() {
        form.removeClass(formShownClassName);
    };

    /** Build up the fieldsets for the form depending on selectedTimeRanges */
    var renderFieldSets = function renderFieldSets() {
        var html = '';
        var count = 0;

        selectedTimeRanges.forEach(function (timeRange) {
            html += generateFieldset(count, timeRange);
            count++;
        });

        // If the max has not yet been reached, add another empty fieldset
        if (selectedTimeRanges.length < maxFieldSetAmount) {
            html += generateFieldset(count);
        }

        $("#form-container").html(html);
    };

    /** Builds a single field set for the rendering of the form */
    var generateFieldset = function generateFieldset(index) {
        var timeRange = arguments.length <= 1 || arguments[1] === undefined ? [] : arguments[1];

        var hourRange = [].concat(_toConsumableArray(new Array(24).keys()));
        var removeBtnTemplate = '<a class="action action--delete">X</a>';
        var optionsFrom = '<option>Van</option>',
            optionsTo = '<option>Tot</option>';

        hourRange.forEach(function (n) {
            optionsFrom += '<option value="' + n + '" ' + (n == timeRange.from ? 'selected' : '') + '> ' + n + ':00 </option>';
            optionsTo += '<option value="' + (n + 1) + '" ' + (n + 1 == timeRange.to ? 'selected' : '') + '> ' + (n + 1) + ':00 </option>';
        });

        var errorMessage = '';
        if (errorBag[index]) {
            errorMessage = '<span class="form-error is-visible"><strong>' + errorBag[index] + '</strong></span>';
        }

        return '\n            <fieldset data-index="' + index + '">\n                <div>\n                    <select>' + optionsFrom + '</select>\n                    <select>' + optionsTo + '</select>\n                    ' + (timeRange.length === 0 ? '' : removeBtnTemplate) + '\n              </div>\n                ' + errorMessage + '\n            </fieldset>';
    };

    /** Select a day or week */
    $(gridItemSelector).on('click', function (event) {
        $(gridItemSelector).removeClass(selectedClassName);
        $(this).addClass(selectedClassName);
        // Clear the errorBag on selecting a new item.
        errorBag = [];

        if ($(this).hasClass(weekClassName)) {
            $(this).siblings(gridItemSelector).addClass(selectedClassName);
            selectedDate = [];
            $(this).siblings(gridItemSelector).each(function () {
                selectedDate.push($(this).data('date'));
            });
            selectedTimeRanges = availability.getTimeRangesForDate(selectedDate);
        } else {
            selectedDate = $(this).data('date');
            selectedTimeRanges = availability.getTimeRangesForDate(selectedDate);
        }

        showFormUnderElement($(this));
    });

    /** Delete one of the time range fieldsets */
    form.on('click', '.action--delete', function (event) {
        var index = $(this).closest('fieldset').data('index');
        errorBag = [];

        // Remove the item from the temporary array and remove the fieldset
        selectedTimeRanges.splice(index, 1);
        $(this).closest('fieldset').remove();

        // Reset the index and re-render the form
        selectedTimeRanges.filter(function () {
            return true;
        });
        availability.updateAvailability(selectedDate, selectedTimeRanges, function () {
            return updateCalendar();
        }).success(function () {
            updateCalendar();
        });

        renderFieldSets();

        // No clue why this stops the form from auto closing after removing the fieldset
        return false;
    });

    /** Check if a new fieldset needs to be added after selecting a value */
    form.on('change', 'select', function (event) {
        var index = $(this).closest('fieldset').data('index');
        var from = $('fieldset:nth-child(' + (index + 1) + ') select:first-child').val();
        var to = $('fieldset:nth-child(' + (index + 1) + ') select:last').val();
        errorBag = []; // Clean any errors

        // Check if either value is still the default placeholder and the max amount of fieldsets are not yet reached.
        if (!$.isNumeric(from) || !$.isNumeric(to)) {
            return;
        }

        selectedTimeRanges[index] = { from: from, to: to };
        availability.updateAvailability(selectedDate, selectedTimeRanges).done(function () {
            updateCalendar();
        }).fail(function (result) {
            _.each(result.responseJSON, function (errors, key) {
                var index = key.split(".")[1];
                errorBag[index] = errors[0];
            });
        }).always(function () {
            return renderFieldSets();
        });
    });

    /** Deselect on an unrelevant click */
    $(document).on('click', function (event) {
        if ($(event.target).closest(gridItemSelector).length || $(event.target).closest('.availability-calendar__form').length) {
            return;
        }

        $(gridItemSelector).removeClass(selectedClassName);
        selectedDate = null;

        hideForm();
    });

    // Get the availability for the current calendar month
    availability.fetchAvailabilityBetweenDates($("[data-date]").first().data('date'), $("[data-date]").last().data('date'), function () {
        return updateCalendar();
    });
})(jQuery);

},{"./Availability":1}]},{},[2]);

//# sourceMappingURL=availability-calendar.js.map
