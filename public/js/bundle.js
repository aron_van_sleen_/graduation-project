(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Availability = function () {
    /** Creates a new avaialbility collection between dates */

    function Availability(startDate, endDate, callback) {
        var _this = this;

        _classCallCheck(this, Availability);

        $.ajax({
            url: '/availability',
            data: {
                from: startDate,
                to: endDate
            }
        }).done(function (results) {
            // Map the object to an array
            _this.availability = [];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = Object.keys(results)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var date = _step.value;

                    _this.availability[date] = results[date];
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            if (callback) {
                callback(_this.availability);
            }
        });
    }

    /** Set the availability for a date */


    _createClass(Availability, [{
        key: 'setAvailability',
        value: function setAvailability(date, timeRanges, callback) {
            var _this2 = this;

            // Todo insert more checks?
            $.ajax({}).done(function (result) {
                _this2.availability[date] = timeRanges;

                if (callback) {
                    callback(date);
                }
            });
        }
    }]);

    return Availability;
}();

exports.default = Availability;

},{}],2:[function(require,module,exports){
'use strict';

var _Availability = require('./Availability');

var _Availability2 = _interopRequireDefault(_Availability);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

$(function ($) {
    var gridItemSelector = '.availability-calendar__grid-item:not(.availability-calendar__grid-item--unavailable)';
    var selectedClassName = 'is-selected';
    var availableClassName = 'is-available';
    var selectedDate = void 0;
    var availability = void 0;

    availability = new _Availability2.default($('[data-date]').first().data('date'), $('[data-date]').last().data('date'), function () {
        return updateCalendar();
    });

    // Update the calendar to show the available days
    var updateCalendar = function updateCalendar() {
        // console.log(availability);
        $.each(availability, function (date, timeRanges) {
            $('[data-date=' + date + ']').addClass(availableClassName);
        });
    };

    // Select a day or week
    $(gridItemSelector).on('click', function () {
        $(gridItemSelector).removeClass(selectedClassName);
        $(this).addClass(selectedClassName);
        selectedDate = $(this).data('date');
        availabilityForm.show(selectedDate);
    });

    // Unselect all on a non-relevant click
    $(document).on('click', function (e) {
        if ($(e.target).closest(gridItemSelector).length || $(e.target).closest('.availability-calendar__form').length) {
            return;
        }
        $(gridItemSelector).removeClass(selectedClassName);
        selectedDate = null;
        availabilityForm.hide();
    });
}(jQuery));

},{"./Availability":1}]},{},[2,1]);

//# sourceMappingURL=bundle.js.map
