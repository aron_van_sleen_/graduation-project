(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function ($) {

    $(".request-card__action").on('click', function (event) {
        event.preventDefault();

        var id = $(this).closest('.request').data('id');
        var type = $(this).closest('.request').data('type');

        if ($(this).hasClass('accept')) {
            var date = null;
            if (type === 'interview') {
                // Only needed for accepting interview requests, not the hiring ones.
                date = $(this).closest('.request-card__interview-dates__date').data('date');
            }
            acceptRequest(type, id, date);
        } else if ($(this).hasClass('decline')) {
            declineRequest(type, id);
        }
    });

    var acceptRequest = function acceptRequest(type, id, date) {
        $.ajax({
            type: 'POST',
            url: '/' + type + '-requests/' + id + '/accept',
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
            },
            data: {
                date: date
            }
        }).success(function (result) {
            flashy(result.message, 'success');
            removeRequestElementById(id);
        });
    };

    var declineRequest = function declineRequest(type, id) {
        $.ajax({
            type: 'DELETE',
            url: '/' + type + '-requests/' + id + '/decline',
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
            }
        }).success(function (result) {
            flashy(result.message, 'success');
            removeRequestElementById(id);
        });
    };

    var removeRequestElementById = function removeRequestElementById(id) {
        $('[data-id="' + id + '"]').remove();
        refreshList();
    };

    var refreshList = function refreshList() {
        if ($('.interview-requests__request').length !== 0) {
            return;
        }
    };
})(jQuery);

},{}]},{},[1]);

//# sourceMappingURL=careprovider-requests.js.map
