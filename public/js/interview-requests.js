(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function ($) {

    $(".interview-requests__request__action").on('click', function () {
        var id = $(this).closest('.interview-requests__request').data('id');

        if ($(this).hasClass('accept-interview')) {
            var date = $(this).closest('.interview-requests__request__date').data('date');

            acceptInterviewRequest(id, date);
        } else if ($(this).hasClass('decline-interview')) {
            declineInterviewRequest(id);
        }
    });

    var acceptInterviewRequest = function acceptInterviewRequest(id, date) {
        $.ajax({
            type: 'POST',
            url: '/interview-requests/' + id + '/accept',
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
            },
            data: {
                date: date
            }
        }).success(function (result) {
            flashy(result.message, 'success');
            removeRequestElementById(id);
        });
    };

    var declineInterviewRequest = function declineInterviewRequest(id) {
        $.ajax({
            type: 'DELETE',
            url: '/interview-requests/' + id + '/decline',
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
            }
        }).success(function (result) {
            flashy(result.message, 'success');
            removeRequestElementById(id);
        });
    };

    var removeRequestElementById = function removeRequestElementById(id) {
        $('[data-id="' + id + '"]').remove();
        refreshList();
    };

    var refreshList = function refreshList() {
        if ($('.interview-requests__request').length !== 0) {
            return;
        }

        $('.interview-requests ul').append('<li class="interview-requests__request"><span>Momenteel zijn er nog geen verzoeken.</span></li>');
    };
})(jQuery);

},{}]},{},[1]);

//# sourceMappingURL=interview-requests.js.map
