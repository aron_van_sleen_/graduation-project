(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

$(document).ready(function ($) {

    $('.role-selection__role a').click(function (event) {
        event.preventDefault();
        // Remove the old selection
        $('.role-selection__role--is-active').removeClass('role-selection__role--is-active');

        $(this).closest('li').addClass('role-selection__role--is-active');

        // Set the newly selected role
        $('#selected_role').val($(this).data('role'));
    });
}(jQuery));

},{}]},{},[1]);

//# sourceMappingURL=registration.js.map
