(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function ($) {
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    };

    var hourlyRateSlider = $('[data-slider]');

    hourlyRateSlider.on('moved.zf.slider', function (event) {
        var amount = $('input[name="hourlyRate"]').val();
        $("#hourlyRateOutput").text('€ ' + (amount / 100).toFixed(2));
    });

    // hourlyRateSlider.on( 'changed.zf.slider', function ( event ) {
    //     let data = $( '#search-form' ).serialize();
    //
    //     console.log( data );
    //     $.ajax( {
    //         method: 'post',
    //         url: '/zoeken',
    //         headers: {
    //             'X-CSRF-TOKEN': $( 'input[name="_token"]' ).attr( 'value' )
    //         },
    //         data: data
    //     } ).success( function ( results ) {
    //         updateResultSet( results );
    //     } );
    // } );
    //
    // const updateResultSet = function ( results ) {
    //     $( "#search__results" ).html( results );
    // }
})(jQuery);

},{}]},{},[1]);

//# sourceMappingURL=search-results.js.map
