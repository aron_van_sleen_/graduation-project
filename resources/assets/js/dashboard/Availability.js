export default class Availability {

    /** Creates a new avaialbility collection for data management */
    constructor( availability = [] ) {
        this.availability = availability;
    }

    /** Fetches the availability of the current user from the server */
    fetchAvailabilityBetweenDates( startDate, endDate, callback ) {
        $.ajax( {
            url: '/availability',
            data: {
                from: startDate, to: endDate
            }
        } ).done( results => {
            // Map the object to an array
            this.availability = [];
            for ( let date of Object.keys( results ) ) {
                this.availability[ date ] = results[ date ];
            }

            if ( callback ) {
                callback( this.availability );
            }
        } );
    }

    /** Set the availability for a date */
    updateAvailability( dates, timeRanges ) {
        // Return this as a promise, so the calendar can react to the response
        if ( ! (dates instanceof Array) ) {
            dates = [ dates ];
        }

        return $.ajax( {
            url: '/availability',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $( 'input[name="_token"]' ).attr( 'value' )
            }, data: {
                dates: dates,
                timeRanges: timeRanges
            }
        } ).done( () => {
            // Sync the availability array to match the result
            if ( timeRanges.length === 0 ) {
                for ( let date of dates ) {
                    delete this.availability[ date ];
                }
            } else {
                for ( let date of dates ) {

                    this.availability[ date ] = timeRanges;
                }
            }
        } );
    }

    /** Returns an array with timeRanges for it's date */
    getTimeRangesForDate( dates ) {
        if ( dates instanceof Array ) {
            return []; // TODO: Timeranges on week select
        }
        else if ( this.availability.hasOwnProperty( dates ) ) {
            // Need to copy the array, otherwise it's passed as reference and will be altered 
            return JSON.parse( JSON.stringify( this.availability[ dates ] ) );
        }

        return [];
    }
}


