import Availability from './Availability';

(function ( $ ) {
    const maxFieldSetAmount = 3;
    const gridItemSelector = '.availability-calendar__item:not(.is-unavailable)';
    const weekClassName = 'availability-calendar__item--week';
    const selectedClassName = 'is-selected';
    const availableClassName = 'is-available';
    const formShownClassName = 'is-shown';
    const form = $( '.availability-calendar__form' );
    const formPointer = $( '.availability-calendar__form__pointer' );

    // Temporary data for the UI to use before persisting it.
    let selectedDate;
    let selectedTimeRanges = [];
    let errorBag = [];

    let availability = new Availability();

    // Update the calendar to show the available days
    let updateCalendar = function () {
        // Remove all, so outdated classes are removed
        $( gridItemSelector ).removeClass( availableClassName );

        for ( let date in availability.availability ) {
            // Apply classes to the relevant dates
            $( `[data-date=${date}]` ).addClass( availableClassName );
        }
    };

    /** Positions the forms and renders it under the given element */
    let showFormUnderElement = function ( element ) {
        form.addClass( formShownClassName );
        renderFieldSets();

        let position = element.position();
        let width = element.outerWidth();
        let height = element.outerHeight();
        let formWidth = form.outerWidth();
        let windowWidth = $( window ).width();
        let formLeftPosition = position.left;

        if ( windowWidth <= formWidth ) {
            // Make it full screen
            formLeftPosition = 0;
        } else if ( position.left + formWidth + width >= windowWidth ) {
            // Make sure form doesn't go out of the edges
            formLeftPosition = (position.left - formWidth + width);
        }

        form.css( {
            top: position.top + height, left: formLeftPosition
        } );
        // Position the pointer to be centered with the button
        formPointer.css( {
            left: position.left - form.position().left + (width / 2) - (formPointer.outerWidth() / 2)
        } );
    };

    let hideForm = function () {
        form.removeClass( formShownClassName );
    };

    /** Build up the fieldsets for the form depending on selectedTimeRanges */
    let renderFieldSets = function () {
        let html = ``;
        let count = 0;

        selectedTimeRanges.forEach( timeRange => {
            html += generateFieldset( count, timeRange );
            count ++;
        } );

        // If the max has not yet been reached, add another empty fieldset
        if ( selectedTimeRanges.length < maxFieldSetAmount ) {
            html += generateFieldset( count );
        }

        $( "#form-container" ).html( html );
    };

    /** Builds a single field set for the rendering of the form */
    let generateFieldset = function ( index, timeRange = [] ) {
        const hourRange = [ ...new Array( 24 ).keys() ];
        const removeBtnTemplate = `<a class="action action--delete">X</a>`;
        let optionsFrom = `<option>Van</option>`, optionsTo = `<option>Tot</option>`;

        hourRange.forEach( n => {
            optionsFrom += `<option value="${n}" ${n == timeRange.from ? 'selected' : ''}> ${n}:00 </option>`;
            optionsTo += `<option value="${n + 1}" ${n + 1 == timeRange.to ? 'selected' : ''}> ${n + 1}:00 </option>`;
        } );

        let errorMessage = ``;
        if ( errorBag[ index ] ) {
            errorMessage = `<span class="form-error is-visible"><strong>${errorBag[ index ]}</strong></span>`;
        }

        return `
            <fieldset data-index="${index}">
                <div>
                    <select>${optionsFrom}</select>
                    <select>${optionsTo}</select>
                    ${timeRange.length === 0 ? '' : removeBtnTemplate }
              </div>
                ${errorMessage}
            </fieldset>`;
    };

    /** Select a day or week */
    $( gridItemSelector ).on( 'click', function ( event ) {
        $( gridItemSelector ).removeClass( selectedClassName );
        $( this ).addClass( selectedClassName );
        // Clear the errorBag on selecting a new item.
        errorBag = [];

        if ( $( this ).hasClass( weekClassName ) ) {
            $( this ).siblings( gridItemSelector ).addClass( selectedClassName );
            selectedDate = [];
            $( this ).siblings( gridItemSelector ).each( function () {
                selectedDate.push( $( this ).data( 'date' ) );
            } );
            selectedTimeRanges = availability.getTimeRangesForDate( selectedDate );

        } else {
            selectedDate = $( this ).data( 'date' );
            selectedTimeRanges = availability.getTimeRangesForDate( selectedDate );
        }

        showFormUnderElement( $( this ) );
    } );

    /** Delete one of the time range fieldsets */
    form.on( 'click', '.action--delete', function ( event ) {
        const index = $( this ).closest( 'fieldset' ).data( 'index' );
        errorBag = [];


        // Remove the item from the temporary array and remove the fieldset
        selectedTimeRanges.splice( index, 1 );
        $( this ).closest( 'fieldset' ).remove();

        // Reset the index and re-render the form
        selectedTimeRanges.filter( () => true );
        availability.updateAvailability( selectedDate, selectedTimeRanges, () => updateCalendar() )
            .success( () => {
                updateCalendar();
            } );

        renderFieldSets();

        // No clue why this stops the form from auto closing after removing the fieldset
        return false;
    } );

    /** Check if a new fieldset needs to be added after selecting a value */
    form.on( 'change', 'select', function ( event ) {
        const index = $( this ).closest( 'fieldset' ).data( 'index' );
        const from = $( `fieldset:nth-child(${index + 1}) select:first-child` ).val();
        const to = $( `fieldset:nth-child(${index + 1}) select:last` ).val();
        errorBag = []; // Clean any errors

        // Check if either value is still the default placeholder and the max amount of fieldsets are not yet reached.
        if ( ! $.isNumeric( from ) || ! $.isNumeric( to ) ) {
            return;
        }

        selectedTimeRanges[ index ] = { from, to };
        availability.updateAvailability( selectedDate, selectedTimeRanges )
            .done( () => {
                updateCalendar();
            } )
            .fail( result => {
                _.each( result.responseJSON, ( errors, key ) => {
                    const index = key.split( "." )[ 1 ];
                    errorBag[ index ] = errors[ 0 ];
                } );
            } )
            .always( () => renderFieldSets() );
    } );

    /** Deselect on an unrelevant click */
    $( document ).on( 'click', function ( event ) {
        if ( $( event.target ).closest( gridItemSelector ).length || $( event.target ).closest( '.availability-calendar__form' ).length ) {
            return;
        }

        $( gridItemSelector ).removeClass( selectedClassName );
        selectedDate = null;

        hideForm();
    } );

    // Get the availability for the current calendar month
    availability.fetchAvailabilityBetweenDates( $( "[data-date]" ).first().data( 'date' ), $( "[data-date]" ).last().data( 'date' ), () => updateCalendar() );
}( jQuery ));