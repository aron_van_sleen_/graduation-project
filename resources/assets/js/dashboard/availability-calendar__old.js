$(function ($) {
    var availabilityCalendar = (function () {
        var calendarDateItems = $('.availability-calendar__grid-item' +
            ':not(.availability-calendar__grid-item--unavailable)' +
            ':not(.availability-calendar__grid-item--week'
        );
        var calendarWeekItems = $('.availability-calendar__grid-item--week:not(.availability-calendar__grid-item--unavailable)');
        var calendarForm = $('.availability-calendar__form');
        var selectedItemClassName = 'availability-calendar__grid-item--selected';
        var availableClassName = 'availability-calendar__grid-item--available';
        var availability = [];
        var selectedDates = null;
        var selectedElements = null;

        var init = function () {
            var from = calendarDateItems.first().data('date');
            var to = calendarDateItems.last().data('date');

            $.ajax({
                url: '/availability',
                data: {
                    from: from,
                    to: to
                }
            }).done(function (data) {
                availability = data;
                renderCalendar();
            });
        };

        var renderCalendar = function () {
            $.each(availability, function (date, timeRanges) {
                $('.availability-calendar__grid').find('[data-date=' + date + ']').addClass(availableClassName);
            });
        };

        var renderForm = function () {
            var timeRanges = availability[selectedDates];
            var html = "";

            if (timeRanges != undefined && timeRanges.length > 0) {
                // Render a fieldset for each previously selected timeRange
                $.each(timeRanges, function (key, timeRange) {
                    html += createAvailabilityFieldset(key, timeRange);
                });
            } else {
                html += createAvailabilityFieldset(0);
            }

            $('#form-container').html(html);
        };

        var createAvailabilityFieldset = function (index, timeRange) {
            var newFieldset = $('<fieldset/>');
            var selectFrom = $("<select name='from["+index+"]'><option>Van</option></select>");
            var selectTo = $("<select name='to[" +index+"]'><option>Tot</option></select>");

            for (var from = 0; from < 24; from ++) {
                var to = from + 1;
                var fromSelected = (timeRange != undefined && from == timeRange.from);
                var toSelected = (timeRange != undefined && to == timeRange.to);

                $('<option>', { value: from, selected: fromSelected }).html(from + ":00").appendTo(selectFrom);
                $('<option>', { value: to, selected: toSelected }).html(to + ":00").appendTo(selectTo)
            }
            selectFrom.appendTo(newFieldset);
            selectTo.appendTo(newFieldset);

            if (timeRange != undefined) {
               $("<a>", {class: 'action action--delete'}).html("X").appendTo(newFieldset);
            } else if (index < 2) {
               $("<a>", {class: 'action action--add' }).html("+").appendTo(newFieldset);
            }

            return newFieldset.prop('outerHTML');
        };

        var deselectGridItems = function () {
            calendarDateItems.removeClass(selectedItemClassName);
            calendarWeekItems.removeClass(selectedItemClassName);
            calendarForm.hide();
        };

        var showForm = function (element) {
            var position = element.position();
            var width = element.outerWidth();
            var height = element.outerHeight();
            var formWidth = calendarForm.outerWidth();
            var windowWidth = $(window).width();
            var pointer = $('.availability-calendar__form-pointer');

            renderForm();
            calendarForm.show();

           
        };

        calendarDateItems.on('click', function (event) {
            deselectGridItems();
            selectedDates = $(this).data('date');
            selectedElements = $(this);
            $(this).addClass(selectedItemClassName);
            showForm($(this));
        });

        calendarWeekItems.on('click', function (event) {
            var selectedRow = $(this).siblings(':not(.availability-calendar__grid-item--unavailable)');
            selectedElements = selectedRow;
            deselectGridItems();

            selectedDates = [];
            selectedRow.each(function () {
                selectedDates.push($(this).data('date'));
            });

            selectedRow.addClass(selectedItemClassName);
            $(this).addClass(selectedItemClassName);
            showForm($(this));
        });

        $('#availability-form').on('submit', function(event) {
            event.preventDefault();
            var input = $(this).serializeArray();
            $.ajax({
                method: 'post',
                url: '/dashboard/beschikbaarheid',
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                },
                data: {
                    timeRanges: input,
                    dates: selectedDates,
                }
            }).done(function (data) {
                console.log(data);
                selectedElements.addClass(availableClassName)
            });
        });

        // Deselect if click isn't on one of the elements
        $(document).on('click', function (e) {
            if ($(e.target).closest('.availability-calendar__grid-item').length || $(e.target).closest('.availability-calendar__form').length) {
                return;
            }
            deselectGridItems();
        });

        init();
    })();
}(jQuery));