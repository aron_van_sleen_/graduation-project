(function ( $ ) {

    $( ".request-card__action" ).on( 'click', function (event) {
        event.preventDefault();

        let id = $( this ).closest( '.request' ).data( 'id' );
        let type = $( this ).closest( '.request' ).data( 'type' );

        if ( $( this ).hasClass( 'accept' ) ) {
            let date = null;
            if ( type === 'interview' ) {
                // Only needed for accepting interview requests, not the hiring ones.
                date = $( this ).closest( '.request-card__interview-dates__date' ).data( 'date' );
            }
            acceptRequest( type, id, date )
        }
        else if ( $( this ).hasClass( 'decline' ) ) {
            declineRequest( type, id );
        }
    } );

    let acceptRequest = function ( type, id, date ) {
        $.ajax( {
            type: 'POST',
            url: `/${type}-requests/${id}/accept`,
            headers: {
                'X-CSRF-TOKEN': $( 'input[name="_token"]' ).attr( 'value' )
            },
            data: {
                date: date
            }
        } ).success( function ( result ) {
            flashy( result.message, 'success' );
            removeRequestElementById( id );
        } );
    };

    let declineRequest = function ( type, id ) {
        $.ajax( {
            type: 'DELETE',
            url: `/${type}-requests/${id}/decline`,
            headers: {
                'X-CSRF-TOKEN': $( 'input[name="_token"]' ).attr( 'value' )
            }
        } ).success( function ( result ) {
            flashy( result.message, 'success' );
            removeRequestElementById( id );
        } );
    };

    let removeRequestElementById = function ( id ) {
        $( `[data-id="${id}"]` ).remove();
        refreshList();
    };

    let refreshList = function () {
        if ( $( '.interview-requests__request' ).length !== 0 ) {
            return;
        }
    };
}( jQuery ));
