(function ( $ ) {

    $('[data-slider]').on('moved.zf.slider', function (event) {
        var amount = $('input[name="hourlyRate"]').val();
        $("#hourlyRateOutput").text(`€ ${ (amount / 100).toFixed(2) }`);
    });



}( jQuery ));