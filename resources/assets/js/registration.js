$(document).ready(function($) {
    
    $('.role-selection__role a').click(function (event) {
        event.preventDefault();
        // Remove the old selection
        $('.role-selection__role--is-active').removeClass('role-selection__role--is-active');

        $(this).closest('li').addClass('role-selection__role--is-active');

        // Set the newly selected role
        $('#selected_role').val($(this).data('role'));
    });
} (jQuery));