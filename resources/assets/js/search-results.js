(function ( $ ) {
    $.urlParam = function ( name ) {
        var results = new RegExp( '[\?&]' + name + '=([^&#]*)' ).exec( window.location.href );
        return results[ 1 ] || 0;
    };


    const hourlyRateSlider = $( '[data-slider]' );

    hourlyRateSlider.on( 'moved.zf.slider', function ( event ) {
        var amount = $( 'input[name="hourlyRate"]' ).val();
        $( "#hourlyRateOutput" ).text( `€ ${ (amount / 100).toFixed( 2 ) }` );
    } );

    // hourlyRateSlider.on( 'changed.zf.slider', function ( event ) {
    //     let data = $( '#search-form' ).serialize();
    //
    //     console.log( data );
    //     $.ajax( {
    //         method: 'post',
    //         url: '/zoeken',
    //         headers: {
    //             'X-CSRF-TOKEN': $( 'input[name="_token"]' ).attr( 'value' )
    //         },
    //         data: data
    //     } ).success( function ( results ) {
    //         updateResultSet( results );
    //     } );
    // } );
    //
    // const updateResultSet = function ( results ) {
    //     $( "#search__results" ).html( results );
    // }

}( jQuery ));