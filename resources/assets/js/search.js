( function ( $ ) {
    var datepicker = $( "#datepicker" );
    var date = datepicker.val();

    // Set default date if no value has been set yet
    if ( ! date ) {
        date = new Date();
    }

    datepicker.fdatepicker( {
        initialDate: date,
        format: 'dd-mm-yyyy',
        language: 'nl',
        disableDblClickSelection: true,
        onRender: function ( date ) {
            return date.valueOf() < new Date().valueOf() ? 'disabled' : ''
        }
    } );
}( jQuery ));