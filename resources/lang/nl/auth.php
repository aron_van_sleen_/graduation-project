<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Het opgegeven e-mailadres en wachtwoord komen niet overeen.',
    'throttle' => 'U heeft te vaak proberen in te loggen. Probeer het nog eens in :seconds seconden.',

];
