<?php

return [
    'bronze' => [
        'title' => 'Brons'
    ],
    'silver' => [
        'title' => 'Silver'
    ],
    'gold' => [
        'title' => 'Goud'
    ],
    'platinum' => [
        'title' => 'Platinum'
    ],
];
