<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'Voer een geldige :attribute in.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'Het bestand moet één van de volgende typen zijn: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'Een :attribute moet tenminste uit :min tekens bestaan.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'De waarde :attribute is niet geldig.',
    'numeric'              => 'Het :attribute veld moet een nummer zijn.',
    'present'              => 'Het :attribute veld moet aanwezig zijn.',
    'regex'                => 'Het :attribute formaat is niet correct.',
    'required'             => 'Een :attribute is verplicht.',
    'required_if'          => 'Het :attribute veld is verplicht indien :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'Het :attribute veld is niet ingevuld.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'Het :attribute komt niet met overeen met het ingevulde :other.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'Het :attribute moet minimaal :size karakters lang zijn.',
        'array'   => 'Het :attribute moet :size items bevatten.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'Dit :attribute is al in gebruik.',
    'url'                  => 'The :attribute format is invalid.',

    'postal_code' => [
        'invalid_format' => 'Dit is geen correcte :attribute.',
    ],

    'password' => [
        'invalid_format' => 'Het :attribute voldoet niet aan de gestelde eisen.'
    ],

    'phone_number' => [
        'invalid_format' => 'Het :attribute bestaat uit een onjuist formaat.'
    ],

    'too_young' => 'Je moet minimaal 16 jaar zijn om te registeren.',
    'too_old'   => 'Je mag niet ouder zijn dan ' . config('acme.max_age') . ' jaar zijn om te registeren.',

    'valid_time_range'                => 'Een begin tijd kan niet hetzelfde of later zijn als een eindtijd.',
    'overlaps_with_other_time_ranges' => 'Deze tijden overlappen met elkaar, je kan geen overlappende tijden op dezelfe dag aangeven.',

    'hourly_rate' => [
        'over_max_amount' => 'Je kan niet meer verdienen dan je huidige niveau toestaat.'
    ],

    'date_future'  => 'Deze datum kan niet in het verleden zijn.',
    'date_in_past' => 'Je kan geen data in het verleden aangeven.',
    'not_a_date'   => 'Kies een geldige datum.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention 'attribute.rule' to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of 'email'. This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'first_name'        => 'voornaam',
        'last_name'         => 'achternaam',
        'street_name'       => 'straatnaam',
        'street_number'     => 'huisnummer',
        'postal_code'       => 'postcode',
        'postal_digits'     => 'cijfers van een postcode',
        'postal_characters' => 'tekens van een postcode',
        'city'              => 'plaats',
        'email'             => 'e-mailadres',
        'phone_number'      => 'telefoonnummer',
        'password'          => 'wachtwoord',
        'password_repeat'   => 'herhaalde wachtwoord',
        'date_of_birth'     => 'geboortedatum',
        'hourly_rate'       => 'prijs per uur',
        'date'              => 'datum',
        'from'              => 'tijd',
        'to'                => 'tijd',
        'location'          => 'locatie',
        'radius'            => 'straal',
        'profile_image'     => 'profiel afbeelding'
    ],

];
