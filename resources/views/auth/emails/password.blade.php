Click here to reset your password: <a href="{{ $link = route('auth.password.reset-form', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
