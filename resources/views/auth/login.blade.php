@extends('layouts.default')

@section('meta-title')
    Inloggen
@endsection

@section('main')

    <main class="form row">

        {{ Form::open(array('route' => array("auth.login"))) }}


        <header class="form__header block">
            <div class="small-12 columns">

                <h1 class="form__title">Log in op Acme</h1>

            </div>
        </header>

        <div class="form__body block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.email', [
                    'label' => 'E-mailadres',
                    'field' => 'email',
                    'options' => [
                        'required' => 'required'
                     ],
                     'class' => ($errors->has('auth')) ? 'is-invalid-input' : ''
                ])

                @include('partials.form.fields.password', [
                        'label' => 'Wachtwoord',
                        'field' => 'password',
                        'options' => [
                            'required' => 'required'
                         ],
                         'class' => ($errors->has('auth')) ? 'is-invalid-input' : ''
                    ])

            </div>
            @if (isset($errors) && $errors->has('auth'))

                <div class="small-12 columns">
                    <span class="form-error is-visible">
                        <strong>{{ $errors->first('auth') }}</strong>
                    </span>
                </div>

            @endif
            <div class="small-12 columns">

                <a href="{{ route('auth.registration.role') }}">Nog geen account? Meld je aan.</a>
                <a href="{{ route('auth.password.email-form') }}">Wachtwoord vergeten?</a>

            </div>
        </div>

        <footer class="form__footer block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.submit', [
                    'label' => 'Inloggen',
                    'class' => 'button--depth'
                ])

            </div>
        </footer>

        {{ Form::close() }}

    </main>

@endsection
