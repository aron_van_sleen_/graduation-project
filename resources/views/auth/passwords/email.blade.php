@extends('layouts.default')

<!-- Main Content -->
@section('main')
    <section class="form row">
        {{ Form::open(['route' => 'auth.password.send-link']) }}

        <header class="form__header block block--narrow">
            <div class="small 12-columns">
                <h1 class="form__title">Wachtwoord vergeten</h1>
            </div>
        </header>

        <div class="form__body block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.email', [
                    'label' => 'E-mail adres',
                    'field' => 'email'
                ])

            </div>
        </div>

        <footer class="form_footer block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.submit', [
                    'label' => 'Stuur een e-mail',
                    'class' => 'button--depth'
                ])

            </div>
        </footer>

        {{ Form::close() }}
    </section>

@endsection
