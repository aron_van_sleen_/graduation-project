@extends('layouts.default')

@section('main')

    <section class="form row">
        {{ Form::open(['route' => 'auth.password.reset']) }}

        {{ Form::hidden('token', $token) }}
        {{ Form::hidden('email', $email) }}

        <header class="form__header block block--narrow">
            <div class="small-12 columns">
                <h1 class="form__title">Wachtwoord resetten</h1>
            </div>
        </header>

        <div class="form__body block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.password', [
                    'label' => 'Nieuwe wachtwoord',
                    'field' => 'password'
                ])

                @include('partials.form.fields.password', [
                    'label' => 'Wachtwoord herhalen',
                    'field' => 'password_confirmation'
                ])

            </div>
        </div>

        <footer class="form__footer block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.submit', [
                    'label' => 'Reset wachtwoord',
                    'class' => 'button--depth'
                ])

            </div>
        </footer>

        {{ Form::close() }}
    </section>

@endsection
