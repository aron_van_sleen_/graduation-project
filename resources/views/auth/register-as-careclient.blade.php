@extends('layouts.default')

@section('main')

    <main class="form row">

        {{ Form::open(array('route' => array("auth.register.careclient"))) }}

        <header class="form__header block block--narrow">
            <div class="small-12 columns">
                <h1 class="form__title">Aanmelden als ouder of verzorger</h1>
            </div>
        </header>

        <div class="form__body block block--narrow">
            <div class="small-12 columns">

                @include('auth.register-form')

            </div>
        </div>

        <footer class="form__footer block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.submit', ['label' => 'Aanmelden', 'class' => 'button--depth'])

            </div>
        </footer>

        {{ Form::close() }}

    </main>
@endsection