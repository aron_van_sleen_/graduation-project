<fieldset class="row">
    <legend class="small-12 columns">Vertel iets over jezelf</legend>
    <div class="small-12 medium-6 columns">
        @include('partials.form.fields.text', [
            'label' => 'Voornaam',
            'field' => 'first_name',
            'options' => [
                'required' => 'required'
             ]
        ])
    </div>

    <div class="small-12 medium-6 columns">
        @include('partials.form.fields.text', [
              'label' => 'Achternaam',
              'field' => 'last_name',
              'options' => [
                'required' => 'required'
              ]
          ])
    </div>

    <div class="small-12 columns">
        @include('partials.form.field-groups.date-of-birth', ['disclaimer' => 'Je dient minimaal 16 jaar te zijn.'])
    </div>

    <div class="small-12 columns">
        @include('partials.form.field-groups.address')
    </div>

</fieldset>

<fieldset class="row">
    <legend class="small-12 columns">Waar kunnen we je op bereiken?</legend>

    <div class="small-12 columns">
        @include('partials.form.fields.telephone', [
            'label' => 'Telefoonnummer',
            'field' => 'phone_number',
            'options' => [
                'required' => 'required'
             ]
        ])
    </div>

    <div class="small-12 columns">
        @include('partials.form.fields.email', [
            'label' => 'E-mailadres',
            'field' => 'email',
            'options' => [
                'required' => 'required'
             ]
        ])
    </div>

</fieldset>

<fieldset class="row">
    <legend class="small-12 columns">Laatste stap</legend>

    <div class="small-12 columns">
        @include('partials.form.fields.password', [

           'label' => 'Wachtwoord',
           'field' => 'password',
           'options' => [
                'required' => 'required'
             ]
        ])
    </div>
    <div class="form__disclaimer small-12 columns">
        <small>Een wachtwoord moet mininaal uit acht karakters bestaan en moet één hoofdletter, één
            cijfer en één speciaal teken bevatten.
        </small>
    </div>

    <div class="small-12 columns">
        @include('partials.form.fields.password', [

           'label' => 'Wachtwoord herhalen',
           'field' => 'password_repeat',
           'options' => [
                'required' => 'required'
             ]
        ])
    </div>

</fieldset>



