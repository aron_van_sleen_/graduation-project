@extends('layouts.default')

@section('main')

    <div class="row block">
        <div class="small-12 columns">

            <div class="role-selection">

                <article class="role-selection__role role-selection__role--careclient">

                    <header class="role-selection__role__header">
                        <h1>Ouder of verzorger</h1>
                    </header>

                    <div class="role-selection__role__body">
                        <p>Quisque efficitur id turpis a pellentesque. Donec gravida ante
                            tempor, vulputate diam id, ornare
                            ex. Duis nulla sapien, scelerisque eu molestie vel, lobortis sed mi. Suspendisse ac eleifend
                            neque. Maecenas blandit nisl condimentum tellus rhoncus lacinia. Proin tincidunt laoreet
                            metus,
                            eget placerat mauris tempus sed. Curabitur fringilla risus eu lacinia volutpat.</p>

                        <p>Fusce sodales
                            imperdiet ex, non aliquet risus malesuada tempor. In sit amet commodo dolor. Aenean dictum
                            erat
                            vel fermentum ornare. Curabitur odio sapien, imperdiet eget eleifend quis, eleifend nec
                            enim.
                            Aliquam erat volutpat. Integer tempor blandit pellentesque. Phasellus vitae egestas nisi.
                            Proin
                            auctor sapien dolor. Suspendisse a risus dui.</p>
                    </div>

                    <footer class="role-selection__role__footer">
                        <a href="{{ route('auth.registration.careclient') }}" class="button button--depth">
                            Registreer als ouder of verzorger
                        </a>
                    </footer>

                </article>

                <article class="role-selection__role role-selection__role--careprovider">

                    <header class="role-selection__role__header">
                        <h1>Oppasser</h1>
                    </header>

                    <div class="role-selection__role__body">
                        <p>Nulla convallis euismod ligula, sed consequat purus luctus
                            quis. Ut mattis a urna porta accumsan.
                            Pellentesque et justo eget metus porta mollis. Maecenas in mattis quam. Aliquam rutrum quam
                            eget
                            nibh pellentesque, varius faucibus nisi sollicitudin. Nulla vestibulum magna a lectus
                            viverra,
                            ut posuere ante consectetur.</p>

                        <p>Sed justo augue, tristique ac efficitur eget, condimentum id nisl.
                            Cras diam justo, fermentum eget nisi ut, fringilla ornare odio. Vestibulum sit amet mauris
                            non
                            magna maximus tempus at in eros. Vestibulum in vehicula sapien, in finibus lacus. Integer
                            mollis
                            nunc varius, feugiat mauris vitae, fringilla ipsum. Aenean nec tempor tellus, scelerisque
                            fermentum enim.</p>

                        <p>ulla convallis euismod ligula, sed consequat purus luctus
                            quis. Ut mattis a urna porta accumsan.</p>
                    </div>

                    <footer class="role-selection__role__footer">
                        <a href="{{ route('auth.registration.careprovider') }}" class="button button--depth">
                            Registreer als oppasser
                        </a>
                    </footer>
                </article>

            </div>

        </div>
    </div>

@endsection