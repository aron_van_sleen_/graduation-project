<p>Beste, {{ $careclientName }}</p>

<p>{{ $careproviderName }} heeft uw inhuur verzoek geaccepteerd.</p>
<p>De afspraak staat ingeplant op {{ strftime("%e %B", $date->timestamp) }} van {{ $date->format('H:i') }} tot {{ $to->format('H:i') }} ingepland.</p>
