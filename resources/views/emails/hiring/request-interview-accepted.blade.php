<p>Beste, {{ $careclientName }}</p>

<p>{{ $careproviderName }} heeft uw verzoek tot een interview geaccepteerd.</p>
<p>Het interview is op {{ strftime("%e %B om %k:%M", $date->timestamp) }} ingepland.</p>
