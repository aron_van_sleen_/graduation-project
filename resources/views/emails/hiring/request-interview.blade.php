<p>Beste, {{ $careproviderName }}</p>

<p>{{ $careclientName }} heeft u een verzoek tot een interview verzonden.</p>

<p><a href="{{ route('dashboard') }}">Ga naar uw dashboard om het verzoek te bekijken.</a></p>