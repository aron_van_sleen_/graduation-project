<footer class="footer">

    <nav class="footer__nav row block block--narrow">
        <div class="footer__nav-section small-12 medium-4  columns">

            <h2>Informatie</h2>
            <ul>
                <li><a>Klantenservice</a></li>
            </ul>

        </div>
        <div class="footer__nav-section small-12 medium-4 columns">

            <h2>Voor oppassers</h2>
            <ul>
                <li><a>Dashboard</a></li>
            </ul>

        </div>
        <div class="footer__nav-section small-12 medium-4 columns">

            <h2>Voor ouders</h2>
            <ul>
                <li><a>Dashboard</a></li>
            </ul>

        </div>
    </nav>

    <div class="footer__bottom">
        <div class="row block block--narrow">
            <div class="small-12 large-6 columns">

                <div class="footer__social-media">SOCIAL MEDIA PLACEHOLDER</div>

            </div>
            <div class="small-12 large-6 columns">

                <small class="footer__copyright">Copyright Saferia 2016 - Alle rechten voorbehouden</small>

            </div>

        </div>
    </div>
</footer>

<script src="{{ asset('/js/vendor.js') }}"></script>
@include('vendor.flashy.message')

@stack('scripts')

<script>
    $(document).foundation();
</script>


</body>