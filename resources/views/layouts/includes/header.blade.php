<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>

    <title>
        @if (Route::current()->getName() == 'home')
            Acme - We share your care
        @else
           {{ isset($metaTitle) ? $metaTitle : '' }} | Acme
        @endif
    </title>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">

    @yield('head')

</head>
<body>

@include('layouts.includes.top-header')