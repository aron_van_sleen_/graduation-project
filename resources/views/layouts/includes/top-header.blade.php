<header class="row-fluid">
    <nav class="top-bar">

        <ul class="top-bar-left vertical medium-horizontal menu">
            <li><a href="/">Acme</a></li>
        </ul>

        <ul class="top-bar-right vertical medium-horizontal menu">
            <li><a href="{{ route('search.results') }}">Vind een oppas</a></li>
            @if (Auth::check() == false)
                {{-- Guest routes --}}
                <li><a href="{{ route('auth.login') }}">Inloggen</a></li>
                <li><a href="{{ route('auth.registration.role') }}">Meld je aan</a></li>
            @else
                {{-- Member routes --}}
                <li><a href="{{ route('dashboard') }}">Mijn dashboard</a></li>
                <li><a href="{{ route('auth.logout') }}">Uitloggen</a></li>
            @endif
        </ul>

    </nav>
</header>
