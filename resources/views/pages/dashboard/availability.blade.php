@extends('pages.dashboard.index')

@section('dashboard-page')

    <section class="block availability-calendar row">
        <div class="small-12 columns">


            <h1>Beschikbaarheid aangeven</h1>

            <header class="availability-calendar__header">

                {{-- Previous month anchor --}}
                <div class="availability-calendar__button availability-calendar__button--prev">
                    @if ($currentDate->year > date("Y") ||
                        ($currentDate->month > date("m") && $currentDate->year == date("Y")))
                        <a href="{{ route("dashboard.availability", [
                        "year" => $currentDate->month == 1 ? $currentDate->year - 1 : $currentDate->year,
                        "month" => $currentDate->month == 1 ? 12 : $currentDate->month - 1
                    ]) }}">Vorige maand</a>
                    @endif
                </div>

                <h2 class="availability-calendar__header-title">{{ ucfirst(strftime("%B %Y", strtotime($currentDate))) }}</h2>

                {{-- Next month anchor --}}
                <div class="availability-calendar__button availability-calendar__button--next">
                    <a href="{{ route("dashboard.availability", [
                    "year" => $currentDate->month == 12 ? $currentDate->year + 1 : $currentDate->year,
                    "month" => $currentDate->month == 12 ? 1 : $currentDate->month + 1
                ]) }}">Volgende maand</a>
                </div>

            </header>

            {{-- Top row with week names --}}
            <div class="availability-calendar__grid">
                <ul>
                    <li class="availability-calendar__weekday">Week</li>
                    @foreach ($days->first() as $date => $datetime)

                        <li class="availability-calendar__weekday">
                            {{ ucfirst(strftime("%a", strtotime($date))) }}
                        </li>

                    @endforeach
                </ul>

                @foreach ($days as $week => $dates)
                    {{-- Print the week number --}}
                    <ul class="availability-calendar__week">

                        <li class="availability-calendar__item availability-calendar__item--week
                        {{ ($week < date("W") && $currentDate->year == date("Y")) ? 'is-unavailable' : '' }}"
                            data-week="{{ $week }}">

                            <a>{{ $week }}</a>
                        </li>

                        @foreach ($dates as $date => $datetime)
                            {{-- Print each day number --}}
                            <li class="availability-calendar__item
                            {{ ($date < date('Y-m-d')) ? 'is-unavailable' : '' }}
                            {{ ($date == date('Y-m-d')) ? 'availability-calendar__item--today' : '' }}"
                                data-date="{{ $date }}">

                                <a {{ ($date < date('Y-m-d')) ? 'tabindex=-1' : '' }}>
                                    <span class="availability-calendar__day">{{ $datetime->format('j') }}</span>
                                </a>
                            </li>

                        @endforeach
                    </ul>
                @endforeach

                <div class="availability-calendar__form">
                    <span class="availability-calendar__form__pointer"></span>

                    {{ Form::open(array('id' => 'availability-form')) }}
                    <div id="form-container"></div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </section>

@endsection

@push('scripts')
<script src="/js/availability-calendar.js"></script>
@endpush