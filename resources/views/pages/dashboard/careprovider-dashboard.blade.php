@extends('pages.dashboard.index')

@section('dashboard-page')
    <div class="row">

        <div class="small-12 medium-6 columns">

            @include('partials.dashboard.interview-requests', ['interviewRequests' => $model->interviewRequests])

        </div>

        <div class="small-12 medium-6 columns">

            @include('partials.dashboard.hiring-requests', ['hiringRequests' => $model->hiringRequests])

        </div>


        {{-- Needed for the Ajax calls to protect against CSRF --}}
        {{ Form::token() }}
    </div>
@endsection

@push('scripts')
<script src="/js/careprovider-requests.js"></script>
@endpush