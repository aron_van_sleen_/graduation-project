@extends('pages.dashboard.edit-profile')

@section('extension')

    <fieldset>
        <legend>Careclient gegevens</legend>

        <div class="small-12 columns">
            @include('partials.form.fields.textarea', [
                'label' => 'Omschrijving',
                'field' => 'description',
                'value' => $profile->description,
                'placeholder' => 'Vertel iets over jezelf',
                'options' => [
                    'size' => '30x3'
                ]
            ])
        </div>

        <div class="small-12 columns">
            @include('partials.form.fields.textarea', [
                'label' => 'Mijn sterke punten',
                'field' => 'qualities',
                'value' => $profile->qualities,
                'placeholder' => 'Wat zijn je sterke punten?',
                'options' => [
                    'size' => '30x3'
                ]
            ])
        </div>

        <div class="small-12 columns">
            @include('partials.form.field-groups.checkbox-group', [
                'label' => 'Mijn eigenschappen',
                'field' => 'characteristics',
                'items' => $characteristics,
                'value' => $profile->characteristics
            ])
        </div>

    </fieldset>

@endsection


