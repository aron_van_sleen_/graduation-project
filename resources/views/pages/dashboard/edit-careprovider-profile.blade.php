@extends('pages.dashboard.edit-profile')

@section('extension')

    <fieldset>
        <legend>Oppasser gegevens</legend>

        <div class="small-12 columns">
            @include('partials.form.fields.hourly-rate', [
                'hourlyRate' => $profile->hourlyRate,
                'maximumHourlyRate' => $profile->maximumHourlyRate
            ])
        </div>

        <div class="small-12 columns">
            @include('partials.form.fields.textarea', [
                'label' => 'Omschrijving',
                'field' => 'description',
                'value' => $profile->description,
                'placeholder' => 'Vertel iets over jezelf',
                'options' => [
                    'size' => '30x3'
                ]
            ])
        </div>

        <div class="small-12 columns">
            @include('partials.form.fields.textarea', [
                'label' => 'Sterke punten',
                'field' => 'qualities',
                'value' => $profile->qualities,
                'placeholder' => 'Wat zijn jouw sterke punten?',
                'options' => [
                    'size' => '30x3'
                ]
            ])
        </div>

        <div class="small-12 columns">
            @include('partials.form.field-groups.checkbox-group', [
                'label' => 'Mijn eigenschappen',
                'field' => 'characteristics',
                'items' => $characteristics,
                'value' => $profile->characteristics
            ])
        </div>
    </fieldset>

@endsection


