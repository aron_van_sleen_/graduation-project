@extends('pages.dashboard.index')

@section('dashboard-page')

    <div class="update-profile row">
        {{ Form::open(array('route' => "dashboard.edit-{$type}-profile", 'files' => true)) }}

        <div class="small-12 medium-4 columns">
            <div class="small-12 columns">
                <div class="row">
                    <div class="small-12 columns">

                        <figure class="update-profile__profile-image">
                        <img class="profile-image"
                             src="{{  $profile->profileImage === null ? '/storage/placeholder.png' : asset('storage/' . $profile->profileImage) }}"/>

                        @if ($profile->profileImage !== null)
                            <a class="update-profile__profile-image__remove-button" href="{{ route('dashboard.remove-profile-image') }}">
                                <i class="fa fa-close"></i>
                            </a>
                        @endif
                        </figure>

                    </div>
                    <div class="small-12 columns">

                        @include('partials.form.fields.file', [
                            'label' => 'Profiel foto',
                            'field' => 'profile_image'
                        ])

                    </div>
                </div>
            </div>
        </div>

        <section class="form form--wide small-12  medium-8 columns">
            <div class="row">

                <header class="form__header block block--narrow">
                    <h1 class="form__title small-12 columns">Mijn profiel</h1>
                </header>

                <div class="form__body">
                    <div class="block block--narrow">

                        <fieldset>
                            <legend>Login gegevens</legend>

                            <div class="small-12 medium-6 columns">

                                @include('partials.form.fields.password', [
                                  'label' => 'Wachtwoord',
                                  'field' => 'password'
                               ])

                            </div>

                            <div class="small-12 medium-6 columns">

                                @include('partials.form.fields.password', [
                                   'label' => 'Wachtwoord herhalen',
                                   'field' => 'password_repeat'
                                ])

                            </div>

                            <div class="small-12 columns">
                                <small class="form__disclaimer ">Een wachtwoord moet mininaal uit acht karakters bestaan
                                    en
                                    moet één hoofdletter, één cijfer en één speciaal teken bevatten.
                                </small>
                            </div>
                        </fieldset>

                    </div>

                    <div class="block block--narrow">

                        <fieldset>
                            <legend>Persoonlijke gegevens</legend>

                            <div class="small-12 medium-6 columns">
                                @include('partials.form.fields.text', [
                                    'label' => 'Voornaam',
                                    'field' => 'first_name',
                                    'value' => $profile->firstName,
                                    'options' => [
                                        'required' => 'required'
                                     ]
                                ])
                            </div>

                            <div class="small-12 medium-6 columns">
                                @include('partials.form.fields.text', [
                                      'label' => 'Achternaam',
                                      'field' => 'last_name',
                                      'value' => $profile->lastName,
                                      'options' => [
                                        'required' => 'required'
                                      ]
                                  ])
                            </div>

                            <div class="small-12 columns">
                                @include('partials.form.field-groups.date-of-birth', [
                                    'value' => $profile->dateOfBirth
                                ])
                            </div>
                            <div class="small-12 columns">
                                @include('partials.form.field-groups.address', [
                                    'streetName' => $profile->streetName,
                                    'streetNumber' => $profile->streetNumber,
                                    'postalCode' => $profile->postalCode,
                                    'city' => $profile->city
                                ])
                            </div>

                            <div class="small-12 columns">
                                @include('partials.form.fields.telephone', [
                                    'label' => 'Telefoonnummer',
                                    'field' => 'phone_number',
                                    'value' => $profile->phoneNumber,
                                    'options' => [
                                        'required' => 'required'
                                    ]
                                ])
                            </div>

                        </fieldset>

                        @yield('extension')

                    </div>
                </div>

                <footer class="form__footer block block--narrow">

                    <div class="small-12 columns">
                        @include('partials.form.fields.submit', [
                            'label' => 'Opslaan',
                            'class' => 'button--depth'
                        ])
                    </div>

                </footer>
            </div>
        </section>

        {{ Form::close() }}
    </div>
@endsection

@push('scripts')
<script src="/js/edit-profile.js"></script>
@endpush
