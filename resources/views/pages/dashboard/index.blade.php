@extends('layouts.default')

@section('main')
    <div class="dashboard">
        <nav class="dashboard__navigation row">
            <ul class="small-12 columns">
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>


                @if (Auth::user()->getType() === 'careprovider')
                    <li><a href="{{ route('dashboard.availability') }}">Beschikbaarheid</a></li>
                @endif

                <li><a href="{{ route('dashboard.edit-profile') }}" >Profiel wijzigen</a></li>
            </ul>
        </nav>

        <main class="dashboard__main">

            @yield('dashboard-page')

        </main>
    </div>
@endsection
