@extends('layouts.default')

@section('main')

    <header class="hero block block--narrow">
        <div class="row">
            <div class="small-12 columns">

                <h1 class="hero__title">
                    Op zoek naar een oppas?
                    <span class="hero__subtitle">Simpel. Betaalbaar. Voor iedereen.</span>
                </h1>

                {{ Form::open(array('route' => array('search.results'), 'method' => 'GET')) }}
                @include('partials.form.forms.search')
                {{ Form::close() }}

            </div>
        </div>
    </header>

@endsection