@extends('layouts.default')


@section('main')

    <div class="row block">
        <div class="small-12 columns">

            <img
                    src="{{ $model->profileImage == null ? '/storage/placeholder.png' : asset('storage/' . $model->profileImage) }}"
                    class="profile-image"
                    alt="{{ $model->name }}"
            />

            <h1>{{ $model->name }}</h1>


            <h2>Rating: {{ $model->rating }}</h2>

            {{  Form::open([route('rate.user')]) }}

            {{ Form::hidden('id', $model->id) }}

            @include('partials.form.field-groups.rating')

            @include('partials.form.fields.submit', [
                'label' => 'Geef een beoordeling',
                'class' => 'button--depth'
            ])

            {{ Form::close() }}
        </div>
    </div>

@endsection
