@extends('pages.profile.profile')


@section('profile-body')
    <article>
        @if ($model->description === null)
            <em>Deze persoon heeft nog geen omschrijving.</em>
        @else
            {{-- TODO: For final product might be nice to use markdown instead of spitting out HTML --}}
            {!!  $model->description !!}
        @endif

        @if ($model->qualities !== null)
            <h2>Mijn kwaliteiten</h2>
            <ul>
                @foreach ($model->qualities as $quality)
                    <li>{{ $quality }}</li>
                @endforeach
            </ul>
        @endif

        {{-- TODO: Implement other actions depending on the relationship with the user --}}
    </article>
@endsection
