@extends('layouts.default')

@section('main')

    <main class="user-profile row">

        <article class="small-12 large-8 columns">

            <header class="user-profile__header row block block--gray">
                <div class="small-12 columns">

                    <figure class="user-profile__profile-image">
                        <img
                                src="{{ $model->profileImage !== null ? asset('storage/' . $model->profileImage) : 'storage/placeholder.png' }}"
                                alt="Profielfoto van {{ $model->name }}"
                        />
                    </figure>

                    <div class="user-profile__title">
                        <h1>{{ $model->name }}</h1>
                        <h2>{{ $model->city }}</h2>
                    </div>

                </div>
            </header>

            <div class="user-profile__body row block block--narrow">
                <div class="small-12 columns">

                    @yield('profile-body')

                </div>
            </div>
        </article>

        <aside class="small-12 large-4 columns">
            {{-- Things like achievements / ratings / recommendations here --}}
        </aside>

    </main>


@endsection




