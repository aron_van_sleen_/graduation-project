@extends('pages.profile.profile')

@section('profile-body')

    <p>Geef drie tijden op wanneer je het interview zou kunnen houden.</p>

    @if (isset($errors) && $errors->count() > 0)
        <span class="form-error is-visible">
            <strong>{{ $errors->first() }}</strong>
        </span>
    @endif

    {{ Form::open(['url' => route('careprovider.request-interview', ['slug' => $slug])]) }}


    @for ($i = 0; $i < 3; $i++)

        <div class="input-group">
            <div class="input-group-label"> {{ $i + 1 }}.</div>
            <label>
                @include('partials.form.fields.text', [
                    'field' => "dates[{$i}]",
                    'options' => [
                        'required' => 'required',
                        'class' => 'input-group-field datetimepicker'
                    ]
                ])
            </label>
        </div>

    @endfor


    @include('partials.form.fields.submit', [
        'label' => 'Kennismakings verzoek versturen',
        'class' => 'button--depth'
    ])

    {{ Form::close() }}

@endsection

@push('scripts')
<script>
    (function ( $ ) {
        $( '.datetimepicker' ).fdatepicker( {
            format: 'dd-mm-yyyy hh:ii',
            language: 'nl',
            pickTime: true,
            disableDblClickSelection: true,
            onRender: function ( date ) {
                return date.valueOf() < new Date().valueOf() ? 'disabled' : ''
            }
        } );
    }( jQuery ));
</script>
@endpush