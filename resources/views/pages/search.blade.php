@extends('layouts.default')

@section('main')

    {{ Form::open(array('route' => array('search.results'), 'method' => 'GET', 'id' => 'search-form')) }}

    <div class="row block block--gray block--narrow">
        <div class="small-12 columns">

            @include('partials.form.forms.search')

        </div>
    </div>

    <section class="search row">
        <header class="search__header small-12 columns">

            <h1>
                @if (Request::has('location'))
                    @if (count($results) > 0)
                        {{ $results->total() }} {{ trans_choice('terms.careproviders', count($results)) }} gevonden.
                    @else
                        Er zijn geen resultaten gevonden.
                    @endif
                @else
                    Zoek naar oppassers.
                @endif
            </h1>

        </header>

        <div class="search__body block block--narrow small-12 columns">
            <div class="row">
                <nav class="search__filters small-12 large-4 columns">

                    <div class="row">
                        <div class="small-12 columns">

                            @include('partials.form.fields.hourly-rate', [
                                'hourlyRate' => Request::has('hourlyRate') ? Request::get('hourlyRate') : 2500,
                                'maximumHourlyRate' => $maximumHourlyRate
                            ])

                        </div>
                        <div class="small-12 columns">

                            @include('partials.form.field-groups.checkbox-group', [
                                'label' => 'Maak het vertrouwd',
                                'field' => 'characteristics',
                                'items' => $characteristics
                            ])

                        </div>
                    </div>

                    @include('partials.form.fields.submit', [
                        'label' => 'Filteren',
                        'class' => 'button--depth'
                    ])

                </nav>

                <div id="search__results" class="small-12 large-8 columns">
                    @if (count($results) > 0)

                        @include('partials.search.search-results', [
                            'results' => $results,
                            'paginationParameters' => $paginationParameters
                        ])

                    @endif
                </div>
            </div>
        </div>

    </section>

    {{ Form::close() }}

    {{-- Intented to be outside the form, don't want it as part as the GET url. Using it for Ajax requests only --}}
    {{--{{ Form::token() }}--}}

@endsection

@push('scripts')
<script src="/js/search-results.js"></script>
@endpush




