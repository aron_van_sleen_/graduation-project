<article class="card" data-equalizer-watch>

    <header class="card__header">
        <figure class="card__profile-image">
            <img
                    src="{{ $careprovider->profileImage == null ? '/storage/placeholder.png' : asset('storage/' . $careprovider->profileImage) }}"
                    alt="{{ $careprovider->name }}"
            />
        </figure>

        <h1 class="card__title">{{ $careprovider->name }}</h1>
        <h2 class="card__city">{{ $careprovider->city }}</h2>
    </header>


    <ul class="card__information">
        <li>Prijs per uur: € {{ number_format($careprovider->hourlyRate / 100, 2, '.', ' ') }}</li>
    </ul>


    <a href="{{ route('user.profile', $careprovider->slug) }}" class="card__button secondary button button--rounded">
       Bekijk <span class="show-for-sr">het</span> profiel <span class="show-for-sr">van {{$careprovider->name}}</span>
    </a>


    <a href="{{ route('user.profile', $careprovider->slug) }}" class="card__anchor" tabindex="-1"></a>
</article>