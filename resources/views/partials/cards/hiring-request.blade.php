<article class="request-card">

    @include('partials.cards.request-profile-info')

    <p class="request-card__hiring-info">
        {{ strftime('%e %B', $request['date']->timestamp) }} van {{ $request['date']->format('H:i') }}
        tot {{ $request['to']->format('H:i') }}
    </p>

    @if ($model['comment'] !== null)

        <p class="request-card__comment">{{ $model['comment'] }}</p>

    @endif

    <footer class="request-card__footer">

        <button class="request-card__action alert button button--depth decline">
            <i class="fa fa-close" aria-hidden="true"></i>
        </button>

        <button class="request-card__action button button--depth accept">
            <i class="fa fa-check" aria-hidden="true"></i>
        </button>

    </footer>

</article>