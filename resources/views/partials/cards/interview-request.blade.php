<article class="request-card">

    @include('partials.cards.request-profile-info')

    @if ($model['comment'] !== null)

        <p class="request-card__comment">{{ $model['comment'] }}</p>

    @endif

    <div class="request-card__interview-dates">
        <h2>Kies één van de volgende data</h2>
        <ul>
            @foreach($request['dates'] as $date)

                <li data-date="{{ $date->format('Y-m-d h:i') }}" class="request-card__interview-dates__date">
                    <a href="#" class="request-card__action accept">
                        <span>{{ strftime('%e %B', $date->timestamp) }}</span> <br />
                        <span>{{ $date->format('H:i') }}</span>
                    </a>
                </li>

            @endforeach

        </ul>
    </div>


    <footer class="request-card__footer">

        <button class="request-card__action alert button button--depth decline">
            <i class="fa fa-close"></i> Ik ben niet beschikbaar op deze data
        </button>

    </footer>

</article>