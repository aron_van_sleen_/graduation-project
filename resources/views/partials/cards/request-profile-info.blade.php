<header class="request-card__header">
    <a href="{{ route('user.profile', ['slug' => $model['slug']]) }}" tabindex="-1">
        <figure class="request-card__profile-image">
            <img
                    src="{{ $model['profileImage'] == null ? '/storage/placeholder.png' : asset('storage/' . $model['profileImage']) }}"
                    alt="Profielfoto van {{ $model['name'] }}"
            />
        </figure>
    </a>

    <div class="request-card__user-info">
        <a href="{{ route('user.profile', ['slug' => $model['slug']]) }}">
            <h1>{{ $model['name'] }}</h1>
        </a>

        <p><i class="fa fa-envelope-o"></i> {{ $model['email'] }}</p>
        <p><i class="fa fa-phone"></i> {{ $model['phoneNumber'] }}</p>
    </div>
</header>
