<div class="hiring-requests">

    <h2 class="hiring-requests__title">Inhuur verzoeken</h2>

    <ul>
        @if (count($hiringRequests) > 0)
            @foreach($hiringRequests as $request)
                <li class="hiring-requests__request request" data-type="hiring" data-id="{{ $request['id'] }}">

                    @include('partials.cards.hiring-request', [
                         'model' => $request
                    ])

                </li>
            @endforeach
        @else
            <li class="hiring-requests__request"><span>Momenteel zijn er nog geen verzoeken.</span></li>
        @endif
    </ul>

</div>