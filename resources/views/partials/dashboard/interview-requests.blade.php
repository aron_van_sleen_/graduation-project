<div class="interview-requests">

    <h2 class="interview-requests__title">Interview verzoeken</h2>

    <ul>
        @if (count($interviewRequests) > 0)
            @foreach($interviewRequests as $request)
                <li class="interview-requests__request request" data-type="interview" data-id="{{ $request['id'] }}">

                    @include('partials.cards.interview-request', [
                        'model' => $request
                    ])

                </li>
            @endforeach
        @else
            <li class="interview-requests__request">
                <span>Momenteel zijn er nog geen interview verzoeken.</span>
            </li>
        @endif
    </ul>

</div>
