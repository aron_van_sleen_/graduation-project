<div class="row">
    <div class="small-9 columns">
        @include('partials.form.fields.text', [
            'label' => 'Straatnaam',
            'field' => 'street_name',
            'value' => isset($streetName) ? $streetName : null,
            'options' => [
                'required' => 'required'
             ]
        ])
    </div>

    <div class="small-3 columns">
        @include('partials.form.fields.text', [
            'label' => 'Huisnr.',
            'field' => 'street_number',
            'value' => isset($streetNumber) ? $streetNumber : null,
            'options' => [
                'required' => 'required'
             ]
        ])
    </div>
</div>

<div class='row'>
    <div class='small-12 medium-6 columns'>
        <div class='row'>

            @include('partials.form.field-groups.postal-code', [
                'postalCode' => isset($postalCode) ? $postalCode : null
            ])

        </div>
    </div>

    <div class='small-12 medium-6 columns'>
        @include('partials.form.fields.text', [
            'label' => 'Plaats',
            'field' => 'city',
            'value' => isset($city) ? $city : null,
            'options' => [
                'required' => 'required'
            ]
        ])
    </div>
</div>


