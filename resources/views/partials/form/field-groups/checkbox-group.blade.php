@if (isset($label))
    <label for="characteristics">{{ $label }}</label>
@endif

@foreach ($items as $key => $item)
    <label>
        @if (isset($checked) && $checked === true)
            {{-- Default the checkboxes to checked --}}
            {{ Form::checkbox("{$field}[]", $key, isset($value) && ! in_array($key, $value) ? false : true)  }}
        @else
            {{ Form::checkbox("{$field}[]", $key, isset($value) && in_array($key, $value) ? true : false)  }}
        @endif

        {{ $item }}
    </label>
@endforeach
