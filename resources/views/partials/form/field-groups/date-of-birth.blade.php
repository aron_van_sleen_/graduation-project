<div class="row">
    <div class="small-12 columns">
        {{ Form::label('date_of_birth[day]', 'Geboortedatum') }}
    </div>

    <div class="small-4 columns">
        {{
            Form::input('number', 'birth_day', (isset($value['day']) ? $value['day'] : null), [
                "name" => "birth_day",
                "min" => 1,
                "max" => 31,
                "class" => (isset($errors) && $errors->has('date_of_birth') ? "is-invalid-input " : '') . "form-control" . (isset($class) ? $class : ''),
                "placeholder" => "17",
                "required" => "required"
            ])
        }}
    </div>

    <div class="small-4 columns">
        {{
            Form::input('number', 'birth_month', (isset($value['month']) ? $value['month'] : null), [
                "name" => "birth_month",
                "min" => 1,
                "max" => 12,
                "class" => (isset($errors) && $errors->has('date_of_birth') ? "is-invalid-input " : '') . "form-control" . (isset($class) ? $class : ''),
                "placeholder" => "04",
                "required" => "required"
            ])
        }}
    </div>

    <div class="small-4 columns">
        {{
            Form::input('number', 'birth_year', (isset($value['year']) ? $value['year'] : null), [
                "name" => "birth_year",
                "pattern" => "[0-9]{4}",
                "class" => (isset($errors) && $errors->has('date_of_birth') ? "is-invalid-input " : '') . "form-control" . (isset($class) ? $class : ''),
                "placeholder" => "1990",
                "required" => "required"
            ])
        }}
    </div>

    @if (isset($disclaimer))
        <div class="form__disclaimer small-12 columns">
            <small>{{ $disclaimer }}</small>
        </div>
    @endif

    @if (isset($errors) && $errors->has('date_of_birth'))

        <div class="small-12 columns">
            <span class="form-error is-visible">
                <strong>{{ $errors->first('date_of_birth') }}</strong>
            </span>
        </div>

    @endif

</div>
