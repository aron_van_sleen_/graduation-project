<div class="small-12 columns">
    <div class="row">
        <div class="small-12 columns">
            {{ Form::label('postal_digits', 'Postcode') }}
        </div>

    </div>

    <div class="row">

        <div class="small-6 medium-8 columns">
            {{
                Form::input('text', 'postal_digits', (isset($postalCode['digits']) ? $postalCode['digits'] : null), [
                    "name" => "postal_digits",
                    "class" => (isset($errors) && $errors->has('postal_code') ? "is-invalid-input " : '') . "form-control" . (isset($class) ? $class : ''),
                    "placeholder" => "7741",
                    "required" => "required"
                ])
            }}
        </div>

        <div class="small-6 medium-4 columns">
            {{
                Form::input('text', 'postal_characters', (isset($postalCode['characters']) ? $postalCode['characters'] : null), [
                    "name" => "postal_characters",
                    "class" => (isset($errors) && $errors->has('postal_code') ? "is-invalid-input " : '') . "form-control" . (isset($class) ? $class : ''),
                    "placeholder" => "JE",
                    "required" => "required"
                ])
            }}
        </div>

        @if (isset($errors) && $errors->has('postal_code'))

            <div class="small-12 columns">
                    <span class="form-error is-visible">
                        <strong>{{ $errors->first('postal_code') }}</strong>
                    </span>
            </div>


        @endif

    </div>
</div>