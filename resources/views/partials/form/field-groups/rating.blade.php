<fieldset class="rate">

    @if(isset($label))
        <legend>{{$label}}</legend>
    @endif

    <div class="rate__stars">

        <input type="radio" id="rating10" name="rating" value="10" required/><label class="full" for="rating10" title="5 stars"></label>
        <input type="radio" id="rating9" name="rating" value="9"/><label class="half" for="rating9"
                                                                         title="4 1/2 stars"></label>

        <input type="radio" id="rating8" name="rating" value="8"/><label  class="full" for="rating8" title="4 stars"></label>
        <input type="radio" id="rating7" name="rating" value="7"/><label class="half" for="rating7"
                                                                         title="3 1/2 stars"></label>

        <input type="radio" id="rating6" name="rating" value="6"/><label  class="full" for="rating6" title="3 stars"></label>
        <input type="radio" id="rating5" name="rating" value="5"/><label class="half" for="rating5"
                                                                         title="2 1/2 stars"></label>

        <input type="radio" id="rating4" name="rating" value="4"/><label  class="full" for="rating4" title="2 stars"></label>
        <input type="radio" id="rating3" name="rating" value="3"/><label class="half" for="rating3"
                                                                         title="1 1/2 stars"></label>

        <input type="radio" id="rating2" name="rating" value="2"/><label  class="full" for="rating2" title="1 star"></label>



        {{--<input type="radio" id="rating1" name="rating" value="1"/><label class="half" for="rating1"--}}
                                                                         {{--title="1/2 star"></label>--}}
        {{--<input type="radio" id="rating0" name="rating" value="0"/><label for="rating0" title="No star"></label>--}}
    </div>
</fieldset>