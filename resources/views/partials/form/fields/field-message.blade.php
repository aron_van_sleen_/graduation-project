@if (isset($errors) && $errors->has($error_field)  && (!isset($disabled) || $disabled != true))
    <span class="form-error is-visible">
        <strong>{{ $errors->first($error_field) }}</strong>
    </span>
@endif

@if(isset($help))
    <p class="help-text">{!! $help !!}</p>
@endif