@if (isset($label))
<label for="{{ $field }}"> {{ $label }}
@endif

{{ Form::file($field) }}

@if (isset($label))
</label>
@endif

@if ( ! isset($suppressErrors) || false === $suppressErrors )
    @include('partials.form.fields.field-message', ['error_field' => $field])
@endif