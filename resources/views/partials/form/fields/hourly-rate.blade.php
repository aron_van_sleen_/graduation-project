<div class="row">
    <label class="small-12 columns"> Prijs per uur:
        <span id="hourlyRateOutput">
                € {{ number_format($hourlyRate / 100, 2, '.', ' ') }}
            </span>
    </label>

    <div class="small-12 columns">

        <div class="slider" data-slider data-step="5" data-initial-start="{{ $hourlyRate }}"
             data-end="{{ $maximumHourlyRate }}">
            <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
            <span class="slider-fill"></span>
            <input name="hourlyRate" type="hidden">
        </div>

    </div>

    @if (isset($errors) && $errors->has('hourlyRate'))

        <div class="small-12 columns">
        <span class="form-error is-visible">
            <strong>{{ $errors->first('hourlyRate') }}</strong>
        </span>
        </div>

    @endif
</div>