@if(isset($disabled) && $disabled)

    <input
            id="{{ $id or $field }}"
            class="form-control {{ $class or '' }}"
            name="{{ $field }}"
            placeholder="{{ $placeholder or '' }}"
            type="{{ $type or 'text' }}"
    @if(isset($options) && is_array($options))
        @foreach($options as $option)
            {{ '' . $option }}
                @endforeach
            @endif
    >

@else

    <?php $error_field = isset($error_field) ? $error_field : $field ?>

    @if (isset($label))
        <label for="{{ isset($id) ? $id : $field }}"> {{ $label }}
    @endif

    {!! Form::input($type, $field, (isset($value) ? $value : null), array_merge([
        'id' => (isset($id) ? $id : $field),
        'class' => (
            isset($errors) && $errors->has($error_field) && (! isset($suppressErrors) || false === $suppressErrors)
            ? "is-invalid-input " : ''
        ) . "form-control " . (isset($class) ? $class : ''),
        'name' => $field,
        'placeholder' => isset($placeholder) ? $placeholder : ''
    ], (isset($options) && is_array($options) ? $options : []))) !!}

    @if (isset($label))
        </label>
    @endif

@endif

@if ( ! isset($suppressErrors) || false === $suppressErrors )
    @include('partials.form.fields.field-message', ['error_field' => $field])
@endif