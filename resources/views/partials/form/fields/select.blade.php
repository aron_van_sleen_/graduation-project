<?php $error_field = isset($error_field) ? $error_field : $field ?>

@if (isset($label))
    <label for="{{ isset($id) ? $id : $field }}"> {{ $label }}
        @endif

        {!! Form::select($field, $list, (isset($value) ? $value : old($field)), array_merge([
            'id' => (isset($id) ? $id : $field),
           'class' => (
                    isset($errors) && $errors->has($field) && (! isset($suppressErrors) || false === $suppressErrors)
                    ? "is-invalid-input " : ''
                ) . "form-control " . (isset($class) ? $class : ''),
            'name' => $field,
            'placeholder' => isset($placeholder) ? $placeholder : '',
        ], (isset($options) && is_array($options) ? $options : []))) !!}

        @if (isset($label))
    </label>
@endif

@if ( ! isset($suppressErrors) || false === $suppressErrors )
    @include('partials.form.fields.field-message', ['error_field' => $field])
@endif