
<button type="submit" class="button {{ isset($class) ? $class : '' }}">
    {{ $label or 'Verstuur' }}
</button>
