@if (isset($label))
    <label for="{{ $field }}"> {{ $label }}
@endif

    {!! Form::textarea($field, isset($value) ? $value : null, array_merge([
        'placeholder' => isset($placeholder) ? $placeholder : null,
        'class' => "form-control " . (isset($class) ? $class : '')
    ], isset($options) ? $options : [])) !!}

@if (isset($label))
    </label>
@endif

