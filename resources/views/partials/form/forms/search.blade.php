<section class="search-form" role="search">

    <div class="row">
        <div class="search-form__input-section  small-12 columns">

            {{-- Build up the date field --}}
            <input id="datepicker" name="date" type="text" class="search-form__date"
                   value="{{ Request::has('date') ? Request::get('date') : '' }}" placeholder="Datum..." required />


            @include('partials.form.fields.select', [
                'field' => 'from',
                'list' => array_map(function($n) { return sprintf("%02d:00", $n); }, range(0, 23)),
                'placeholder' => 'Van',
                'options' => [
                    'required' => 'required'
                ],
                'class' => 'search-form__from',
                'suppressErrors' => true
            ])

            @include('partials.form.fields.select', [
                'field' => 'to',
                'list' => array_map(function($n) { return sprintf("%02d:00", $n + 1); }, array_flip(range(1, 24))),
                'placeholder' => 'Tot',
                'options' => [
                    'required' => 'required'
                ],
                'class' => 'search-form__to',
                'suppressErrors' => true
            ])

        </div>
    </div>


    <div class="row">
        <div class="search-form__input-section small-12 columns">

            @include('partials.form.fields.text', [
               'field' => 'location',
               'placeholder' => 'Plaats of postcode...',
               'options' => [
                   'required' => 'required'
               ],
               'class' => 'search-form__location',
               'suppressErrors' => true
            ])

            @include('partials.form.fields.select', [
                'field' => 'radius',
                'list' => [5 => '5 kilometer', 10 => '10 kilometer', 20 => '20 kilometer', 50 => '50 kilometer'],
                'placeholder' => 'Zoek binnen straal',
                'options' => [
                    'required' => 'required'
                ],
                'class' => 'search-form__radius',
                'suppressErrors' => true
            ])

            @include('partials.form.fields.submit', ['label' => 'Zoeken', 'class' => 'button--rounded'])
        </div>
    </div>

    @if (count($errors) > 0)

        <div class="row search-form__errors">
            <div class="small-12 columns">
                    <span class="form-error is-visible">
                        <strong>{{ $errors->first() }}</strong>
                    </span>
            </div>
        </div>

    @endif

</section>

@push('scripts')
<script src="/js/search.js"></script>
@endpush