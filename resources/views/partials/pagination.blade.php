<nav class="pagination row" aria-label="Pagination">
    <div class="small-12 columns">

        <ul>
            @if ($page > 1)
                <li class="pagination__previous-button">
                    <a href="{{ route(Route::current()->getName()) }}&{{ $page - 1 }}">
                        < <span class="show-for-sr">Vorige pagina</span>
                    </a>
                </li>
            @else
                <li class="pagination__previous-button disabled"> < <span class="show-for-sr">Vorige pagina</span></li>
            @endif

            @if ($page > 1)
                <li><a>{{ $page - 1 }}</a></li>
            @endif

            <li><a>{{ $page }}</a></li>

            <li class="pagination__next-button">
                <a href="{{ route(Route::current()->getName()) }}">
                    > <span class="show-for-sr">Volgende pagina</span>
                </a>
            </li>
        </ul>

    </div>
</nav>
