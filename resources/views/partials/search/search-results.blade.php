<ul class="search__results row small-up-1 medium-up-2 large-up-3"
    data-equalizer data-equalize-on="medium">
    @foreach($results as $result)

        <li class="search__results__item column">
            @include('partials.cards.careprovider', ["careprovider" => $result])
        </li>

    @endforeach
</ul>

<div class="search__pagination row">
    <div class="small-12 columns">

        {{-- Render the pagination --}}
        {!! $results->setPath(route('search.results'))->appends($paginationParameters)->render() !!}

    </div>
</div>