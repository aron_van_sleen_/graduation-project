@extends('layouts.default')


@section('main')

    <main class="form row">

        {{ Form::open(['route' => 'subscription.checkout.paypal']) }}

        <header class="form__header block block--narrow">
            <div class="small-12 columns">

                <h1 class="form__title">Bevestig je keuze</h1>

            </div>
        </header>


        <div class="form__body block block--narrow">
            <div class="small-12 columns">

                @include('partials.form.fields.select', [
                    'label' => 'Gekozen abonnement',
                    'field' => 'plan',
                    'list' => $plans,
                    'options' => [
                        'required' => 'required'
                    ]
                ])

                <small class="form__disclaimer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed cursus
                    ipsum non nibh vestibulum, non dignissim libero imperdiet. Aliquam interdum vel nisi gravida
                    lobortis. Nam condimentum tristique eleifend. Ut quis faucibus odio.
                </small>

            </div>
        </div>

        <footer class="form__footer block block--narrow">
            <div class="small-12 columns">

                <button class="button button--depth"><i class="fa fa-paypal"></i>Afrekenen met PayPal</button>
                <a href="{{ route('subscription.plans') }}"> Terug naar de abonnementen</a>

            </div>
        </footer>

        {{ Form::close() }}
    </main>

@endsection