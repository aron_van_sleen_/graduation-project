@extends('layouts.default')


@section('main')

    <div class="block row">
        <div class="small-12 columns">
            <h1> Kies een abonnement</h1>


            <div class="subscription-plans">
                @foreach ($plans as $type => $plan)
                    <article class="subscription-plans__plan subscription-plans__plan--{{ $type }}">

                        <header class="subscription-plans__plan__header">
                            <h1 class="subscription-plans__plan__title">{{ trans("subscriptions.{$type}.title") }}</h1>
                        </header>

                        <div class="subscription-plans__plan__body">
                            <p>Prijs per jaar: &euro; {{ number_format($plan['price'] / 100, 2, ',', '') }}</p>

                            {{--TODO: Replace with actual plan information--}}
                            <table>
                                <tr>
                                    <td>Iets</td><td>Waarde</td>
                                </tr>
                                <tr>
                                    <td>Iets 2</td><td>Waarde 2</td>
                                </tr>
                                <tr>
                                    <td>Iets 3</td><td>Waarde 3</td>
                                </tr>
                            </table>
                        </div>

                        <footer class="subscription-plans__plan__footer">
                            <a class="button button--depth"
                               href="{{ route('subscription.checkout', ['plan' => $type]) }}">

                                Kies dit pakket
                            </a>
                        </footer>

                    </article>
                @endforeach
            </div>
        </div>
    </div>

@endsection