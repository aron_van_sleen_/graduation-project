<?php

/** API routes needing Authentication */
Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => '/interview-requests'], function () {
        Route::post('/{id}/accept', ['as' => 'interview-requests.accept', 'uses' => 'Api\InterviewRequestController@accept']);
        Route::delete('/{id}/decline', ['as' => 'intervier-requests.decline', 'uses' => 'Api\InterviewRequestController@decline']);
    });

    Route::group(['prefix' => '/hiring-requests'], function () {
        Route::post('/{id}/accept', ['as' => 'hiring-requests.accept', 'uses' => 'Api\HiringRequestController@accept']);
        Route::delete('/{id}/decline', ['as' => 'hiring-requests.decline', 'uses' => 'Api\HiringRequestController@decline']);
    });

});