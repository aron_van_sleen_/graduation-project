<?php

/** Authentication routes  & registration routes */

Route::group(['namespace' => 'Auth'], function () {
    
    Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'AuthController@logout']);

    Route::group(['middleware' => 'guest'], function () {
        Route::get('/inloggen', ['as' => 'auth.login', 'uses' => 'AuthController@showLoginForm']);
        Route::post('/inloggen', ['as' => 'auth.login', 'uses' => 'AuthController@login']);

        Route::get('/aanmelden', ['as' => 'auth.registration.role', 'uses' => 'RegistrationController@showRoleSelectionPage']);
        Route::get('/aanmelden/als-ouder-of-verzorger', ['as' => 'auth.registration.careclient', 'uses' => 'RegistrationController@showRegisterAsCareclientForm']);
        Route::get('/aanmelden/als-oppasser', ['as' => 'auth.registration.careprovider', 'uses' => 'RegistrationController@showRegisterAsCareproviderForm']);

        Route::post('/registeren-als-ouder-of-verzorger', ['as' => 'auth.register.careclient', 'uses' => 'RegistrationController@registerAsCareclient']);
        Route::post('/registeren-als-oppasser', ['as' => 'auth.register.careprovider', 'uses' => 'RegistrationController@registerAsCareprovider']);

        Route::get('/wachtwoord-vergeten', ['as' => 'auth.password.email-form', 'uses' => 'PasswordController@getEmail']);
        Route::post('/wachtwoord-vergeten', ['as' => 'auth.password.send-link', 'uses' => 'PasswordController@postEmail']);

        Route::get('/reset-wachtwoord/{token?}', ['as' => 'auth.password.reset-form', 'uses' => 'PasswordController@showResetForm']);
        Route::post('/reset-wachtwoord', ['as' => 'auth.password.reset', 'uses' => 'PasswordController@reset']);
    });
});

/** Routes that need authentication */
Route::group(['middleware' => ['auth', 'subscription.active']], function () {
    /** Dashboard routes */
    Route::group(['namespace' => 'Dashboard'], function () {
        Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@showDashboardPage']);
        Route::get('/dashboard/profiel-wijzigen', ['as' => 'dashboard.edit-profile', 'uses' => 'DashboardController@showEditProfileForm']);

        // a GET request?
        Route::get('/dashboard/profiel-afbeelding-verwijderen', ['as' => 'dashboard.remove-profile-image', 'uses' => 'DashboardController@removeProfileImage']);

        Route::group(['middleware' => 'auth.careprovider'], function () {
            Route::get('/dashboard/beschikbaarheid/{year?}/{month?}', ['as' => 'dashboard.availability', 'uses' => 'CareproviderDashboardController@showAvailabilityForm']);

            // TODO: Possibly refactor to AJAX API route group & controllers
            Route::get('/availability', ['uses' => 'CareproviderDashboardController@getAvailabilityBetweenDates']);
            Route::post('/availability', ['as' => 'dashboard.stateavailability', 'uses' => 'CareproviderDashboardController@stateAvailability']);

            Route::post('/dashboard/oppasser-profiel-wijzigen', ['as' => 'dashboard.edit-careprovider-profile', 'uses' => 'DashboardController@updateCareproviderProfile']);
        });

        Route::group(['middleware' => 'auth.careclient'], function () {
            Route::post('/dashboard/ouder-profiel-wijzigen', ['as' => 'dashboard.edit-careclient-profile', 'uses' => 'DashboardController@updateCareclientProfile']);
        });
    });

    Route::get('profiel/{slug}', ['as' => 'user.profile', 'uses' => 'Profilecontroller@showUserProfilePage']);

    /** Careclient only routes */
    Route::group(['middleware' => 'auth.careclient'], function () {
        Route::get('profiel/{slug}/kennismaken', ['as' => 'careprovider.request-interview', 'uses' => 'ProfileController@showRequestInterviewPage']);
        Route::post('profiel/{slug}/kennismaken', ['as' => 'careprovider.request-interview', 'uses' => 'ProfileController@requestInterview']);
    });
});

/**
 * These routes can't be in the general auth group, otherwise u get a redirect wars,
 * the general redirect group requires active subscription for careclient, thus redirecting to these routes if not
 */
Route::get('/abonnementen', ['as' => 'subscription.plans', 'uses' => 'SubscriptionController@showSubscriptionPlansPage']);
Route::group(['middleware' => ['auth.careclient', 'subscription.inactive']], function () {
    Route::get('/abonnementen/checkout', ['as' => 'subscription.checkout', 'uses' => 'SubscriptionController@showSubscriptionCheckoutPage']);
    Route::post('/abonnementen/checkout', ['as' => 'subscription.checkout.paypal', 'uses' => 'SubscriptionController@checkoutToPayPal']);
});

/** Search routes */
Route::get('/zoeken', ['as' => 'search.results', 'uses' => 'SearchController@showResultsPage']);
Route::post('/zoeken', ['as' => 'search.filter', 'uses' => 'SearchController@filterSearch']);

/** PayPal IPN Routes */
Route::post('/ipn-subscription-purchased', ['as' => 'ipn.subscription.purchased', 'uses' => 'PayPalIPNController@handleSubscriptionPurchasedIPNMessage']);


Route::get('/', ['as' => 'home', 'uses' => function () {
    return view('pages.frontpage');
}]);