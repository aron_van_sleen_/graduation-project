<?php

use Carbon\Carbon;
use Acme\App\Services\BcryptHasher;
use Acme\Domain\Careprovider\Availability;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Shared\Email;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Password;
use Acme\Domain\Shared\TimeRange;
use Webpatser\Uuid\Uuid;

class AvailabilityTest extends TestCase
{
    /**
     * @var Careprovider
     */
    private $careprovider;

    public function setUp()
    {
        $this->careprovider = Careprovider::register(
            new Email('asleen@impres.nl'),
            HashedPassword::fromHashingStrategy(new Password('Veilig123!'), new BcryptHasher())
        );
    }

    /** @test */
    public function it_should_be_able_to_state_available_time_ranges()
    {
        $timeRange = new TimeRange(1, 5);
        $availability = new Availability(Carbon::create(3000, 07, 25, 0), $timeRange, $this->careprovider);

        $this->assertInstanceOf(Availability::class, $availability);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_cannot_state_availability_in_the_past()
    {
        $timeRange = new TimeRange(0, 5);
        new Availability(Carbon::create(2016, 07, 25, 0), $timeRange, $this->careprovider);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function a_careprovider_cannot_state_more_than_three_availability_at_a_time()
    {
        $availability = collect([
            new Availability(Carbon::now(), new TimeRange(3, 7), $this->careprovider),
            new Availability(Carbon::now(), new TimeRange(3, 7), $this->careprovider),
            new Availability(Carbon::now(), new TimeRange(3, 7), $this->careprovider),
            new Availability(Carbon::now(), new TimeRange(3, 7), $this->careprovider),
        ]);

        $this->careprovider->stateAvailability($availability);
    }
}