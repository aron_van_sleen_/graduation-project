<?php

use Money\Money;
use Acme\App\Services\BcryptHasher;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Careprovider\Level;
use Acme\Domain\Shared\Email;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Password;

class CareproviderTest extends TestCase
{
    /** @var  Careprovider */
    private $careprovider;

    public function setUp()
    {
        $this->careprovider = Careprovider::register(
            new Email('johndoe@impres.nl'), HashedPassword::fromHashingStrategy(
            new Password('Veilig123!'), new BcryptHasher()
        ));
        $this->careprovider->setLevel(new Level("level 1", Money::EUR(1500)));
    }

    /** @test */
    public function it_can_set_an_hourly_rate()
    {
        $newHourlyRate = Money::EUR(800);
        $this->careprovider->setHourlyRate($newHourlyRate);

        $this->assertEquals($this->careprovider->getHourlyRate()->getAmount(), $newHourlyRate->getAmount());
    }

    /**
     * @test
     * @expectedException \Acme\Domain\Careprovider\Exceptions\HourlyRateCannotExceedTheMaximumRateOfTheLevel
     */
    public function it_can_not_state_an_hourly_rate_higher_than_its_allowed_maximum()
    {
        $newHourlyRate = Money::EUR(2000);
        $this->careprovider->setHourlyRate($newHourlyRate);
    }
}