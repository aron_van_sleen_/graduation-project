<?php

use Acme\App\Services\BcryptHasher;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Password;

class HashingTest extends TestCase
{
    /**
     * @var BcryptHasher
     */
    protected $strategy;

    public function setUp()
    {
        $this->strategy = new BcryptHasher();
    }

    /** @test */
    public function it_should_hash_passwords_to_hashed_passwords()
    {
        $password = new Password("%123A456");

       $hashed = HashedPassword::fromHashingStrategy($password, $this->strategy);

        $this->assertInstanceOf('Saferia\Domain\Shared\HashedPassword', $hashed);
    }

    /** @test */
    public function it_should_be_able_to_check_passwords()
    {
        $password = new Password("A123456%");
        $hashed = HashedPassword::fromHashingStrategy($password, $this->strategy);
        
        $this->assertTrue($this->strategy->check($password, $hashed));
    }
}