<?php

use Carbon\Carbon;
use Acme\App\Services\BcryptHasher;
use Acme\Domain\Careclient\Careclient;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Hiring\Interview;
use Acme\Domain\Hiring\InterviewRequest;
use Acme\Domain\Shared\Email;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Password;
use Webpatser\Uuid\Uuid;

class InterviewRequestTests extends TestCase
{
    private $careprovider;

    private $careclient;

    public function setUp()
    {
        $this->careprovider = Careprovider::register(
            new Email('asleen@impres.nl'),
            HashedPassword::fromHashingStrategy(new Password('Veilig123!'), new BcryptHasher())
        );
        $this->careclient = Careclient::register(
            new Email('asleen+zorger@impres.nl'),
            HashedPassword::fromHashingStrategy(new Password('Veilig123!'), new BcryptHasher())
        );
    }

    /** @test */
    public function it_should_be_able_to_be_requested()
    {
        $dates = collect([Carbon::now()->addDays(5), Carbon::now()->addDays(3), Carbon::now()->addDays(2)]);
        $request = new InterviewRequest($this->careclient, $this->careprovider, $dates);

        $this->assertInstanceOf(InterviewRequest::class, $request);
    }

    /**
     * @test
     * @expectedException  \InvalidArgumentException
     */
    public function it_must_have_atleast_three_datetimes()
    {
        $dates = collect([Carbon::now()->addDays(3)]);
        $request = new InterviewRequest($this->careclient, $this->careprovider, $dates);
    }

    /**
     * @test
     * @expectedException  \InvalidArgumentException
     */
    public function it_cannot_have_a_date_in_the_past()
    {
        $dates = collect([Carbon::now()->subDays(1), Carbon::now()->addDays(3), Carbon::now()->addDays(2)]);
        $request = new InterviewRequest($this->careclient, $this->careprovider, $dates);

        $this->assertInstanceOf(InterviewRequest::class, $request);
    }
}