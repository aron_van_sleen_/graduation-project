<?php

use Acme\Domain\Shared\MovingAverage;

class MovingAverageTest extends TestCase
{
    /** @test */
    public function a_new_moving_average_has_an_average_of_0()
    {
        $movingAverage = new MovingAverage();

        $this->assertTrue($movingAverage->getValue() === 0);
    }

    /** @test */
    public function it_should_be_able_to_be_added_to()
    {
        $movingAverage = new MovingAverage();

        $movingAverage->addToAverage(5);

        $this->assertTrue($movingAverage->getValue() === 5);
    }

    /** @test */
    public function it_should_be_able_to_calculate_new_averages()
    {
        $movingAverage = new MovingAverage();

        $movingAverage->addToAverage(5);
        $movingAverage->addToAverage(5);
        $movingAverage->addToAverage(2);

        $this->assertTrue($movingAverage->getValue() === 4);
    }
    
    /** @test */
    public function it_should_be_able_to_keep_track_of_amount_of_times_added_to()
    {
        $movingAverage = new MovingAverage();
        $movingAverage->addToAverage(1);
        $movingAverage->addToAverage(1);
        $movingAverage->addToAverage(1);

        $this->assertTrue($movingAverage->getCount() === 3);
    }

    /** @test */
    public function it_can_remove_old_amounts()
    {
        $movingAverage = new MovingAverage();
        $movingAverage->addToAverage(3);
        $movingAverage->addToAverage(2);
        $movingAverage->addToAverage(5);

        $movingAverage->subtractFromAverage(5);

        $this->assertTrue($movingAverage->getValue() === 2.5);
    }
}