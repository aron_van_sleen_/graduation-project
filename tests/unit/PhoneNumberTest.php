<?php

use Acme\Domain\Shared\PhoneNumber;

class PhoneNumberTest extends TestCase
{
    /** @test */
    public function it_should_be_able_to_convert_phoneNumber_to_numeric()
    {
        $phoneNumber = new PhoneNumber('06 (123) 45-678');
        $this->assertEquals($phoneNumber->getNumeric(), '0612345678');
    }

    /**
     * @test
     * @expectedException Exception
     */
    public function it_should_not_accept_an_invalid_phone_number_format()
    {
        new PhoneNumber('06 234 56 789 132');
    }

    /** @test */
    public function it_allows_american_phone_numbers()
    {
        $phoneNumber = new PhoneNumber('+1 - 800 - 633 - 3469');
        $this->assertInstanceOf(PhoneNumber::class, $phoneNumber);
    }
}