<?php

use Acme\Domain\Shared\Rating;

class RatingTest extends TestCase
{
    /** @test */
    public function it_can_be_created()
    {
        $rating = new Rating(3);

        $this->assertTrue($rating instanceof Rating);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_cannot_have_negative_numbers()
    {
        new Rating(-1);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_cannot_have_a_value_above_5()
    {
        new Rating(5.01);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_can_only_have_numeric_values()
    {
       new Rating("asdas");
    }
}