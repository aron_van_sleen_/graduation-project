<?php

use Acme\Domain\Shared\TimeRange;

class TimeRangeTest extends TestCase
{
    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function it_cannot_state_availability_ending_at_zero_o_clock()
    {
     new TimeRange(23, 0);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function it_cannot_state_availability_starting_24_o_clock()
    {
       new TimeRange(24, 5);
    }

    /** @test */
    public function it_can_intersect_another_time_range()
    {
        $timeRange = new TimeRange(7, 12);
        $other = new TimeRange(7, 12);

        $this->assertTrue($timeRange->intersects($other));
    }

    /** @test */
    public function it_can_be_combined_into_a_larger_time_range()
    {
        $timeRange = new TimeRange(7, 12);
        $other = new TimeRange(1, 7);

        $larger = $timeRange->combine($other);

        $this->assertEquals($larger->from(), 1);
        $this->assertEquals($larger->to(), 12);
    }

    /** @test */
    public function it_can_get_its_duration()
    {
        $timeRange = new TimeRange(5, 23);
        $this->assertEquals($timeRange->duration(), 18);
    }
}