<?php

use Carbon\Carbon;
use Acme\App\Services\BcryptHasher;
use Acme\Domain\Careprovider\Careprovider;
use Acme\Domain\Careprovider\CareproviderRepository;
use Acme\Domain\Shared\Address;
use Acme\Domain\Shared\Email;
use Acme\Domain\Shared\HashedPassword;
use Acme\Domain\Shared\Name;
use Acme\Domain\Shared\Password;
use Acme\Domain\Shared\PhoneNumber;
use Acme\Domain\User\Profile;
use Acme\Infrastructure\Careprovider\InMemoryCareproviderRepository;

class UserTest extends TestCase
{
    /** @var  Careprovider */
    protected $careprovider;

    /** @var  CareproviderRepository */
    protected $repository;

    protected $name;

    public function setUp()
    {
        $this->name = new Name("John", "Doe");
        $email = new Email("johndoe@impres.nl");

        $hashedPassword = HashedPassword::fromHashingStrategy(new Password('Veilig123!'), new BcryptHasher());

        $this->careprovider = Careprovider::register($email, $hashedPassword);
        $this->repository = new InMemoryCareproviderRepository();
    }

    /** @test */
    public function it_should_be_able_to_register_as_a_careprovider()
    {
        $this->repository->store($this->careprovider);

        $this->assertEquals($this->careprovider, $this->repository->findByEmail(new Email('johndoe@impres.nl')));
    }
}


